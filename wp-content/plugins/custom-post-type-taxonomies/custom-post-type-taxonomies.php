<?php
	/*
		Plugin Name: Jersey Finance Custom Post Types
		Description: Custom post types for the 'Jersey Finance' theme developed by Switch Digital.
		Version: 1.0
		Author: Switch Digital
		Author URI: https://www.switch.je
	*/

	/* ==== Register Custom Post Types ==== */
	function custom_post_types() {

      /* ===== News ===== */

			$labels = array(
				'name'                  => 'News',
				'singular_name'         => 'News Article',
				'menu_name'             => 'News',
				'name_admin_bar'        => 'News',
				'archives'              => 'News'
			);
			$rewrite = array(
				'slug'                  => 'news'
			);
			$args = array(
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'revisions', ),
				'hierarchical'          => false,
				'public'                => true,
				'has_archive'           => true,
				'publicly_queryable'    => true,
				'menu_icon'             => 'dashicons-welcome-write-blog',
				'rewrite'               => $rewrite
			);
			register_post_type( 'all-news', $args );

      /* === END === */

      /* ===== Events ===== */

			$labels = array(
				'name'                  => 'Events',
				'singular_name'         => 'Event',
				'menu_name'             => 'Events',
				'name_admin_bar'        => 'Events',
				'archives'              => 'Events'
			);
			$rewrite = array(
				'slug'                  => 'events'
			);
			$args = array(
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'revisions', ),
				'hierarchical'          => false,
				'public'                => true,
				'has_archive'           => true,
				'publicly_queryable'    => true,
				'menu_icon'             => 'dashicons-calendar-alt',
				'rewrite'               => $rewrite
			);
			register_post_type( 'all-events', $args );

      /* === END === */

      /* ===== Work ===== */

			$labels = array(
				'name'                  => 'Work',
				'singular_name'         => 'Work',
				'menu_name'             => 'Work',
				'name_admin_bar'        => 'Work',
				'archives'              => 'Work'
			);
			$rewrite = array(
				'slug'                  => 'our-work'
			);
			$args = array(
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'revisions', ),
				'hierarchical'          => false,
				'public'                => true,
				'has_archive'           => true,
				'publicly_queryable'    => true,
				'menu_icon'             => 'dashicons-building',
				'rewrite'               => $rewrite
			);
			register_post_type( 'all-work', $args );

      /* === END === */

      /* ===== Business Directory ===== */

			$labels = array(
				'name'                  => 'Businesses',
				'singular_name'         => 'Business',
				'menu_name'             => 'Businesses',
				'name_admin_bar'        => 'Businesses',
				'archives'              => 'Businesses'
			);
			$rewrite = array(
				'slug'                  => 'business-directory'
			);
			$args = array(
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'revisions', ),
				'hierarchical'          => false,
				'public'                => true,
				'has_archive'           => true,
				'publicly_queryable'    => true,
				'menu_icon'             => 'dashicons-businessman',
				'rewrite'               => $rewrite
			);
			register_post_type( 'all-businesses', $args );

      /* === END === */

      /* ===== Markets ===== */

			$labels = array(
				'name'                  => 'Markets',
				'singular_name'         => 'Market',
				'menu_name'             => 'Markets',
				'name_admin_bar'        => 'Markets',
				'archives'              => 'Markets'
			);
			$rewrite = array(
				'slug'                  => 'jersey-the-finance-centre/markets'
			);
			$args = array(
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'revisions', ),
				'hierarchical'          => false,
				'public'                => true,
				'has_archive'           => true,
				'publicly_queryable'    => true,
				'menu_icon'             => 'dashicons-admin-site',
				'rewrite'               => $rewrite
			);
			register_post_type( 'all-markets', $args );

      /* === END === */

      /* ===== Sectors ===== */

			$labels = array(
				'name'                  => 'Sectors',
				'singular_name'         => 'Sector',
				'menu_name'             => 'Sectors',
				'name_admin_bar'        => 'Sectors',
				'archives'              => 'Sectors'
			);
			$rewrite = array(
				'slug'                  => 'jersey-the-finance-centre/sectors'
			);
			$args = array(
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'revisions', ),
				'hierarchical'          => false,
				'public'                => true,
				'has_archive'           => true,
				'publicly_queryable'    => true,
				'menu_icon'             => 'dashicons-image-filter',
				'rewrite'               => $rewrite
			);
			register_post_type( 'all-sectors', $args );

      /* === END === */

      /* ===== JFL Careers ===== */

			$labels = array(
				'name'                  => 'JFL Vacancies',
				'singular_name'         => 'JFL Vacancy',
				'menu_name'             => 'JFL Vacancies',
				'name_admin_bar'        => 'JFL Vacancies',
				'archives'              => 'JFL Vacancies'
			);
			$rewrite = array(
				'slug'                  => 'who-we-are/join-our-team'
			);
			$args = array(
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'revisions', ),
				'hierarchical'          => false,
				'public'                => true,
				'has_archive'           => true,
				'publicly_queryable'    => true,
				'menu_icon'             => 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c3ZnIHdpZHRoPSI1NHB4IiBoZWlnaHQ9IjM3cHgiIHZpZXdCb3g9IjAgMCA1NCAzNyIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj4gICAgICAgIDx0aXRsZT5Hcm91cDwvdGl0bGU+ICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPiAgICA8ZGVmcz4gICAgICAgIDxwb2x5Z29uIGlkPSJwYXRoLTEiIHBvaW50cz0iMCAwLjEyMTA5NzY0NyA4LjQ2ODkgMC4xMjEwOTc2NDcgOC40Njg5IDEyLjg1MzEwNzcgMCAxMi44NTMxMDc3Ij48L3BvbHlnb24+ICAgICAgICA8cG9seWdvbiBpZD0icGF0aC0zIiBwb2ludHM9IjAuMjU1MyAwLjM1ODc3MDM5OCA1Mi45OTY3MzggMC4zNTg3NzAzOTggNTIuOTk2NzM4IDIxIDAuMjU1MyAyMSI+PC9wb2x5Z29uPiAgICA8L2RlZnM+ICAgIDxnIGlkPSJTeW1ib2xzIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4gICAgICAgIDxnIGlkPSJ4bF92aXNfbWFzdGhlYWRfdXBkYXRlZF9sb2dvIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMjUuMDAwMDAwLCAtMTguMDAwMDAwKSI+ICAgICAgICAgICAgPGcgaWQ9Ikdyb3VwLTQ3IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgyNS4wMDAwMDAsIDE4LjAwMDAwMCkiPiAgICAgICAgICAgICAgICA8ZyBpZD0iR3JvdXAiPiAgICAgICAgICAgICAgICAgICAgPHBhdGggZD0iTTIyLjk0OTIsMTAuNjUzNCBDMjMuNjI4MiwxMC4xMDY0IDI0LjUzMDIsOS42MzI0IDI1LjU5MDIsOS4zMDE0IEMyNS42OTUyLDguODI3NCAyNS43NDgyLDguNTkwNCAyNS44NjAyLDguMTE2NCBDMjQuMTQzMiw4LjU2MTQgMjIuNzIxMiw5LjM1NjQgMjEuNzY4MiwxMC4yOTY0IEMyMi4yNDUyLDEwLjQzMTQgMjIuNDgwMiwxMC41MDQ0IDIyLjk0OTIsMTAuNjUzNCIgaWQ9IkZpbGwtMSIgZmlsbD0iI0ZGRkZGRiI+PC9wYXRoPiAgICAgICAgICAgICAgICAgICAgPHBhdGggZD0iTTMyLjAyMjQsOC4xMTA1IEMzMi4xNDE0LDguNTg5NSAzMi4yMDA0LDguODI3NSAzMi4zMTc0LDkuMzA0NSBDMzMuMzE5NCw5LjYxNzUgMzQuMTc5NCwxMC4wNTY1IDM0Ljg0NDQsMTAuNTY3NSBDMzUuMzI2NCwxMC40MjA1IDM1LjU2NjQsMTAuMzQ5NSAzNi4wNTI0LDEwLjIxMzUgQzM1LjA5NTQsOS4zMDY1IDMzLjY5NTQsOC41NDM1IDMyLjAyMjQsOC4xMTA1IiBpZD0iRmlsbC0zIiBmaWxsPSIjRkZGRkZGIj48L3BhdGg+ICAgICAgICAgICAgICAgICAgICA8cGF0aCBkPSJNMzIuMzY5MSwxNC4yNTExIEMzMi4zNjkxLDE0LjI1MTEgMzUuMTM4MSwxNC45NTAxIDM1LjkyMDEsMTIuNDkxMSBDMzcuNzA3MSwxMS41NTkxIDM4LjY0MTEsMTEuMTM2MSA0MC41ODIxLDEwLjM5ODEgQzM4LjI5OTEsMTAuODU0MSAzNy4xODkxLDExLjE1NjEgMzUuMDU4MSwxMS44ODYxIEMzNS4wNTgxLDExLjg4NjEgMzUuMzYyMSwxNC4zOTMxIDMyLjM2OTEsMTQuMjUxMSBNMjIuNjc1MSwxMS44OTcxIEMyMC41NDIxLDExLjE3MTEgMTkuNDMyMSwxMC44NzAxIDE3LjE0NzEsMTAuNDIwMSBDMTkuMDkwMSwxMS4xNTMxIDIwLjAyNTEsMTEuNTc0MSAyMS44MTIxLDEyLjUwMzEgQzIyLjU5ODEsMTQuOTYyMSAyNS4zNjgxLDE0LjI1ODEgMjUuMzY4MSwxNC4yNTgxIEMyMi4zNzUxLDE0LjQwNzEgMjIuNjc1MSwxMS44OTcxIDIyLjY3NTEsMTEuODk3MSBNMjguODE1MSwyLjY4MDEgQzI4LjA3ODEsNS41OTMxIDI3Ljc1MTEsNy4wNTgxIDI3LjE4MDEsOS45OTMxIEMyNy4xODAxLDkuOTkzMSAyOC43NjUxLDEwLjQ3NjEgMjguNzcwMSwxMi4yMDExIEMyOC43ODQxLDguMzkzMSAyOC43OTUxLDYuNDg4MSAyOC44MTUxLDIuNjgwMSBNMzcuODk0MSwxNi40ODIxIEMzNy44NjExLDE2LjY4MjEgMzcuNjQwMSwxNi44MDExIDM3LjQwNTEsMTYuNzUwMSBDMzEuODA4MSwxNS41NjMxIDI2LjAwMzEsMTUuNTYzMSAyMC40MDUxLDE2Ljc1MDEgQzIwLjE3MDEsMTYuODAxMSAxOS45NTAxLDE2LjY4MzEgMTkuOTE2MSwxNi40ODQxIEMxOS44ODIxLDE2LjI4NTEgMjAuMDQ3MSwxNi4wNzgxIDIwLjI4NzEsMTYuMDI4MSBDMjAuNTAxMSwxNS45ODIxIDIwLjYxMDEsMTUuOTYxMSAyMC44MjUxLDE1LjkxNzEgQzE4LjkzMTEsMTMuMjU1MSAxNy44ODQxLDExLjk3MjEgMTUuNjA2MSw5LjU0NDEgQzE5LjAwNTEsMTAuMDUxMSAyMC42MzgxLDEwLjQ3MzEgMjMuNzI0MSwxMS41NjcxIEMyMy41ODMxLDExLjgxNTEgMjMuNTE3MSwxMi4wODkxIDIzLjU0NDEsMTIuMzY3MSBDMjMuNjMwMSwxMy4yNTUxIDI0LjU4MzEsMTMuODU4MSAyNS42ODAxLDEzLjc3NDEgQzI2Ljc3NjEsMTMuNjkxMSAyNy42NDcxLDEyLjk0ODEgMjcuNjI4MSwxMi4wNTcxIEMyNy42MTAxLDExLjI5NDEgMjYuOTEyMSwxMC42NzExIDI1Ljk3OTEsMTAuNTYzMSBDMjYuOTM4MSw2LjMwNTEgMjcuNTIyMSw0LjE4NzEgMjguOTA1MSwwLjAwMDEgQzMwLjI4NDEsNC4xNzkxIDMwLjg2OTEsNi4yOTIxIDMxLjgyNTEsMTAuNTQwMSBDMzAuODI3MSwxMC42MDQxIDMwLjA1OTEsMTEuMjUzMSAzMC4wNDQxLDEyLjA1MzEgQzMwLjAyNjEsMTIuOTQzMSAzMC45MDExLDEzLjY4NjEgMzEuOTk2MSwxMy43NjYxIEMzMy4wOTIxLDEzLjg0NjEgMzQuMDQ0MSwxMy4yMzgxIDM0LjEyNjEsMTIuMzUxMSBDMzQuMTUwMSwxMi4wOTExIDM0LjA5NTEsMTEuODM5MSAzMy45NzQxLDExLjYwNzEgQzM3LjEwMTEsMTAuNDg4MSAzOC43NTgxLDEwLjA1OTEgNDIuMjAzMSw5LjU0NDEgQzM5LjkyNzEsMTEuOTcyMSAzOC44ODAxLDEzLjI1NTEgMzYuOTg1MSwxNS45MTcxIEMzNy4xOTkxLDE1Ljk2MTEgMzcuMzA4MSwxNS45ODIxIDM3LjUyNDEsMTYuMDI4MSBDMzcuNzYzMSwxNi4wNzgxIDM3LjkyOTEsMTYuMjg1MSAzNy44OTQxLDE2LjQ4MjEiIGlkPSJGaWxsLTUiIGZpbGw9IiNGRkZGRkYiPjwvcGF0aD4gICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik0xMC4xMDI1LDIxLjA4OCBDMTAuMDg2NSwyMS4zNjYgOS44NTc1LDIxLjM1IDkuNTkyNSwyMS42MDMgQzkuMjAzNSwyMi4wMyA5LjY5NjUsMjIuNTMxIDkuOTU2NSwyMi43NTIgQzEwLjI2MTUsMjIuOTUxIDEwLjc5OTUsMjIuOTU3IDExLjExNTUsMjIuNzY3IEMxMC40Mzg1LDIyLjQ2IDkuOTkyNSwyMS45ODQgMTAuNDUzNSwyMS40NjQgQzExLjQ2MTUsMjAuNjQ2IDkuNzQ5NSwyMC4zNSA5Ljk2OTUsMTkuNjcxIEMxMC4xODQ1LDE5LjUwNiAxMC42NjA1LDIwLjEzMiAxMC45Mjk1LDIwLjA5MSBDMTMuOTc3NSwxOS42NDcgMTEuNzQ5NSwyMi42NTQgMTMuMDEwNSwyMy42OSBDMTQuMTEyNSwyMy4wMDMgMTMuMjUwNSwyMS41MzIgMTMuNTgwNSwyMC41MTMgQzEzLjgzNjUsMTkuOTQ5IDE0LjI2NjUsMjAuNjQxIDE0LjY4MDUsMjAuMjA4IEMxNC45ODY1LDE5Ljc2NCAxNS43Mzg1LDE5LjcwMSAxNi4wNjQ1LDE5Ljc4NiBDMTQuODY5NSwyMC40NDIgMTcuMDM5NSwyMi4yMyAxNC45NzQ1LDIyLjU0NCBMMTQuODA3NSwyMi41OTMgQzE1LjExMzUsMjIuNTExIDE0LjkxNTUsMjMuMDY4IDE2LjE4MTUsMjIuOTI5IEMxNi42ODY1LDIyLjgyIDE2LjkyODUsMjIuNTk2IDE2LjkwOTUsMjIuMDYxIEMxNi44NTU1LDIxLjc1MiAxNi40ODI1LDIxLjczNCAxNi42MjY1LDIxLjMwMyBDMTYuODAxNSwyMC43NzYgMTcuNDY2NSwyMS4wNDUgMTcuNzg0NSwyMC42NzggQzE4LjE2MDUsMjAuMDk0IDE4LjA0OTUsMTkuMTYzIDE3LjMwMTUsMTguNzUgQzE2LjkwOTUsMTguNTkyIDE2LjIxNTUsMTguNTI3IDE1LjkwMTUsMTguODYgQzE1LjQ2MzUsMTguNTQgMTYuMjAyNSwxOC40MDkgMTYuMTU3NSwxOC4wOTggQzE1Ljg5NDUsMTcuMzUgMTUuMTEzNSwxOC4xNTkgMTUuMDE5NSwxNy44MjggQzE0Ljk4MDUsMTcuNTgxIDE1LjA0OTUsMTcuMzgzIDE1LjEyNTUsMTcuMTg0IEMxNS4zNTg1LDE2LjY4MiAxNC40NjA1LDE3LjAwMiAxNC40MDg1LDE3LjA2NCBDMTQuMTczNSwxNy4yMDMgMTQuMTkzNSwxNy40ODMgMTQuMTE4NSwxNy42ODcgQzEzLjg4MzUsMTcuMzcgMTMuOTc1NSwxNi42MzQgMTMuMzY3NSwxNi43ODYgQzEzLjIxMzUsMTYuODI1IDEzLjMxODUsMTcuMjU5IDEzLjA4ODUsMTcuNDE4IEMxMy4wMjg1LDE3LjM0MiAxMi45ODQ1LDE3LjMyMiAxMi45MjE1LDE3LjEzOCBDMTIuNzUwNSwxNi40ODIgMTEuODkyNSwxNi41OTkgMTEuOTQ1NSwxNy40MDQgTDExLjU3MjUsMTcuMTA3IEMxMC45Njg1LDE3LjAxNSAxMS4xMjU1LDE3LjYwNyAxMC45MjM1LDE3LjkwNSBDMTAuNzc2NSwxNy43MzUgMTAuNDM4NSwxNy42MjkgMTAuNDAyNSwxNy42NDEgQzkuNTkzNSwxNy43NDYgMTAuMjg1NSwxOC40NzYgMTAuMjYxNSwxOC45MjEgQzkuNjQwNSwxOC41MjUgOC43MTQ1LDE5LjE1MSA4LjcxNDUsMTkuMTUxIEM4LjM2MTUsMTkuNTg1IDguMjY2NSwyMC4wMSA4LjMxOTUsMjAuNTYxIEM4LjQ3NzUsMjEuNTQ2IDkuNTcwNSwyMC45NjEgMTAuMTAyNSwyMS4wODgiIGlkPSJGaWxsLTciIGZpbGw9IiNGRkZGRkYiPjwvcGF0aD4gICAgICAgICAgICAgICAgICAgIDxnIGlkPSJHcm91cC0xMSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC4wMDAwMDAsIDE3LjAzODUwMCkiPiAgICAgICAgICAgICAgICAgICAgICAgIDxtYXNrIGlkPSJtYXNrLTIiIGZpbGw9IndoaXRlIj4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHVzZSB4bGluazpocmVmPSIjcGF0aC0xIj48L3VzZT4gICAgICAgICAgICAgICAgICAgICAgICA8L21hc2s+ICAgICAgICAgICAgICAgICAgICAgICAgPGcgaWQ9IkNsaXAtMTAiPjwvZz4gICAgICAgICAgICAgICAgICAgICAgICA8cGF0aCBkPSJNMy41NDI5LDcuMjkyNyBDMi4xNzU5LDYuODU1NyAzLjA1MDksOC40NjQ3IDIuMjQ3OSw4LjgxNDcgQzEuODU2OSw5LjE1NTcgMS40NzU5LDguNzIyNyAxLjA5NDksOC42NzY3IEwxLjcyNTksOS4zMTY3IEMyLjIwMzksOS45MTc3IDIuOTY3OSw5LjM2MDcgMy4yNjQ5LDguOTYwNyBDMy45NDA5LDkuMzQ3NyAzLjc4MDksOC4zMjc3IDQuMzAxOSw4LjIwMTcgTDQuNTQ1OSw4LjcyMDcgTDQuMzk2OSwxMC44OTY3IEM0LjQ2NzksMTEuMjM3NyA0LjY4ODksMTEuNDQ5NyA1LjAyNDksMTEuNTQ1NyBDNC44MzI5LDExLjE5MTcgNC44Mzc5LDEwLjYwNTcgNS4wNDE5LDEwLjI1NjcgQzUuNTc1OSwxMC44Mjc3IDQuOTM2OSwxMi4wNzk3IDYuMDM1OSwxMi4xOTM3IEM1Ljg1NTksMTEuODA0NyA1Ljg2NDksMTEuMTcwNyA2LjA1NDksMTAuNzkwNyBDNi4xNDM5LDEwLjkxNjcgNi4yODk5LDExLjAzOTcgNi4zMDY5LDExLjE3NTcgQzYuNTI0OSwxMS44NTI3IDYuMjQ3OSwxMi44OTY3IDcuMzEwOSwxMi44NTE3IEw3LjE5MjksMTIuMDc3NyBDNy42NjY5LDExLjIyNzcgNy45NTk5LDEyLjY1NzcgOC40Njg5LDEyLjA5MzcgQzguMTEyOSw5LjU5NTcgNi42NzA5LDcuNTIwNyA1LjM1OTksNS40MDM3IEM0LjkyMjksNC4yNDA3IDUuOTgxOSwzLjM5MDcgNi4zNzY5LDIuNDM1NyBDNi41OTU5LDEuOTA0NyA2LjI5OTksLTAuMDE3MyA0LjI2NTksMC4yNzE3IEM1LjMwNjksMC4zNTY3IDUuMjAzOSwxLjMyNTcgNS4wNjM5LDEuNTUyNyBDNC44NjY5LDEuODY5NyA0LjQ1MDksMi4xNjU3IDQuNTAyOSwyLjU3NTcgQzQuMjAxOSwxLjk5MzcgMy41NTU5LDEuNjc3NyAzLjIwNTksMS4xNjY3IEMzLjEwMDksMS4wMTM3IDIuOTAyOSwwLjYwMjcgMi4zMTM5LDAuMzMxNyBDMS44MjQ5LDAuMTEwNyAxLjI3NzksLTAuMDE2MyAwLjk0NjksMC4zNjQ3IEMxLjk5NjksMC4yNTc3IDIuMDU4OSwwLjk0NDcgMi4wODE5LDEuNDAwNyBDMS44NDg5LDIuNjczNyAyLjgxODksMy4xNDk3IDMuNDQ5OSw0LjEwMTcgQzMuNjA4OSw0LjI5NDcgMy42NDk5LDQuNjc5NyAzLjQ0MDksNC44NjM3IEMyLjY4NjksNC43MjY3IDEuNzY2OSwyLjMyODcgLTAuMDAwMSw0LjE4MjcgQzAuNzg3OSwzLjgwNzcgMS43MjM5LDQuMzE0NyAxLjg2NjksNC44NjA3IEMyLjAxMTksNS40MDc3IDIuMDIwOSw1Ljc0NzcgMi40NTQ5LDUuOTgwNyBDMi44ODk5LDYuMjE0NyAzLjIyODksNi4xNzQ3IDMuNTUyOSw2LjUzMjcgQzMuNzk1OSw2Ljc3NDcgNC4wMTY5LDcuMTA1NyAzLjU0MjksNy4yOTI3IiBpZD0iRmlsbC05IiBmaWxsPSIjRkZGRkZGIiBtYXNrPSJ1cmwoI21hc2stMikiPjwvcGF0aD4gICAgICAgICAgICAgICAgICAgIDwvZz4gICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik0xNS4xMjEsMjEuODY5MyBMMTUuMjQ0LDIxLjI2MzMgQzE1LjAxNSwyMS43NDgzIDEzLjU2MywyMS4wNzAzIDEzLjk2NCwyMi4wMTIzIEMxNC4zODMsMjIuMDM1MyAxNC44MzIsMjIuNTM2MyAxNS4xMjEsMjEuODY5MyIgaWQ9IkZpbGwtMTIiIGZpbGw9IiNGRkZGRkYiPjwvcGF0aD4gICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik0xMC45NTIxLDIxLjE4NTcgTDExLjA3NjEsMjEuNzkyNyBDMTEuMzY0MSwyMi40NTg3IDExLjgxMTEsMjEuOTU4NyAxMi4yMzAxLDIxLjkzMzcgQzEyLjYzMzEsMjAuOTkzNyAxMS4xODExLDIxLjY3MDcgMTAuOTUyMSwyMS4xODU3IiBpZD0iRmlsbC0xNCIgZmlsbD0iI0ZGRkZGRiI+PC9wYXRoPiAgICAgICAgICAgICAgICAgICAgPHBhdGggZD0iTTEzLjcwNSwyNC4wMDIxIEMxMy44OTgsMjQuMjY2MSAxNC4wNTUsMjQuMzYyMSAxNC40NSwyNC4zMTYxIEMxNC42NjEsMjQuMzE2MSAxNS4zNzMsMjMuODE1MSAxNS4zNzgsMjMuMjU2MSBDMTQuODg4LDIzLjM3MDEgMTQuNTc0LDIzLjA2OTEgMTQuMjM1LDIyLjg1NjEgQzE0LjMyOCwyMy4zNTUxIDEzLjQ3OSwyMy40NDIxIDEzLjcwNSwyNC4wMDIxIiBpZD0iRmlsbC0xNiIgZmlsbD0iI0ZGRkZGRiI+PC9wYXRoPiAgICAgICAgICAgICAgICAgICAgPHBhdGggZD0iTTEwLjgxNTQsMjMuMTgzNyBDMTAuODIzNCwyMy43MDg3IDExLjQ5MzQsMjQuMTgxNyAxMS42OTI0LDI0LjE4MTcgQzEyLjA2OTQsMjQuMjI2NyAxMi4yMTU0LDI0LjEzMjcgMTIuMzk3NCwyMy44ODU3IEMxMi42MDk0LDIzLjM1OTcgMTEuODA5NCwyMy4yNzU3IDExLjg5NjQsMjIuODA1NyBDMTEuNTc2NCwyMy4wMDY3IDExLjI4MTQsMjMuMjg4NyAxMC44MTU0LDIzLjE4MzciIGlkPSJGaWxsLTE4IiBmaWxsPSIjRkZGRkZGIj48L3BhdGg+ICAgICAgICAgICAgICAgICAgICA8cGF0aCBkPSJNMTcuMDE3NSwyNS40NTEzIEMxNy4wMzU1LDI1LjQ1MTMgMTcuMjQ1NSwyNS44NTczIDE3LjI5ODUsMjUuMTA1MyBDMTcuODY0NSwyNC4yMTIzIDE2Ljg5MzUsMjMuOTQ3MyAxNi42ODk1LDIzLjE5ODMgTDE2LjU1NTUsMjMuMzE4MyBDMTUuOTIxNSwyMy4xNjMzIDE1Ljg3NjUsMjQuODIzMyAxNS4wNTk1LDI0LjkzNzMgQzEzLjg2NTUsMjUuMDY5MyAxNC44MDc1LDI2Ljc2MzMgMTMuNzM5NSwyNy4xNTczIEMxMy41Mjk1LDI3LjI5ODMgMTIuODU1NSwyNy4yMjYzIDEyLjQ2MTUsMjcuMDc1MyBDMTEuNzQ0NSwyNi42MjIzIDExLjk5MjUsMjUuODMyMyAxMS45NDU1LDI1LjEwODMgTDExLjcxOTUsMjUuMDM2MyBDMTAuNzA5NSwyNC43ODQzIDEwLjI3NTUsMjQuMjEyMyAxMC4yMjA1LDIzLjM2ODMgQzkuNzE5NSwyMy41OTczIDkuNzQ3NSwyMi45NTAzIDkuMzU0NSwyMi45MTkzIEM5LjAwNDUsMjMuNTE0MyA4LjU2ODUsMjQuMzE3MyA4Ljc5ODUsMjQuOTM2MyBMOS40MzY1LDI0LjI0NjMgTDkuNTc3NSwyNC4zNjkzIEM5LjA3MjUsMjUuMDQ5MyA4LjY4MzUsMjUuNzY5MyA4LjY1OTUsMjYuNjQ4MyBDOC42MzI1LDI2Ljg4MTMgOC43MDM1LDI3LjI5MjMgOS4wNDE1LDI3LjE1NTMgQzguOTg0NSwyNi42NjIzIDkuMzM1NSwyNi4zNDUzIDkuNjg5NSwyNi4wMjUzIEMxMC4wMjA1LDI2LjI0NTMgOS42ODQ1LDI2LjM2MDMgOS41NDk1LDI2LjgzNjMgQzkuMjgzNSwyNy42MTYzIDkuMTM0NSwyOC40MTEzIDkuNzY3NSwyOC45MzczIEM5LjY2MzUsMjguMDYzMyA5LjkyNjUsMjcuMjM3MyAxMC41NjE1LDI2LjgwMjMgQzEwLjYxNTUsMjcuNTE5MyA5LjkxNDUsMjguMjM2MyAxMC4xNDg1LDI5LjIwMjMgQzEwLjI4OTUsMjkuNjY5MyAxMC41NDI1LDI5Ljg5NzMgMTAuOTEzNSwyOS44NDQzIEMxMC42NjU1LDI5LjE2NTMgMTAuNzkyNSwyOC4zMjgzIDExLjMwNzUsMjcuODI2MyBDMTEuNjc3NSwyOC40MTUzIDExLjM1NTUsMjguNjgwMyAxMS4zMzU1LDI5LjY0OTMgQzExLjMzNTUsMzAuMjU5MyAxMS40NTE1LDMxLjA1NDMgMTIuMjg1NSwzMC44NzUzIEMxMi4yOTY1LDI5Ljg2MTMgMTEuNjU5NSwyOC40MzMzIDEyLjMyMTUsMjcuODM3MyBMMTIuNTczNSwyOC4wODYzIEMxMi4yMjk1LDI5LjA5NzMgMTIuNjQ3NSwzMC43MTAzIDEyLjkyNTUsMzAuODg0MyBDMTQuMzAyNSwzMC4zMTYzIDEzLjI3MDUsMjguODE2MyAxMy41OTQ1LDI3Ljg1NDMgQzE0LjI4MDUsMjguMzM5MyAxNC4xMTY1LDI5LjI4MDMgMTQuMDc1NSwyOS44ODUzIEwxNC4yMDI1LDI5Ljg4NTMgQzE0Ljk5MDUsMjkuNDkzMyAxNS4wMjI1LDI4Ljc5NDMgMTQuODY5NSwyNy45OTQzIEMxNC43NzQ1LDI3LjYzNTMgMTQuNDkzNSwyNy4zNjMzIDE0Ljc0MzUsMjcuMTAzMyBDMTUuMzMyNSwyNy4zNTAzIDE1LjE0OTUsMjguMjMzMyAxNS4zNTU1LDI4LjYzODMgQzE1LjU3NzUsMjguNTA1MyAxNS41ODA1LDI4LjI3MzMgMTUuNzM4NSwyOC4xMjYzIEMxNi4yNTM1LDI3LjI2MjMgMTUuMzQ2NSwyNi40OTgzIDE1LjY0NzUsMjUuNzE3MyBDMTUuODUzNSwyNi4wNDAzIDE2LjI0ODUsMjYuNDkxMyAxNi4yNjY1LDI2Ljk5NDMgQzE3LjExODUsMjYuNDAzMyAxNi4yNjM1LDI1LjM5OTMgMTYuNTQwNSwyNC43MjAzIEwxNy4wMTc1LDI1LjQ1MTMgWiIgaWQ9IkZpbGwtMjAiIGZpbGw9IiNGRkZGRkYiPjwvcGF0aD4gICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik0xMi42ODc0LDI2LjYwMDcgQzEyLjY4NzQsMjYuNjAwNyAxMi44OTU0LDI2LjY0NDcgMTMuMTc3NCwyNi42NDQ3IEMxMy40NTc0LDI2LjY0NDcgMTMuNTMwNCwyNi4yOTQ3IDEzLjUzMDQsMjYuMjk0NyBDMTMuNTMwNCwyNi4yOTQ3IDEzLjYwMTQsMjUuMTk0NyAxMy42MjQ0LDI0LjkxMjcgQzEzLjY0NjQsMjQuNjMxNyAxMy42MDE0LDI0LjYyOTcgMTMuNDU5NCwyNC40NDM3IEMxMy4zNzM0LDI0LjMyNTcgMTMuMDM5NCwyNC4wOTE3IDEyLjg5NjQsMjQuMDkxNyBDMTIuNzU3NCwyNC4wOTE3IDEyLjQ5OTQsMjQuNzcyNyAxMi40OTk0LDI0Ljc3MjcgQzEyLjQyODQsMjQuOTM1NyAxMi40MDU0LDI1Ljk2NTcgMTIuNDI4NCwyNi4yNDg3IEMxMi40NTI0LDI2LjUzMDcgMTIuNjg3NCwyNi42MDA3IDEyLjY4NzQsMjYuNjAwNyIgaWQ9IkZpbGwtMjIiIGZpbGw9IiNGRkZGRkYiPjwvcGF0aD4gICAgICAgICAgICAgICAgICAgIDxnIGlkPSJHcm91cC0yNiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMS4wMDAwMDAsIDE2LjAzODUwMCkiPiAgICAgICAgICAgICAgICAgICAgICAgIDxtYXNrIGlkPSJtYXNrLTQiIGZpbGw9IndoaXRlIj4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHVzZSB4bGluazpocmVmPSIjcGF0aC0zIj48L3VzZT4gICAgICAgICAgICAgICAgICAgICAgICA8L21hc2s+ICAgICAgICAgICAgICAgICAgICAgICAgPGcgaWQ9IkNsaXAtMjUiPjwvZz4gICAgICAgICAgICAgICAgICAgICAgICA8cGF0aCBkPSJNNTIuODYxMywxNi40NzgyIEM1Mi44NjEzLDE2LjQ3ODIgNTIuOTUxMywxNy41MjAyIDUxLjY4OTMsMTcuMzYxMiBDNTEuMjU2MywxNi41ODgyIDQ5LjcyNjMsMTYuNTAyMiA0OS4yODAzLDE2LjM0NDIgQzQ4Ljk1MzMsMTYuMjA5MiA0OC42OTAzLDE1Ljc4MTIgNDkuNTQzMywxNS40MDkyIEM1MC40MzIzLDE1LjAzMzIgNTEuNDgxMywxNC45OTEyIDUxLjk2ODMsMTUuOTQ1MiBDNTIuNjU0MywxNC4xNzUyIDUwLjIyODMsMTQuNDQ5MiA0OS41NjUzLDEzLjYxNjIgQzQ5Ljg5MzMsMTMuMjY3MiA1MC4wNjAzLDEzLjA4ODIgNTAuMzQwMywxMi43MzQyIEM0OS41MTAzLDEyLjY2NTIgNDkuNDkyMywxMi43MzUyIDQ5LjIwMDMsMTIuNDY1MiBMNDkuODQxMywxMS45NTUyIEM0OS41MzkzLDExLjQzODIgNDguODIzMywxMS44ODQyIDQ4LjQ0MTMsMTEuNTU3MiBDNDguNzQyMywxMS4yNDcyIDQ4Ljg1OTMsMTEuMTU3MiA0OS4xMTQzLDEwLjkzOTIgQzQ4Ljc2ODMsMTAuODU4MiA0OC4zODUzLDEwLjkzNzIgNDguMDcwMywxMC45MDUyIEM0Ny44MTYzLDEwLjY0MjIgNDguMTA4MywxMC4yOTcyIDQ4LjIwNTMsMTAuMDIwMiBDNDcuMzU5MywxMC4yNjQyIDQ0LjUwNTMsMTEuOTA0MiA0MS43NTIzLDExLjA2NTIgQzM5Ljc3MDMsMTAuMzcwMiAzNy44ODAzLDguNTQ0MiAzNS40MjQzLDguODQ2MiBDMzYuMzA4Myw4LjU1MDIgMzguOTA0Myw4Ljc1NzIgNDAuNTk2Myw4Ljg2NTIgQzQxLjQzNjMsOC45MTcyIDQyLjM0MTMsOC45ODkyIDQzLjE2ODMsOS4wODQyIEM0NS42NTkzLDkuMzczMiA0Ny44MzYzLDkuNzkyMiA0OC45NzgzLDkuMjY0MiBDNDkuNjcyMywxMC4xMzMyIDUxLjAxODMsMTAuNzQyMiA1Mi4yOTIzLDEwLjMyNjIgQzUxLjczMTMsMTAuMTU3MiA1MS4xODUzLDkuNjczMiA1MC45ODEzLDkuNDgwMiBDNDkuOTA3Myw4LjI3OTIgNTEuODA1Myw3LjIxMDIgNTEuMDc1Myw1Ljk2NDIgQzQ5LjY0MTMsMy43MzcyIDQ2LjM4NTMsNC43MDMyIDQ0LjA1NDMsNC4wNzgyIEM0Mi45NjEzLDMuNTYyMiA0MS45NjYzLDMuMzM0MiA0MC43NDAzLDMuMzE5MiBDNDAuNjU2MywzLjMwODIgNDAuNTI4MywzLjMwMDIgNDAuMzI5MywzLjMwNTIgQzM5LjkxMzMsMy4zMTIyIDQwLjM4MjMsMy41MzYyIDQwLjYwNTMsMy42MzEyIEMzOS4wMjczLDMuNjAwMiAzNy41OTgzLDIuNzE2MiAzOC4yMDYzLDMuNzc5MiBDMzcuMzIzMywzLjI3OTIgMzUuMTEyMywzLjA3NjIgMzUuNjYyMywzLjUyMTIgTDM2LjAwNzMsMy43NzkyIEMzNS43MTgzLDMuNzk3MiAzMi43OTczLDMuMDUwMiAzMy41NTkzLDMuODI4MiBDMzMuNDY5Myw0LjEwMjIgMzEuMDY3MywzLjM5NzIgMzEuNzY0Myw0LjA1MzIgQzMxLjY0MjMsNC4wNzIyIDMwLjk3NTMsNC4yMTEyIDMwLjgwNzMsNC4zMDEyIEMzMC40NjgzLDQuNDQwMiAzMC4xMzYzLDQuNjYwMiAyOS44MzgzLDQuODAxMiBDMjcuMDM2Myw1LjgwNDIgMjMuMzM5Myw2LjMyNjIgMjAuMzY2Myw1LjE4OTIgQzE5LjUwODMsNC44NjIyIDE5LjM0MjMsNC4xMDgyIDE5LjYxNDMsMy41OTYyIEMyMC4zNDczLDIuMjEyMiAyMy4zMzIzLDEuOTk1MiAyNS4yMjYzLDEuOTc2MiBDMjcuOTAxMywxLjk1MDIgMjkuNzkxMywxLjkyNDIgMzIuMzYwMywyLjE2MDIgQzMzLjg3OTMsMi4yOTkyIDM3LjYwMzMsMi41OTYyIDM5LjgxMDMsMi42ODQyIEM0MC45NTIzLDIuNzMwMiA0MS43OTUzLDIuNzMxMiA0Mi41NjQzLDIuNzY4MiBDNDMuMTE5MywyLjc5NTIgNDcuMjgwMywzLjA3NjIgNDguOTM0MywyLjg2MDIgQzQ5Ljg0NjMsMi4zNDgyIDUwLjY4MjMsMS4yMzkyIDUxLjg4NDMsMS44NzQyIEM1MS40OTYzLDEuMzczMiA1MC43MzUzLDAuOTg0MiA0OS43MDQzLDEuNDMxMiBDNDguNjc0MywxLjg3ODIgNDcuMzM5MywwLjk0MDIgNDYuMjc3MywwLjUyMzIgQzQzLjgxMTMsLTAuMTE5OCA0My4wODYzLDEuMzA1MiA0MS4wMDkzLDEuNjc0MiBDNDAuMDUyMywxLjg0NTIgMzkuNjMyMywxLjc5MjIgMzguNzg3MywxLjc1MzIgQzM4LjI1NjMsMS43MjkyIDM2LjcyMTMsMS41NTYyIDM2LjA2MjMsMS40NDkyIEMzMy4wNTQzLDAuOTU1MiAzMS4wNjEzLDAuODE0MiAzMS4wNjEzLDAuODE0MiBDMjguNzAwMywwLjY0NjIgMjYuNTYxMywwLjY3NTIgMjQuMjQ5MywwLjkxNzIgQzIzLjAzMDMsMS4wNDUyIDIxLjUxNDMsMS4yNTEyIDIwLjE5NzMsMS41MjcyIEMxOS43NDEzLDEuNjIzMiAxOS43NjQzLDEuNjI1MiAxOS4zOTAzLDEuNzE0MiBDMTkuMTAxMywxLjc4MzIgMTkuMDY2MywxLjY2MjIgMTguODI1MywxLjQ1NDIgQzE4LjEzMjMsMC44NTgyIDE3LjAwNjMsMS4xODQyIDE2LjcwNzMsMS4xODAyIEMxOC4zNzMzLDEuNjU2MiAxOC4xMDMzLDMuNTc5MiAxOC40NTIzLDQuOTEyMiBDMTguODM1Myw2LjIzNzIgMjAuMjA1Myw2LjcwNzIgMjEuMzY2Myw2Ljg2ODIgQzI1LjYyMTMsNy40NTgyIDI1LjM4MjMsNi4zMjUyIDMzLjkyMTMsNS43NTAyIEM0Mi40NjAzLDUuMTc2MiA0Ni4zNDEzLDUuMTc2MiA0OC4yNjEzLDUuNTM2MiBDNDguODM0Myw1LjY0ODIgNTAuMDY2Myw2LjAzNzIgNDkuNzcyMyw3LjA5NjIgQzQ5LjAzNzMsOS4zMDcyIDQ1LjcyMTMsOC4yMjIyIDQzLjIzODMsOC4wMzcyIEM0MC43NTUzLDcuODUyMiAzNy40OTUzLDcuNTcwMiAzNS41NTczLDcuNTUzMiBDMzMuNjIwMyw3LjUzNzIgMzEuODQ5Myw3LjQ1ODIgMjcuNjUzMyw3Ljc3NDIgQzI0Ljc5NDMsNy45OTYyIDIxLjQ3OTMsOC4yNzIyIDIxLjQ3OTMsOC4yNzIyIEwxNy4xMzMzLDguNDczMiBDMTcuMDc2Myw5LjI1MjIgMTYuOTcyMywxMC41MTcyIDE1Ljk2ODMsMTEuNDMxMiBDMTQuOTY1MywxMi4zNDYyIDE0LjY0NjMsMTMuMTY4MiAxNC4yNDQzLDEzLjk0NjIgQzEzLjgzODMsMTQuODQxMiAxMi43MDYzLDE0LjQ3MzIgMTIuNTYxMywxNS40NTcyIEMxMC4zNTkzLDE2LjI2MzIgNy42ODgzLDE1LjkxNzIgNS4yNTczLDE2LjEzNDIgQzQuNDU4MywxNi4wMzIyIDQuNDM0MywxNC45NjAyIDQuNDEyMywxNC43MjAyIEM0LjM5MDMsMTQuNDgxMiA0LjQyMDMsMTQuMTY2MiA0LjEzNDMsMTQuMDI5MiBDMy44NDkzLDEzLjg5MTIgMy41MDYzLDEzLjk5NTIgMy4zNzAzLDE0LjE5MjIgQzIuOTUzMywxMy44ODcyIDEuNTc4MywxMi45MDcyIDAuNTQ1MywxNC4xNTcyIEMxLjEzNDMsMTMuOTIwMiAxLjg4NDMsMTQuMTcyMiAyLjEyNDMsMTQuNDQ5MiBDMi41NTQzLDE0Ljk0OTIgMi43NTQzLDE1Ljc5MTIgMy40NjkzLDE1Ljg2NjIgQzMuNTUyMywxNi4xNjQyIDMuODIzMywxNi41NDMyIDMuNzE3MywxNi43NjIyIEMzLjUwNTMsMTYuNTgwMiAzLjA5NDMsMTYuNjA3MiAzLjA4MjMsMTYuMjM5MiBDMi4zNzgzLDE1LjM5MTIgMC4zNjgzLDE1LjgxOTIgMC4yNTUzLDE3LjIyNjIgQzAuNTYyMywxNi45MTYyIDAuOTUwMywxNi43ODYyIDEuNDA4MywxNi44NTYyIEMxLjY3MTMsMTcuODg2MiAyLjc1NjMsMTcuNTAwMiAzLjQ0NjMsMTcuNzc1MiBMMy4xOTEzLDE4LjAzNjIgQzIuMzAwMywxNy44MzUyIDEuODIyMywxOC4xODkyIDEuMzQzMywxOC42ODMyIEMxLjI3NDMsMTguNzUzMiAxLjA4NjMsMTkuMDY1MiAxLjAxODMsMTkuNDI5MiBDMC45MTgzLDE5Ljk0MjIgMC44ODUzLDIwLjUyODIgMS4zNTkzLDIwLjgyNTIgQzEuMjA3MywyMC4yMTcyIDEuNjA4MywxOS44NDgyIDEuODk2MywxOS40MjcyIEMzLjE4MTMsMjAuMDg2MiAzLjc4NzMsMTguNjU0MiA0LjU5NzMsMTguMDUzMiBMNC44NDQzLDE4LjMwOTIgQzQuNzA3MywxOC40NjAyIDQuNDI3MywxOC42MjQyIDQuNTkwMywxOC44MTcyIEw1LjM0OTMsMTguOTU1MiBDNS42OTIzLDE4Ljk2OTIgNS42OTMzLDE4LjYxNzIgNS44NzEzLDE4LjQ0NTIgQzYuNDU4MywxOC45NDEyIDUuMjY5MywxOS40NjcyIDUuODUwMywyMC4xMTAyIEM1Ljg1MDMsMjAuMTEwMiA1LjUxNjMsMjAuNzIzMiA0LjU2NDMsMjAuNzM4MiBDNS42NTEzLDIxLjM0NjIgNi45MjYzLDIwLjU0NDIgNy4yNTgzLDIwLjI2MDIgQzcuNTI4MywyMC4wMzEyIDcuNzA0MywxOS43MzAyIDcuNjQ2MywxOS4yMzkyIEM3LjYyMDMsMTguOTMwMiA3LjE4NTMsMTguNzc5MiA3LjE0NzMsMTguNDYwMiBDNy45MzUzLDE4LjMxNTIgOC42ODkzLDE5LjM3NDIgOS41NjkzLDE5LjAwNzIgTDguODE0MywxOC4yMzEyIEM5Ljg5MzMsMTguMTIwMiAxMC41NTUzLDE5LjU1NzIgMTEuNzU3MywxOC45MDUyIEwxMC44NzAzLDE4LjAwMDIgQzEyLjAxNTMsMTcuODM5MiAxMi45MTEzLDE5LjQwNzIgMTQuMzE3MywxOC42ODcyIEMxMy44OTEzLDE4LjI5NDIgMTMuMjgwMywxOC4wOTcyIDEzLjA1MDMsMTcuNTEzMiBMMTIuOTcxMywxNy4yMDEyIEwxMy4zMDEzLDE3LjI1NjIgQzE0LjU2NzMsMTcuMzUyMiAxNS4wODYzLDE5LjIyNjIgMTYuNzU0MywxOC4zMzAyIEMxNi43NTQzLDE4LjMzMDIgMTcuNzMyMywxNi42OTQyIDE3LjMwNzMsMTQuNDkxMiBDMTYuOTUzMywxMi42NTYyIDE4Ljc5ODMsMTIuOTk5MiAyMC4xNjUzLDEyLjk2NTIgQzIwLjc5ODMsMTIuOTUzMiAyMS4yMTgzLDEyLjYwMjIgMjEuMzc2MywxMi40NjgyIEMyMi4wNzQzLDExLjg3NDIgMjIuNzk5MywxMS4zNDMyIDIzLjU1MzMsMTAuODc3MiBDMjQuODg1MywxMC4wMDIyIDI4LjE3NjMsMTAuMzUwMiAyOS44OTMzLDExLjA3NTIgQzI5LjI5MjMsMTEuMjU4MiAyOC41MDMzLDExLjE5ODIgMjcuODgyMywxMS4xNjIyIEMyNy4zNzUzLDExLjEyOTIgMjYuNjY2MywxMC45MDMyIDI2LjI5NjMsMTEuNDA2MiBDMjYuMTA3MywxMS43MzAyIDI2LjQyODMsMTIuMDI2MiAyNi42NzUzLDEyLjA1OTIgQzI2LjY3NTMsMTIuMDU5MiAyOS44ODQzLDEyLjU2NzIgMzAuNDQ1MywxMi42NzMyIEMzMS40NjAzLDEyLjgxOTIgMzEuODE2MywxMi45ODcyIDMxLjc5NDMsMTMuMzk5MiBDMzEuNzc0MywxMy43NTMyIDMxLjE3NDMsMTMuODMyMiAzMC43MzEzLDEzLjg4OTIgQzI5LjQzODMsMTQuMDU2MiAyOC40MDkzLDE0LjE2NTIgMjcuMzM3MywxNC4wNzYyIEMyNi45NzkzLDE0LjA0NTIgMjUuOTgwMywxNC4wMjkyIDI1LjU4NTMsMTMuNjU0MiBDMjUuMjAyMywxMy4yOTAyIDI1LjAxMzMsMTIuNTQ0MiAyNC42MzAzLDEyLjE1OTIgQzI0LjI5NjMsMTEuNTcwMiAyMi4zNTQzLDExLjI2MTIgMjEuOTM1MywxMy4wMTQyIEMyMi40NzIzLDEyLjQ1NTIgMjMuMDc5MywxMi42ODAyIDIzLjM0MDMsMTIuOTExMiBDMjIuODcxMywxMy45NDkyIDI0LjA1NjMsMTMuODc0MiAyNC40ODAzLDE0LjMyNzIgTDI0LjUwNTMsMTQuNTg0MiBMMjQuMjE4MywxNC41NzcyIEwyMi42ODczLDEzLjY3NDIgQzIxLjU2MDMsMTMuMDM1MiAxOS40MTczLDEzLjU3MDIgMTkuMjA1MywxNS44MDAyIEMxOS4yMDUzLDE1LjgwMDIgMjAuMDk4MywxNC4xODYyIDIwLjc4MTMsMTQuOTk5MiBDMjEuNDYzMywxNS44MTIyIDIyLjkyODMsMTUuMTE3MiAyMy44MjUzLDE1LjczMDIgTDIzLjg3MTMsMTUuODYyMiBMMjMuNjkzMywxNS44NTgyIEMyMi40NjEzLDE1LjMxMDIgMjAuOTM3MywxNi43ODMyIDIxLjQ3NTMsMTguNTE0MiBDMjEuOTA4MywxNy4wMTkyIDIyLjU3ODMsMTcuMzE4MiAyMi44NDMzLDE3LjU5MTIgQzIyLjkwMjMsMTcuNjg3MiAyMy4xMTgzLDE3LjkxNDIgMjMuMzgyMywxNy45ODEyIEMyMy42NDczLDE4LjA2NTIgMjMuODI3MywxOC4wNjQyIDIzLjk1MTMsMTguMDA1MiBDMjQuNTAxMywxNy43NDcyIDIzLjk3MjMsMTYuMzY4MiAyNC45NjUzLDE2LjI1NjIgQzI1LjIzMjMsMTYuMjI0MiAyNS40MzUzLDE2LjI5MTIgMjUuNTk5MywxNi41MTgyIEMyNS45NTkzLDE2Ljk1MzIgMjYuMzU3MywxNi40NTQyIDI2Ljc1NDMsMTYuMDczMiBDMjcuMTgwMywxNS42MDIyIDI3LjQwMzMsMTUuOTA2MiAyNy40NTkzLDE1Ljk4NjIgQzI3LjcxOTMsMTYuMzU1MiAyNy4wOTQzLDE2Ljg4NDIgMjcuNTA0MywxNy4zMDkyIEMyNy4xODAzLDE3LjY1OTIgMjcuMDA5MywxNy44NTgyIDI2LjM0NDMsMTcuOTM0MiBDMjcuNTQxMywxOC40NTQyIDI4LjMxODMsMTcuODQ3MiAyOC40ODQzLDE3LjU3NzIgQzI4LjY0OTMsMTcuMzA3MiAyOC43MTUzLDE3LjE1MDIgMjguNzk5MywxNi44MDkyIEMyOS4wMzAzLDE1LjYwMjIgMzAuMzgyMywxNi44MjIyIDMxLjIzNDMsMTYuMzMzMiBDMzEuMjM0MywxNi4zMzMyIDMwLjg4ODMsMTYuMDU0MiAzMC42NDMzLDE1Ljg3NzIgQzMwLjM5NzMsMTUuNzAxMiAzMC42MTMzLDE1LjYwMjIgMzAuNjEzMywxNS42MDIyIEMzMS40MjAzLDE1LjkwNzIgMzIuNjMwMywxNi4zNjgyIDMzLjI5MDMsMTUuODQ4MiBDMzIuOTM4MywxNS42NTIyIDMzLjA3MDMsMTUuNzQ3MiAzMi42MzMzLDE1LjQ2MDIgQzMyLjQ3MjMsMTUuMzM2MiAzMi40ODczLDE1LjE0MDIgMzIuNjUxMywxNS4wNzUyIEMzMy4zOTkzLDE1LjUxMjIgMzQuMjg5MywxNS45NzkyIDM1LjIxMjMsMTUuNjE0MiBDMzQuOTQ3MywxNS41ODAyIDM0Ljc1MTMsMTUuMzc3MiAzNC4zNzEzLDE1LjAxMjIgQzM0LjEwNTMsMTQuNzU2MiAzNC40NDkzLDE0LjYxOTIgMzQuNDQ5MywxNC42MTkyIEMzNS4zMzEzLDE1LjM0MDIgMzYuNzc4MywxNS40ODkyIDM3LjkwNzMsMTUuMjYzMiBDMzcuNDc5MywxNC45MDIyIDM2LjgyMDMsMTUuMDc1MiAzNi41MDczLDE0LjQ3NTIgQzM3LjI2MTMsMTQuMzQwMiAzOC4zNjQzLDE1LjE0NTIgMzkuMDY3MywxNC41MDkyIEMzOC42MTgzLDE0LjA3MDIgMzguMDQ5MywxMy45MDEyIDM3LjUzNjMsMTMuNTk2MiBDMzQuMDM4MywxMy4yNzEyIDMxLjU3MTMsMTAuNTQ3MiAyOC4zNjgzLDkuNTEwMiBMMjguNTAxMyw5LjM4NDIgQzMxLjY3OTMsOS43MTgyIDMzLjg0OTMsMTIuMzg2MiAzNy4wMzYzLDEyLjcwMDIgQzM5LjczMTMsMTUuOTk0MiA0My43NjQzLDExLjE4NDIgNDYuNjEzMywxNC40NzUyIEM0Ny44MTYzLDE3LjA0NzIgNDQuNDQ0MywxNy4wODQyIDQzLjUwODMsMTguNzEwMiBDNDMuMTUwMywxOS4zMzAyIDQzLjQwODMsMjAuMDc5MiA0My44NDgzLDIwLjU4MTIgQzQzLjc4NTMsMjAuMDU4MiA0My45MTkzLDE5LjYxNDIgNDQuMzc4MywxOS4zMTIyIEM0Ni4wNjAzLDIwLjY2NzIgNDUuNjYzMywxOC4wODgyIDQ2LjgzMzMsMTcuNjcyMiBDNDcuNDk0MywxOC42NTQyIDQ2LjM4MjMsMjAuNDQzMiA0OC4wNzAzLDIwLjg5MjIgQzQ4LjQ4ODMsMjAuOTkxMiA0OS4wMzYzLDIxLjExODIgNDkuNjAzMywyMC43ODEyIEM0OS4xNjkzLDIwLjU3OTIgNDguNTE2MywyMC4zMzQyIDQ4LjQ2MDMsMTkuNzQ2MiBDNDkuNjgxMywxOC44NjMyIDQ3LjgwMzMsMTcuOTQ0MiA0OC42MzAzLDE3LjA2MzIgQzQ5LjEzODMsMTcuNTM1MiA0OS41ODkzLDE4LjE0MjIgNTAuMTUyMywxOC40ODcyIEM1MC44NjczLDE4LjkxODIgNTEuOTI5MywxOC40ODkyIDUyLjU4MDMsMTcuODc3MiBDNTIuOTM3MywxNy40NzQyIDUzLjE1MTMsMTYuOTQ1MiA1Mi44NjEzLDE2LjQ3ODIiIGlkPSJGaWxsLTI0IiBmaWxsPSIjRkZGRkZGIiBtYXNrPSJ1cmwoI21hc2stNCkiPjwvcGF0aD4gICAgICAgICAgICAgICAgICAgIDwvZz4gICAgICAgICAgICAgICAgPC9nPiAgICAgICAgICAgIDwvZz4gICAgICAgIDwvZz4gICAgPC9nPjwvc3ZnPg==',
				'rewrite'               => $rewrite
			);
			register_post_type( 'all-jfl-vacancies', $args );

      /* === END === */

      /* ===== Careers ===== */

			$labels = array(
				'name'                  => 'Vacancies',
				'singular_name'         => 'Vacancy',
				'menu_name'             => 'Vacancies',
				'name_admin_bar'        => 'Vacancies',
				'archives'              => 'Vacancies'
			);
			$rewrite = array(
				'slug'                  => 'working-in-finance/current-vacancies'
			);
			$args = array(
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'revisions', ),
				'hierarchical'          => false,
				'public'                => true,
				'has_archive'           => true,
				'publicly_queryable'    => true,
				'menu_icon'             => 'dashicons-portfolio',
				'rewrite'               => $rewrite
			);
			register_post_type( 'all-vacancies', $args );

      /* === END === */

      /* ===== Team ===== */

			$labels = array(
				'name'                  => 'Our Team',
				'singular_name'         => 'Our Team',
				'menu_name'             => 'Our Team',
				'name_admin_bar'        => 'Our Team',
				'archives'              => 'Our Team'
			);
			$rewrite = array(
				'slug'                  => 'our-team/%all-team-groups%'
			);
			$args = array(
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'revisions', ),
				'hierarchical'          => false,
				'public'                => true,
				'has_archive'           => false,
				'publicly_queryable'    => true,
				'menu_icon'             => 'dashicons-groups',
				'rewrite'               => $rewrite
			);
			register_post_type( 'all-team', $args );

      /* === END === */

      /* ===== Working Groups ===== */

			$labels = array(
				'name'                  => 'Working Groups',
				'singular_name'         => 'Working Group',
				'menu_name'             => 'Working Groups',
				'name_admin_bar'        => 'Working Groups',
				'archives'              => 'Working Groups'
			);
			$rewrite = array(
				'slug'                  => 'working-groups'
			);
			$args = array(
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'revisions', 'comments' ),
				'hierarchical'          => false,
				'public'                => true,
				'has_archive'           => false,
				'publicly_queryable'    => true,
				'menu_icon'             => 'dashicons-lock',
				'rewrite'               => $rewrite
			);
			register_post_type( 'all-working-groups', $args );

      /* === END === */

      /* ===== Timeline Items (output on taxonomy template) ===== */

			$labels = array(
				'name'                  => 'Timeline Items',
				'singular_name'         => 'Timeline Item',
				'menu_name'             => 'Timeline Items',
				'name_admin_bar'        => 'Timeline Items',
				'archives'              => 'Timeline Items'
			);
			$args = array(
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'revisions', ),
				'hierarchical'          => false,
				'public'                => true,
				'has_archive'           => false,
				'publicly_queryable'    => false,
				'menu_icon'             => 'dashicons-exerpt-view',
				'excluded_from_search'	=> true
			);
			register_post_type( 'all-timeline-items', $args );

      /* === END === */

      /* ===== AIFMD Map ===== */

			$labels = array(
				'name'                  => 'AIFMD Countries',
				'singular_name'         => 'AIFMD Country',
				'menu_name'             => 'AIFMD Countries',
				'name_admin_bar'        => 'AIFMD Countries',
				'archives'              => 'AIFMD Countries'
			);
			$args = array(
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'revisions', ),
				'hierarchical'          => false,
				'public'                => true,
				'has_archive'           => false,
				'publicly_queryable'    => false,
				'menu_icon'             => 'dashicons-location'
			);
			register_post_type( 'all-aifmd-countries', $args );

      /* ===== Fund Distribution Map ===== */

			$labels = array(
				'name'                  => 'Fund Distribution Map',
				'singular_name'         => 'Fund Distribution Map',
				'menu_name'             => 'Fund Distribution Map',
				'name_admin_bar'        => 'Fund Distribution Map',
				'archives'              => 'Fund Distribution Map'
			);
			$args = array(
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'revisions', ),
				'hierarchical'          => false,
				'public'                => true,
				'has_archive'           => false,
				'publicly_queryable'    => false,
				'menu_icon'             => 'dashicons-location'
			);
			register_post_type( 'all-fund-countries', $args );

      /* === END === */
   }

   add_action( 'init', 'custom_post_types', 0 );


	/* ==== Register Custom Taxonomies ==== */
	function custom_taxonomies() {

		/* ==== SHARED TAXONOMIES ==== */

			/* ===== Authors ===== */
				$labels = array(
					'name'                  => 'Authors',
					'singular_name'         => 'Author',
					'menu_name'             => 'Authors',
					'name_admin_bar'        => 'Authors',
					'archives'              => 'Authors'
				);
				$args = array(
					'labels'					=> $labels,
					'hierarchical'			=> true,
					'query_var' 			=> true,
					'public'					=> false,
					'show_ui'				=> true,
					'show_admin_column'	=> true,
					'show_in_nav_menus'	=> true,
					'show_tagcloud'		=> false,
				);
				register_taxonomy( 'all-authors', array('all-news', 'all-events'), $args );

		/* === END SHARED === */

		/* ==== NEWS ==== */

			/* ===== Types ===== */
				$labels = array(
					'name'                  => 'Types',
					'singular_name'         => 'Type',
					'menu_name'             => 'Types',
					'name_admin_bar'        => 'Types',
					'archives'              => 'Types'
				);
				$args = array(
					'labels'					=> $labels,
					'hierarchical'			=> true,
					'query_var' 			=> true,
					'public'					=> false,
					'show_ui'				=> true,
					'show_admin_column'	=> true,
					'show_in_nav_menus'	=> true,
					'show_tagcloud'		=> false,
				);
				register_taxonomy( 'all-news-types', array('all-news'), $args );

			/* ===== Topics ===== */
				$labels = array(
					'name'                  => 'Topics',
					'singular_name'         => 'Topic',
					'menu_name'             => 'Topics',
					'name_admin_bar'        => 'Topics',
					'archives'              => 'Topics'
				);
				$args = array(
					'labels'					=> $labels,
					'hierarchical'			=> true,
					'query_var' 			=> true,
					'public'					=> true,
					'show_ui'				=> true,
					'show_admin_column'	=> true,
					'show_in_nav_menus'	=> true,
					'show_tagcloud'		=> false,
					'rewrite'				=> array(
						'slug' => 'spotlight-on'
					)
				);
				register_taxonomy( 'all-topics', array('all-news', 'all-events', 'all-work'), $args );

		/* === END NEWS === */

		/* ==== EVENTS ==== */

			/* ===== Locations ===== */
				$labels = array(
					'name'                  => 'Locations',
					'singular_name'         => 'Location',
					'menu_name'             => 'Locations',
					'name_admin_bar'        => 'Locations',
					'archives'              => 'Locations'
				);
				$args = array(
					'labels'					=> $labels,
					'hierarchical'			=> true,
					'query_var' 			=> true,
					'public'					=> false,
					'show_ui'				=> true,
					'show_admin_column'	=> true,
					'show_in_nav_menus'	=> true,
					'show_tagcloud'		=> false,
				);
				register_taxonomy( 'all-event-locations', array('all-events'), $args );

		/* === END EVENTS === */

		/* ==== WORK ==== */

			/* ===== Types ===== */
				$labels = array(
					'name'                  => 'Types',
					'singular_name'         => 'Type',
					'menu_name'             => 'Types',
					'name_admin_bar'        => 'Types',
					'archives'              => 'Types'
				);
				$args = array(
					'labels'					=> $labels,
					'hierarchical'			=> true,
					'query_var' 			=> true,
					'public'					=> false,
					'show_ui'				=> true,
					'show_admin_column'	=> true,
					'show_in_nav_menus'	=> true,
					'show_tagcloud'		=> false,
				);
				register_taxonomy( 'all-work-types', array('all-work'), $args );

		/* === END WORK === */

		/* ==== BUSINESSES ==== */

			$labels = array(
				'name'                  => 'Categories',
				'singular_name'         => 'Category',
				'menu_name'             => 'Categories',
				'name_admin_bar'        => 'Categories',
				'archives'              => 'Categories'
			);
			$args = array(
				'labels'					=> $labels,
				'hierarchical'			=> true,
				'query_var' 			=> true,
				'public'					=> true,
				'show_ui'				=> true,
				'show_admin_column'	=> true,
				'show_in_nav_menus'	=> true,
				'show_tagcloud'		=> false,
				'rewrite'				=> array(
					'slug' => 'business-categories'
				)
			);
			register_taxonomy( 'all-business-categories', array('all-businesses'), $args );

		/* === END BUSINESSES === */

		/* ==== TEAM ==== */

			// Groups
			$labels = array(
				'name'                  => 'Groups',
				'singular_name'         => 'Group',
				'menu_name'             => 'Groups',
				'name_admin_bar'        => 'Groups',
				'archives'              => 'Groups'
			);
			$args = array(
				'labels'					=> $labels,
				'hierarchical'			=> true,
				'query_var' 			=> true,
				'public'					=> true,
				'show_ui'				=> true,
				'show_admin_column'	=> true,
				'show_in_nav_menus'	=> true,
				'show_tagcloud'		=> false,
				'rewrite'				=> array(
					'slug' => 'our-team'
				)
			);
			register_taxonomy( 'all-team-groups', array('all-team'), $args );
			
			// Teams
			$labels = array(
				'name'                  => 'Teams',
				'singular_name'         => 'Team',
				'menu_name'             => 'Teams',
				'name_admin_bar'        => 'Teams',
				'archives'              => 'Teams'
			);
			$args = array(
				'labels'					=> $labels,
				'hierarchical'			=> true,
				'query_var' 			=> true,
				'public'					=> false,
				'show_ui'				=> true,
				'show_admin_column'	=> true,
				'show_in_nav_menus'	=> true,
				'show_tagcloud'		=> false,
			);
			register_taxonomy( 'all-team-teams', array('all-team'), $args );

		/* === END TEAM === */

		/* ==== VACANCIES ==== */

			$labels = array(
				'name'                  => 'Types',
				'singular_name'         => 'Category',
				'menu_name'             => 'Types',
				'name_admin_bar'        => 'Types',
				'archives'              => 'Types'
			);
			$args = array(
				'labels'					=> $labels,
				'hierarchical'			=> true,
				'query_var' 			=> true,
				'public'					=> false,
				'show_ui'				=> true,
				'show_admin_column'	=> true,
				'show_in_nav_menus'	=> true,
				'show_tagcloud'		=> false,
			);
			register_taxonomy( 'all-vacancy-types', array('all-vacancies'), $args );

			$labels = array(
				'name'                  => 'Terms',
				'singular_name'         => 'Term',
				'menu_name'             => 'Terms',
				'name_admin_bar'        => 'Terms',
				'archives'              => 'Terms'
			);
			$args = array(
				'labels'					=> $labels,
				'hierarchical'			=> true,
				'query_var' 			=> true,
				'public'					=> false,
				'publicly_queryable'	=> false,
				'show_ui'				=> true,
				'show_admin_column'	=> true,
				'show_in_nav_menus'	=> true,
				'show_tagcloud'		=> false,
			);
			register_taxonomy( 'all-vacancy-terms', array('all-vacancies'), $args );

		/* === END TEAM === */

		/* === TIMELINE ITEMS === */
		
			/* ===== Timelines ===== */
				$labels = array(
					'name'                  => 'Timelines',
					'singular_name'         => 'Timeline',
					'menu_name'             => 'Timelines',
					'name_admin_bar'        => 'Timelines',
					'archives'              => 'Timelines'
				);
				$args = array(
					'labels'					=> $labels,
					'hierarchical'			=> true,
					'public'					=> true,
					'show_ui'				=> true,
					'show_admin_column'	=> true,
					'show_in_nav_menus'	=> true,
					'show_tagcloud'		=> false,
					'rewrite'				=> array(
						'slug' => 'timelines'
					)
				);
				register_taxonomy( 'all-timelines', array('all-timeline-items'), $args );
		
		/* === END TIMELINE ITEMS === */


	}
	add_action( 'init', 'custom_taxonomies', 0 );
?>