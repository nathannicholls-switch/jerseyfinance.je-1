<?php amp_header_core() ?>

<div style="display: block; visibility: hidden; height: 0; width: 0;">
	<? 
		// Output SVG sprite	
		readfile( get_template_directory_uri() . '/dist/images/sprite.svg' ); 
	?>
</div>

<div class="c-breadcrumbs red">
	<? amp_breadcrumb();?>
</div>

<? echo get_template_part('includes/header'); ?>

<?php amp_menu(); ?>

<div class="content-wrapper container">
