<?php amp_header(); ?>

<div class="u-align-centre">
	<?php amp_title(); ?>
</div>

<div class="main-content">
	<? include( locate_template( '/includes/flexible-content.php' ) ); ?>

	<? include( locate_template('includes/singles/related-posts.php') ); ?>

</div>
<?php amp_footer()?>