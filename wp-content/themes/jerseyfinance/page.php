<? if( !request_is_AJAX() ): ?>
	<? include( locate_template('/includes/head.php') ); ?>
<? endif; ?>

<script>
	// Return user favourites as a JS array so we can check if the current post is in their saved items
	var user_favourites = <? echo json_encode( fetch_favourites() ); ?>;
</script>

<? if( user_can_view( get_field('members_only') ) ): ?>

	<?
		while ( have_posts() ) : the_post(); 
		
			if( locate_template('/template-pages/page-' . $post->post_name . '.php') ):
				include( locate_template('/template-pages/page-' . $post->post_name . '.php') );
			else:
				include( locate_template('/template-pages/page.php') );
			endif;

		endwhile;
	?>

<? else: ?>

	<? include( locate_template('includes/partials/login-form.php') ); ?>

<? endif; ?>

<? if( !request_is_AJAX() ): ?>
	<? include( locate_template('/includes/footer.php') ); ?>
<? endif; ?>