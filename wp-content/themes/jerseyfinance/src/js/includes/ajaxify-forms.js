// Must pass `this` in as parameter so it knows which form we are dealing with
function updateWithAjax(context) {
	// Get form action and serialise the form inputs (returns all the input values for use in the query string)
	var action 			= $(context).attr('action');
	var searchParams 	= $(context).serialize();
	
	/*
		Note about fix for Chrome issue:
		The problem here is that when you do a history.back() after performing a search with AJAX, you get the cached XMLHttpRequest response from the previously run AJAX request.
	
		The cached XMLHttpRequest header when going back via the browser means our header & footer isn't loaded as the server thinks it's an AJAX request.

		By having seperate, unique URLs for the AJAX request and history state, the requests are seperately cached and we don't get header overwrite issues when navigating in the browser.

		Sauce: https://goo.gl/BG8kWE
	*/
	var ajaxUrl    = action + '?' + searchParams + '&ajax=true';
	var historyUrl = action + '?' + searchParams;

	ajaxLoading = $.ajax({
		'url': 	ajaxUrl,
		cache: 	true
	})
	.done(function(data) {
		var pagination = $('#ajax-container .pagination-container');

		console.log( $(data) );

		// Add the grid itmes the ajax item container
		$('#ajax-container .item-container').html( $(data).filter('div').not('.pagination, .topics') );

		// If the topics element already exists, replace it, if not, create it
		if( $(data).filter('.topics').length ) {
			if( $('.topics').length ) {
				$('#ajax-container .topics').html( $(data).filter('.topics').html() );
			} else {
				$('#ajax-container').prepend( $(data).filter('.topics')[0].outerHTML );
			}
		} else {
			$('#ajax-container .topics').remove();
		}

		// Remove old pagination once finished
		if( $(data).filter('.pagination').length ) {
			pagination.html( $(data).filter('.pagination')[0].outerHTML );
			//console.log('New pagination');
		} else {
			pagination.find('.pagination').remove();
			//console.log('Out of posts, remove pagination');
		}

		// Add our history URL to the history state so back buttons and refreshes return us to the same search results
		history.pushState(historyUrl, '', historyUrl);

		// Push to GTM so it registers as a pageview or something
		// Make sure this fires after history.pushState otherwise it can't track the new URL
		dataLayer.push({
			'event':'search',
			'searchurl':historyUrl
		});
		
		// Refire countdown timers if they exist
		if( typeof start_countdown_timers == 'function' ) {
			start_countdown_timers();
		}
	});

	if( $('#events-calendar').length ) {
		// Remove all events from the calendar
		$('#events-calendar').fullCalendar( 'removeEventSources' );

		// Grab the new filters from the form and fetch new JSON
		var new_json_url = '/json/events' + '?' + $(context).serialize();

		// Add new json URL into Full Calendar as an event source
		$('#events-calendar').fullCalendar( 'addEventSource', new_json_url );
	}
}

$(function() {

	// Prevent ajax forms from being submitted with enter + do ajax once the user stops typing
		var typingTimer;
		var typingTimeout = 700;

		$('form.enable-ajax input[type="text"]').on('keydown', function(e) {		
			var targetForm = $(this).closest('form.enable-ajax');

			// Don't let user submit with enter
				var keyCode = e.keyCode || e.which;
				
				if (keyCode === 13) { 
					e.preventDefault();
					return false;
				}

			// Clear the timeout when a key is pressed
				clearTimeout(typingTimer);

			// If the typing timeout has expired, run the ajax update function
				typingTimer = setTimeout(function() {			
					updateWithAjax(targetForm); // pass in form element
				}, typingTimeout);
		});

	// When an input is changed, run the ajax function (except for keyword as we run ajax after timeout above)
		$('form.enable-ajax input').not('.c-filters__bar-keyword input[type="text"]').change( function() {
			var targetForm = $(this).closest('form.enable-ajax');

			updateWithAjax(targetForm);

			return false;
		});


		$('.c-filters__modifiers-sort a').click(function(){
			var targetForm = $(this).closest('form.enable-ajax');		
			
			// Toggle highlight class on the buttons
			$('.c-filters__modifiers-sort a').removeClass('selected');
			$(this).addClass('selected');

			// Update value of hidden field
			var sortValue = $(this).data('value');
			$('#sort-by').val(sortValue);

			// Reload the results with the new paramters
			updateWithAjax(targetForm);

			return false;
		});
	
});