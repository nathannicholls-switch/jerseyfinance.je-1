function start_countdown_timers() {
	if( $('.c-countdown').length ) {

		$('.c-countdown').each(function() {
			var target = $(this);
			var dateTime = $(this).data('date');

			// Set the date we're counting down to (multiply UNIX by 1000 so we are dealing with milliseconds)
			var countDownDate = new Date(dateTime*1000).getTime();

			// Update the count down every 1 second
			var x = setInterval(function() {

				// Get todays date and time
				var now = new Date().getTime();

				// Find the distance between now and the count down date
				var distance = countDownDate - now;

				// Time calculations for days, hours, minutes and seconds (as strings)
				var days = '' + Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = '' + Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = '' + Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = '' + Math.floor((distance % (1000 * 60)) / 1000);


				// If the value changes, update elements with new time (this prevents repainting)
				if( target.find('.c-countdown__days .value').html() != days ) {	
					target.find('.c-countdown__days .value').html(days);
				}

				if( target.find('.c-countdown__hours .value').html() != hours ) {	
					target.find('.c-countdown__hours .value').html(hours);
				}
				
				if( target.find('.c-countdown__minutes .value').html() != minutes ) {	
					target.find('.c-countdown__minutes .value').html(minutes);
				}
				
				if( target.find('.c-countdown__seconds .value').html() != seconds ) {	
					target.find('.c-countdown__seconds .value').html(seconds);
				}

				// If the count down is finished, stop timer and hide countdown
				if (distance < 0) {
					clearInterval(x);
					target.hide();
				}
			}, 1000);
		});
	}
}

$(function() {
	start_countdown_timers();
});