$(function() {

	$('.content-toggle .c-tabs__tab').click(function() {
		var target = $(this).data('target');

		if( !$(this).hasClass('active') ) {

			// Toggle tab highlighting
			$('.c-tabs__tab').removeClass('active');
			$(this).addClass('active');

			// Hide all content areas except the target
			$('.c-tabs__content').not(target).fadeOut(400);

			// Show the target
			$('#' + target).fadeIn(400, function() {
				// Re-fire any existing isotope layouts as they don't render correctly if they are hidden on load
				//$grid.isotope('layout');
			});
		}
	});

});