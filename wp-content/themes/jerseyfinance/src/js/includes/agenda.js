/* ==== Next and previous buttons ==== */

	function changeSlide( activeSlide, targetSlide ) {

		var slideTitle 	= targetSlide.data('title');

		if( targetSlide.length ) {
			$('.c-agenda__day-box .day').html(slideTitle);

			// Hide the current day
			activeSlide.removeClass('active');
			activeSlide.slideUp(function() {
				// Show the next day
				targetSlide.addClass('active');
				targetSlide.slideDown();
			});	
		}

		// Next button highlighting
		if( targetSlide.next('.c-agenda__slide').length ) {
			$('.c-agenda .next').removeClass('disabled');
		} else {
			$('.c-agenda .next').addClass('disabled');
		}

		// Previous button highlighting
		if( targetSlide.prev('.c-agenda__slide').length ) {
			$('.c-agenda .prev').removeClass('disabled');
		} else {
			$('.c-agenda .prev').addClass('disabled');
		}		

	}

	$(function() {
		$('.c-agenda .next').click(function() {
			var activeSlide 	= $(this).closest('.c-agenda__slider').find('.c-agenda__slide.active');
			var nextSlide 		= activeSlide.next('.c-agenda__slide');

			changeSlide( activeSlide, nextSlide );
		});

		$('.c-agenda .prev').click(function() {
			var activeSlide 	= $(this).closest('.c-agenda__slider').find('.c-agenda__slide.active');
			var prevSlide 		= activeSlide.prev('.c-agenda__slide');

			changeSlide( activeSlide, prevSlide );
		});
	});

/* === END === */

/* ==== Add the click event for the "More" buttons, toggles height and labels ==== */

	$(function() {
		$('.c-agenda__description-box').each(function() {
			var textHeight 	=  $(this).find('.text-height').outerHeight(true);
			var targetHeight 	= $(this).find('.c-agenda__text').outerHeight()

			if( textHeight <= targetHeight ) {
				$(this).find('.c-agenda__more').hide();
			}
		});
	});

	$('.c-agenda__more').click(function() {
		
		// Get hidden text height, including margins
		var textHeight = $(this).parent().find('.text-height').outerHeight(true);

		// Animate the max-height attribute on the target text element so we can essentially animate to "auto"
		var targetText = $(this).prev();	

		// If the parent container smaller than the text element, expand otherwise shrink back to default max-height
		if( targetText.outerHeight() < textHeight ) {

			// Animate max-height
			targetText.animate({
				'max-height': textHeight + 'px'
			}, 400, function() {
				// Once the animation is done, remove max-height in case the browser width changes
				targetText.css('max-height', 'none');
			});
		
		} else {
			// Explicitly set max-height to be the current height so that we can animate it
			targetText.css('max-height', textHeight + 'px');

			var maxHeight = $(window).width() > 600 ? '93px' : '80px';

			// Animate max-height
			targetText.animate({
				'max-height': maxHeight
			}, 400);
		}
		
		// Toggle the open class - needed to animate arrow
		$(this).parent().toggleClass('open');
		
		// Change the label from more to less and vice-versa
		var label = $(this).find('.label');
		label.html( label.text() == 'More' ? 'Less' : 'More' );
	});

/* === END === */