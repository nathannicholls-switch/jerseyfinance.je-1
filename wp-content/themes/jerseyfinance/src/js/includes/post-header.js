// This entire function tracks all elements positions and heights on scroll, so it will need refiring if more are loaded in with ajax
function trackArticleProgress(elements) {
	var loaded_count = 1;
	var ajax_active = false;

	$(window).scroll(function() {
		// Roughly 2 thirds of the way down the page
		var scrollPosition 	= $(document).scrollTop() + ($(window).height() * 0.75);

		$(elements).each(function() {
			var article = $(this);

			// Article positioning
			var articleHeight 	= article.outerHeight();
			var articleTop 		= article.offset().top;
			var articleBottom 	= article.offset().top + articleHeight;
			
			// Current article title & permalink (for ajax loading next posts)
			var articleTitle 		= article.data('title');
			var currentPost  		= article.data('permalink');
			var currentPostID  	= article.data('post-id');

			// if the bottom of the header is between the start and end of the article, then that article is active and needs tracking
			if( scrollPosition > articleTop && scrollPosition < articleBottom ) {
				// Update post title in post header
				$('.post-header__title').html(articleTitle);

				change_favourite_state(currentPostID);

				// Push the current post URL to the history
				if(history.state != currentPost) {
					history.replaceState(currentPost, '', currentPost);
				}

				// Calculate progress based on how far down the middle of the screen is on the article, multiply by 100 to form a percentage
				var progress = Math.ceil(((scrollPosition - articleTop) / articleHeight) * 100);

				// Apply percentage to progress bar
				$('.progress').css('width', progress + '%');
			}


			// Load next article with ajax if it has the load-next class
			if( article.hasClass('load-next') ) {

				var ajaxContainer				= $('.infinite-scroll-container');
				var ajaxContainerBottom 	= ajaxContainer.offset().top + ajaxContainer.outerHeight() - 100;

				// If the user is at the bottom, the ajax is NOT currently running and we have loaded less than 5 extra articles
				if( scrollPosition > ajaxContainerBottom && !ajax_active && loaded_count <= 5 ) {
					ajax_active = true;

					var target_url = next_article;
					// console.log(target_url);

					// Run the ajax request
					ajaxLoading = $.ajax({
						url: next_article  + '?ajax=true',
						dataType: 'html',
						cache: true
					})
					.done(function (data) {
						// Increase our count, so that we can only load 5
						loaded_count++;

						$('.infinite-scroll-container').append(data);

						// Refresh Animate on Scroll so the new elements will animate in
						AOS.refreshHard();	

						ajax_active = false;

						// Push to GTM so it registers as a pageview or something
						// Make sure this fires after history.pushState otherwise it can't track the new URL
						dataLayer.push({
							'event':'infinite_news_view',
							'searchurl':target_url
						});
					});
				}
			}
		});
	});
}

$(function() {
	if( $('article.track-progress').length ) {
		// On load track all on-screen article progress
		trackArticleProgress('article.track-progress');
	}
});