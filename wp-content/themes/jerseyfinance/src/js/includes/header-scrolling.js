function addScrollClasses(elements) {
	var previousScroll 	= 0;
	
	// Controls how sensitive the scroll toggle is
	var upScrollStep 		= 80; // Scrolling up
	var downScrollStep 	= 15; // Scrolling down

	$(window).scroll(function() {
		var currentScroll = $(document).scrollTop();

		// Set previous scroll to 0 if it's not already defined
		if (typeof previousScroll === 'undefined') {
			previousScroll = 0;
		}

		// console.log(previousScroll + ' -> ' + currentScroll);

		var upperBounds = (previousScroll + downScrollStep);
		var lowerBounds = (previousScroll - upScrollStep); 

		// console.log('Currently at: ' + currentScroll);
		// console.log('Hide header at: ' + upperBounds);
		// console.log('Show header at: ' + lowerBounds);

		// If the scroll step is greater than 5 then set the new scroll variable
		if( currentScroll > $('.site-header').outerHeight() && currentScroll >= upperBounds || currentScroll <= lowerBounds || currentScroll <= 0 ) {		
			
			// Toggle header classes depending on whether ot not the user scrolls
			if( currentScroll < upperBounds || currentScroll <= 0 ) {
				// The user has scrolled down
				// console.log('Up');

				// Toggle classes
				elements.addClass('scroll-up').removeClass('scroll-down');
			} else if ( currentScroll > lowerBounds) {
				// The user has scrolled down
				// console.log('Down');

				// Toggle classes
				elements.addClass('scroll-down').removeClass('scroll-up');
			}

			// Set new previous scroll position
			previousScroll = currentScroll;
		}	
	});
}

$(function() {
	setTimeout(function() {
	// When the user scrolls up/down add a hide class to the following:
	addScrollClasses($('body'));
	}, 1000)
});
