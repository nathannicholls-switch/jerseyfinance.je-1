$(function() {

	/* ==== Main slide-out menu ==== */

		// Hamburger and close icons
		$('.menu-toggle').click(function(e) {
			// Toggle open classes
			$('body').toggleClass('u-overflow-hidden');
			$('.site-menu, .site-overlay').toggleClass('open');
		});

		// Search
		$('.site-search-toggle').click(function(e) {
			// Toggle open classes
			$('body').toggleClass('u-overflow-hidden');
			$('.site-search, .site-overlay').toggleClass('open');

			// Wait until the animation has finished, then apply focus to the input
			setTimeout( function() {
				$('.site-search__search-wrapper input')[0].focus();				
			}, 600);			
		});

		// When someone clicks outside of the open menu
		$('.site-menu, .site-search').click(function(e) {
			// Get the clicked element
			var target = $(e.target);

			// Only close menu if the parent menu div was clicked, not the children
			if( target.is($(this)) ) {

				// Remove open classes
				$('body').removeClass('u-overflow-hidden');
				$('.site-menu, .site-search, .site-overlay').removeClass('open');
			}
		});

	/* === END === */

	/* ==== Main menu subnav toggles ==== */

		$('.site-menu .expand').click(function() {
			// Get the subnav for this menu item
			var target = $(this).siblings('ul.sub-menu');

			/* 
				** If the menu is already open, remove the active class if it exists and add the reset-rotate class 
				** which will over-ride the current-menu-item selector styles we use to determine whether or not the 
				** menu needs to be displayed on load.
			*/

			if( target.is(':visible') ) {
				// Remove arrow styles applied by menu parent classes
				$(this).addClass('reset-rotate')
				
				// Remove active styles for arrow
				$(this).removeClass('active');
			} else {
				// Add active styles for arrow
				$(this).addClass('active');
			}

			// Show/hide subnav
			target.slideToggle(300);
		});

	/* === END === */

});