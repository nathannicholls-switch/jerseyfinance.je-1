//@prepros-prepend 'plugins/jquery.min.js'

//@prepros-prepend 'includes/min/get-cookie.min.js'
//@prepros-prepend 'includes/min/notices.min.js'

//@prepros-prepend 'includes/min/add-to-favourites.min.js'

//@prepros-prepend 'includes/min/menu.min.js'
//@prepros-prepend 'includes/min/header-scrolling.min.js'
//@prepros-prepend 'includes/min/post-header.min.js'
//@prepros-prepend 'includes/min/header-anchor-links.min.js'

//@prepros-prepend 'includes/min/video-muting.min.js'
//@prepros-prepend 'includes/min/content-tabs.min.js'

//@prepros-prepend 'includes/min/accordion.min.js'
//@prepros-prepend 'includes/min/agenda.min.js'
//@prepros-prepend 'includes/min/countdown.min.js'

//@prepros-prepend 'includes/min/ajaxify-forms.min.js'
//@prepros-prepend 'includes/min/filters.min.js'
//@prepros-prepend 'includes/min/pagination.min.js'

//@prepros-prepend 'includes/min/smooth-scroll.min.js'

//@prepros-prepend 'plugins/aos.min.js'

// Init AOS
$(function() {
	AOS.init({
		duration: 	650,
		delay: 		100,
		offset:		50
	});
});

//@prepros-prepend 'plugins/simple-lightbox.min.js'
//@prepros-prepend 'plugins/slick.min.js'
//@prepros-prepend 'plugins/flatpickr.min.js'
//@prepros-prepend 'plugins/object-fit-polyfill.min.js'

//@prepros-prepend 'plugins/masonry.min.js'

//@prepros-prepend 'plugins/algolia/algoliasearchLite.min.js'
//@prepros-prepend 'plugins/algolia/instantsearch.production.min.js'
//@prepros-prepend 'plugins/algolia/algolia.min.js'



// Fix issue with Gravity Forms retaining thumbnails on file upload fields inside list items (when new list items are added)
$(function() {
	if( typeof gform !== 'undefined' ) {
		// When a new row is added
		gform.addAction( 'gform_list_post_item_add', function ( item, container ) {
			
			// Replace the new row with a copy of our blank one from the bottom
			item.html( container.find('.gfield_list_group').last().html() );

		});	
	}
});