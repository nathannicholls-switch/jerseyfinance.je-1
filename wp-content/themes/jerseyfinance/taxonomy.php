<? if( !request_is_AJAX() ): ?>

	<? include( locate_template('/includes/head.php') ); ?>

<? endif; ?>

<?
	// Include a file that pushes information about the current post to the datalayer
	include( locate_template('functions/custom-seo/post-datalayer.php') ); 
?>

	<?
      // Check if there is a custom taxonomy template for this post type, otherwise just default to taxonomy.php
		if( locate_template('/template-taxonomies/taxonomy-' . get_queried_object()->taxonomy . '.php') ):
			include( locate_template('/template-taxonomies/taxonomy-' . get_queried_object()->taxonomy . '.php') );
		else:
			include( locate_template('/template-taxonomies/taxonomy.php') );
		endif;
	?>

<? if( !request_is_AJAX() ): ?>

	<? include( locate_template('/includes/footer.php') ); ?>

<? endif; ?>