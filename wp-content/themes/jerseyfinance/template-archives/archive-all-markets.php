<?
	// Will search for a page that matches the archive slug and will pull header content from it
	include( locate_template('/includes/archives/archive-header.php') ); 
?>

<div class="main-content u-padding-top-0 u-padding-bottom-0">
	<div class="site-wrapper">

		<div class="c-world-map">
			<div class="container wide">
				<div class="c-world-map__wrapper">
					<img src="<? echo get_stylesheet_directory_uri(); ?>/dist/images/world-map.png" alt="Jersey Finance Markets" />

						<? while( have_posts() ): the_post(); ?>
							<? setup_postdata($post); ?>

							<a class="location  <? echo $post->post_name; ?>" href="<? echo get_the_permalink(); ?>">
								<svg class="ring">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ring"></use>
								</svg>
							</a>
						<? endwhile; ?>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="c-title">
				Key markets
			</div>
		</div>

		<div class="container">
			<div class="c-link-grid">
				<div class="c-grid ontablet-portrait-make-col-4 onmobile-make-col-12">

					<? while( have_posts() ): the_post(); ?>
						<? setup_postdata($post); ?>

						<div class="c-grid__col-3 c-link-grid__item">

							<a href="<? the_permalink(); ?>">
								<img class="u-margin-bottom-30" style="max-width: 180px; width: 90%;" src="<? echo get_field('header_image')['sizes']['two-column-header-image']; ?>" alt="Jersey Finance" />
							</a>
					
							<a class="c-link-grid__link u-font-family-bright u-font-weight-bold  u-margin-bottom-10" href="<? the_permalink(); ?>">
								<? the_title() ?>&nbsp;›
							</a>

							<? echo get_field('summary') ? '<div class="c-link-grid__text u-font-size-small">' . get_field('summary') . '</div>' : '' ; ?>
						
						</div>

					<? endwhile; ?>
					<? wp_reset_postdata(); ?>
				
				</div>
			</div>
		</div>	

	</div>
</div>