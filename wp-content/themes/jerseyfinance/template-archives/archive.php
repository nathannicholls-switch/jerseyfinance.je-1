<? if( !request_is_AJAX() ): ?>

	<?
		// Will search for a page that matches the archive slug and will pull header content from it
		include( locate_template('/includes/archives/archive-header.php') ); 
	?>

	<div class="main-content u-padding-bottom-0">
		<div class="site-wrapper">

			<? 
				// Will search for a page that matches the archive slug and will pull page content from it
				include( locate_template('/includes/archives/archive-page-content.php') ); 
			?>

			<? 
				// get_template_partial( 'includes/archives/filters', array(
				// 	'date' 			=> true,
				// ));
			?>

			<div class="container">

				<? if( !have_posts() ): ?>
					<h4 class="u-align-centre u-margin-bottom-70">Sorry, there are currently no posts.</h4>
				<? endif; ?>

				<div id="ajax-container" class="c-feature-grid c-feature-grid__grid-view">

<? endif; ?>

						<? // If the request is AJAX make sure no items appear large
							$i = !request_is_AJAX() ? 0 : 10;

							if( have_posts() ):
								while( have_posts() ): the_post();
									setup_postdata($post);
									
									include( locate_template('/includes/archives/post-item.php') );

									$i++;
								endwhile;
							endif;
						?>

<? if( !request_is_AJAX() ): ?>

					</div>

					<? 
						if( have_posts() ):
							include( locate_template('/includes/archives/pagination.php') ); 
						endif;
					?>
			</div>
		</div>
	</div>

<? endif; ?>