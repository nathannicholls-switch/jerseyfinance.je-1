<? if( !request_is_AJAX() ): ?>
	<? include( locate_template('/includes/head.php') ); ?>
<? endif; ?>

<script>
	// Return user favourites as a JS array so we can check if the current post is in their saved items
	var user_favourites = <? echo json_encode( fetch_favourites() ); ?>;
</script>

<? include( locate_template('includes/partials/login-form.php') ); ?>

<? if( !request_is_AJAX() ): ?>
	<? include( locate_template('/includes/footer.php') ); ?>
<? endif; ?>