<? if( !request_is_AJAX() ): ?>

	<? include( locate_template('/includes/head.php') ); ?>

<? endif; ?>

<script>
	// Return user favourites as a JS array so we can check if the current post is in their saved items
	var user_favourites = <? echo json_encode( fetch_favourites() ); ?>;
</script>

<?
	// Include a file that pushes information about the current post to the datalayer
	include( locate_template('functions/custom-seo/post-datalayer.php') ); 
?>

<? if( user_can_view( get_field('members_only') ) ): ?>
	
	<?
		while ( have_posts() ) : the_post(); 
		
			// Check if there is a custom single template for this post type, otherwise just defualt to single.php
			if( locate_template('/template-singles/single-' . get_post_type() . '.php') ):
				include( locate_template('/template-singles/single-' . get_post_type() . '.php') );
			else:
				include( locate_template('/template-singles/single.php') );
			endif;

			// Increment view count for the current post
			setPostViews( get_the_ID() ); 

		endwhile;
	?>

<? else: ?>

	<? include( locate_template('includes/partials/login-form.php') ); ?>

<? endif; ?>

<? if( !request_is_AJAX() ): ?>

	<? include( locate_template('/includes/footer.php') ); ?>

<? endif; ?>