<? 
	$header_template = locate_template( '/includes/post-headers/two-column-header.php' );

	if( $header_template ):
		include( $header_template );
	endif; 
?>

<div class="main-content">
	<div class="site-wrapper">

		<? include( locate_template('/includes/flexible-content.php') ); ?>
	
	</div>
</div>