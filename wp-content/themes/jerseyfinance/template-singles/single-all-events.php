<article class="track-progress" 
	data-title="<? the_title(); ?>"
	data-permalink="<? the_permalink(); ?>"
	data-post-id="<? echo get_the_ID(); ?>">

	<? 
		$header_template = locate_template( '/includes/post-headers/' . get_field('header_style') . '-header.php' );

		if( $header_template ):
			include( $header_template );
		endif; 
	?>

	<?
		$tabs['Overview'] = 'overview';

		if( have_rows('event_fields') ):
			while ( have_rows('event_fields') ) : the_row();
				if( get_row_layout() == 'logo_grid' ):
					$tabs['Sponsors'] = 'sponsors';
				elseif( get_row_layout() == 'google_map' ):
					$tabs['Venue'] 	= 'venue';
				elseif( get_row_layout() == 'contacts' ): 
					$tabs['Key Contacts'] = 'key-contacts';
				elseif( get_row_layout() == 'people' ): 
					$tabs['Speakers'] = 'speakers';
				elseif( get_row_layout() == 'agenda' ): 
					$tabs['Agenda'] = 'agenda';
				endif;
			endwhile;
		endif;

		// Include content tabs bar using the above tabs
		get_template_partial( 'includes/singles/content-tabs', array(
			'tabs'		=> $tabs,
			'anchors' 	=> true
		)); 
	?>

	<? 
		// Events have the option to shift the header content into the body, for images with text on them, so output here if that option is selected
		if( get_field('move_header_content') ):
			include_breadcrumbs( 'red' );

			echo '<div class="container u-align-centre u-margin-top-40 u-margin-bottom-60">';
				include( locate_template('/includes/post-headers/header-content.php') );
			echo '</div>';
		endif; 
	?>

	<div class="main-content u-padding-top-60">
		<div class="site-wrapper">

			<?
				$booking_url = get_field('booking_url');

				if( $booking_url ):

					$booking_url = add_http($booking_url); ?>
			
					<div class="container">
						<div class="u-pattern-wrapper fill u-padding-25 u-margin-bottom-50">
							<div class="u-position-relative">
								<a href="<? echo $booking_url; ?>" class="c-button red solid u-margin-bottom-0 u-margin-right-10" target="_blank">Book Now</a>

								<a class="u-display-inline-block u-margin-top-10 u-margin-bottom-10 u-margin-right-10 u-font-size-small" href="/event-booking-terms-conditions/" target="_blank">Terms and Conditions</a>

								<a class="u-display-inline-block u-margin-top-10 u-margin-bottom-10 u-font-size-small" href="/privacy-policy" target="_blank">Privacy Policy</a>
							</div>
						</div>

					</div>
				
				<? endif; 
			?>

			<div id="overview">
				<? include( locate_template('/includes/flexible-content.php') ); ?>
			</div>

			<? 
				if( have_rows('event_fields') ):
					while ( have_rows('event_fields') ) : the_row();

						if( get_row_layout() == 'text' ): 

							echo '<div id="overview">';						
								include( locate_template('/includes/components/flexible-content/c-text.php' ) );
							echo '</div>';

						elseif( get_row_layout() == 'google_map' ): 

							echo '<div id="venue">';
								echo '<div class="container">';
									if( get_field('location') ):
										echo '<div class="c-title">Venue: ' . get_field('location') . '</div>';
									else:
										echo '<div class="c-title">Venue</div>';
									endif;
								echo '</div>';
								
								include( locate_template('/includes/components/flexible-content/c-google-map.php' ) );
							echo '</div>';

						elseif( get_row_layout() == 'logo_grid' ): 

							echo '<div id="sponsors">';
								echo '<div class="container">';
									echo '<div class="c-title">Sponsors</div>';
								echo '</div>';
								
								include( locate_template('/includes/components/flexible-content/c-logo-grid.php' ) );
							echo '</div>';

						elseif( get_row_layout() == 'contacts' ): 

							echo '<div id="key-contacts">';				
								echo '<div class="container">';		
									echo '<div class="c-title">Key Contacts</div>';
								echo '</div>';

								include( locate_template('/includes/components/flexible-content/c-people.php' ) );
							echo '</div>';

						elseif( get_row_layout() == 'people' ): 

							echo '<div id="speakers">';				
								echo '<div class="container">';		
									echo '<div class="c-title">Speakers</div>';
								echo '</div>';

								include( locate_template('/includes/components/flexible-content/c-people.php' ) );
							echo '</div>';

						elseif( get_row_layout() == 'agenda' ): 

							echo '<div id="agenda">';	
								echo '<div class="container">';					
									echo '<div class="c-title">Agenda</div>';
								echo '</div>';

								include( locate_template('/includes/components/flexible-content/c-agenda.php' ) );
							echo '</div>';

						endif;

					endwhile;
				endif;
			?>

			<? include( locate_template('includes/singles/contact-details.php') ); ?>

			<? include( locate_template('includes/components/c-footer-contact.php') ); ?>

			<? include( locate_template('includes/singles/related-posts.php') ); ?>
		
		</div>
	</div>

</article>

<?
	// Include sticky events header (at the bottom so we know what tabs we have)
	include( locate_template('includes/singles/headers/event-header.php') );
?>