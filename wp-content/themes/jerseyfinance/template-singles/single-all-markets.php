<? if( !request_is_AJAX() ): ?>

	<? 
		// Inlcude post header
		include( locate_template('includes/post-headers/two-column-header.php') ); 
	?>
	
<? endif; ?>

<?
	/* === The query for the "Latest" tab === */
	$meta_query = [];

	// Fetch any posts that have this market tagged under `related markets`
	$meta_query[] = array(
		'key' 		=> 'related_markets',
		'compare'	=> 'LIKE',
		'value'		=> '"' . get_the_ID() . '"',
	);

	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	$post_types = filter_input( INPUT_GET, 'post_types', FILTER_SANITIZE_SPECIAL_CHARS, FILTER_REQUIRE_ARRAY ); 

	if( empty( $post_types ) ):
		$post_types = array('all-news','all-events','all-work');
	endif;

	$args = array(
		'post_type' 		=> $post_types,
		'posts_per_page' 	=> 10,
		'meta_query'		=> $meta_query,
		'paged'				=> $paged
	);

	$latest_query = new WP_Query($args);
?>

<? if( !request_is_AJAX() ): ?>

	<?	
		// Overview tab
		$tabs['Overview'] = 'overview';

		// Latest tab		
		if( $latest_query->have_posts() ):
			$tabs['Latest'] = 'latest';
		endif;

		// Include content tabs bar using the above tabs
		get_template_partial( 'includes/singles/content-tabs', array(
			'tabs'	=> $tabs
		)); 
	?>

	<div class="site-wrapper">
		<div id="overview" class="main-content c-tabs__content u-padding-top-40">
			

			<? 
				// Output the introduction + stats/feature
				include( locate_template('/includes/singles/' . get_post_type() . '-introduction.php') ); 
			?>

			<?
				// Output regular post content
				include( locate_template('/includes/flexible-content.php') ); 
			?>

			<? /* ==== The sectors that have been selected as a related sector ==== */ ?>

				<?
					// We can't order a relationship field by menu_order, so get sector ID's so we can pull them out with a WP_Query
					$sectors = get_field('sectors', false, false);

					if( $sectors ):
						$args = array(
							'post_type' 		=> 'all-sectors',
							'posts_per_page'	=> '-1',
							'post__in' 			=> $sectors
						);

						$sectors_query = new WP_Query($args);

						if( $sectors_query->have_posts() ): ?>

							<div class="container">
								<div class="c-title">
									Sectors for <? the_title(); ?>:
								</div>
							</div>

							<div class="container">
								<div class="c-link-grid">
									<div class="c-grid ontablet-portrait-make-col-4 onmobile-make-col-12">

										<? while( $sectors_query->have_posts() ): $sectors_query->the_post(); ?>

											<div class="c-grid__col-3 c-link-grid__item">
										
												<a class="c-link-grid__link u-font-family-bright u-font-weight-bold  u-margin-bottom-10" href="<? the_permalink(); ?>">
													<? the_title() ?>&nbsp;›
												</a>

												<? //echo get_field('sector_summary') ? '<div class="c-link-grid__text u-font-size-small">' . get_field('sector_summary') . '</div>' : '' ; ?>
											
											</div>

										<? endwhile; ?>
									
									</div>
								</div>
							</div>

						<? endif;
					endif; 
				?>

			<? /* === END === */ ?>

		</div>

<? endif; ?>

	<?
		// Include the grid of related news, events and work
		include( locate_template('/includes/singles/latest-posts.php') );
	?>

<? if( !request_is_AJAX() ): ?>

</div>



<? endif; ?>