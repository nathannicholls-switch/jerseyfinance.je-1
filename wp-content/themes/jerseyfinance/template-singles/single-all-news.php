<? if( !request_is_AJAX() ): ?>
	<div class="infinite-scroll-container">
<? endif; ?>

	<script>
		<?
			// Get the next post for ajax loaded articles
			$next_post = get_previous_post()->ID;
			$next_post = get_permalink( get_previous_post()->ID );
		?>

		var next_article = "<? echo $next_post; ?>";
	</script>

	<article class="track-progress load-next" 
		data-title="<? the_title(); ?>"
		data-permalink="<? the_permalink(); ?>"
		data-post-id="<? echo get_the_ID(); ?>">

		<? 
			// Using the `header_style` field, include the appropriate header
			$header_style 		= get_field('header_style') ? get_field('header_style') : 'one-column';
			$header_template 	= locate_template( '/includes/post-headers/' . $header_style . '-header.php' );

			if( $header_template ):
				include( $header_template );
			endif; 

			if( !request_is_AJAX() ):

				// Include sticky post header that tracks read progress
				include( locate_template('includes/singles/headers/read-progress-tracker.php') ); 
				
			endif; 
		?>

		<div class="main-content">
			<div class="site-wrapper">

				<? include( locate_template('/includes/flexible-content.php') ); ?>
			
			</div>
		</div>

	</article>

	<? include( locate_template('includes/singles/related-posts.php') ); ?>

	<? include( locate_template('includes/components/c-footer-contact.php') ); ?>

<? if( !request_is_AJAX() ): ?>
	</div>
<? endif; ?>