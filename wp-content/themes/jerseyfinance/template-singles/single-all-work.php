<? 
	$header_template = locate_template( '/includes/post-headers/' . get_field('header_style') . '-header.php' );

	if( $header_template ):
		include( $header_template );
	endif; 
?>

<?
	if( have_rows('additional_work_tabs') ):
	
		// Overview tab
		$tabs['Overview'] = 'overview';

		// Repeater tabs
		while( have_rows('additional_work_tabs') ): the_row();
			$tabs[get_sub_field('tab_title')] = strtolower( str_replace(' ', '', get_sub_field('tab_title')) );
		endwhile;

		// Include content tabs bar using the above tabs
		get_template_partial( 'includes/singles/content-tabs', array(
			'tabs'	=> $tabs
		)); 
	endif;
?>

<div class="site-wrapper">
	
	<div class="main-content c-tabs__content" id="overview">

		<?
			/*
				$issuu = get_field('issuu'); 
			
				if( $issuu ): ?>
					<div class="container">
						<iframe style="width:100%; height:600px;" src="//e.issuu.com/embed.html#<? echo $issuu; ?>" frameborder="0" allowfullscreen></iframe>
					</div>
				<? endif; 
			*/
		?>

		<? include( locate_template('/includes/flexible-content.php') ); ?>
	
	</div>

	<?
		if( have_rows('additional_work_tabs') ):
			while( have_rows('additional_work_tabs') ): the_row();

				echo '<div class="main-content c-tabs__content" id="' .  strtolower( str_replace(' ', '', get_sub_field('tab_title')) ) . '">';

					include( locate_template('/includes/flexible-content.php') );
				
				echo '</div>';

			endwhile;
		endif; 
	?>
</div>