<? 
	$header_template = locate_template( '/includes/post-headers/' . get_field('header_style') . '-header.php' );

	if( $header_template ):
		include( $header_template );
	endif; 
?>

<div class="main-content u-padding-top-20">
	<div class="site-wrapper">

		<? if( get_field('application_url') ): ?>
			<div class="container u-align-centre">
				<a class="c-button red u-margin-top-0" href="<? echo get_field('application_url'); ?>" target="_blank">
					Apply Online
				</a>
			</div>
		<? endif; ?>

		<?
			$reference 	= get_field('reference_number');
			$salary 		= get_field('salary');
			$term 		= get_the_terms(get_the_ID(), 'all-vacancy-terms');

			if( $term ):
				$term		= implode(', ', array_column($term, 'name'));
			endif;

			if( $reference || $salary || $term ): ?>

				<div class="container narrow">
					<table class="u-margin-top-0">
						<? if( $reference ): ?>
							<tr>
								<td class="u-align-centre"><? echo '<strong>Reference:</strong> ' . $reference; ?></td>
							</tr>
						<? endif; ?>
						<? if( $salary ): ?>
							<tr>
								<td class="u-align-centre"><? echo '<strong>Salary:</strong> ' . $salary; ?></td>
							</tr>
						<? endif; ?>
						<? if( $term ): ?>
							<tr>
								<td class="u-align-centre"><? echo '<strong>Term:</strong> ' . $term; ?></td>
							</tr>
						<? endif; ?>
					</table>
				</div>
			
			<? endif; 
		?>

		<? include( locate_template('/includes/flexible-content.php') ); ?>

		<? if( get_field('application_url') ): ?>
			<div class="container u-align-centre">
				<a class="c-button red" href="<? echo get_field('application_url'); ?>" target="_blank">
					Apply Online
				</a>
			</div>
		<? endif; ?>

		<? include( locate_template('includes/singles/contact-details.php') ); ?>
	
	</div>
</div>