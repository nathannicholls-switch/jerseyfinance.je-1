<? if( user_in_working_group() ): ?>

	<? 
		$header_template = locate_template( '/includes/post-headers/' . get_field('header_style') . '-header.php' );

		if( $header_template ):
			include( $header_template );
		endif; 
	?>

	<div class="main-content">
		<div class="site-wrapper">

			<? include( locate_template('/includes/flexible-content.php') ); ?>

			<? 
				if( comments_open() ):
					comments_template( '', true ); 
				endif; 
			?>
		
		</div>
	</div>

<? else: ?>

	<? include( locate_template('includes/partials/login-form.php') ); ?>

<? endif; ?>