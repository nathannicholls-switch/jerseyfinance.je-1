<? if( !request_is_AJAX() ): ?>
	<? include( locate_template('/includes/head.php') ); ?>
<? endif; ?>

<?	
	// Get content from "Page Not Found" page
	$args = array(
		'pagename' => 'page-not-found'
	);

	$page_query = new WP_Query($args);

	if( $page_query->have_posts() ):
		while( $page_query->have_posts() ): $page_query->the_post(); ?>

			<?
				// If the user has landed on /404 because of an invalid or expired confirmation link, output message
				if( $_REQUEST['action'] === 'failedemail' ):
					$title 	= 'Sorry, there was a problem confirming your change of email';
					$text 	= 'Your confirmation link may have expired or you may have already confirmed your new email address.<br><br>Please <a href="/login">login</a> and try again.';
				endif;

				get_template_partial( '/includes/post-headers/one-column-header', array(
					'title' 	=> $title,
					'text'	=> $text
				)); 
			?>


			<div class="main-content">
				<div class="site-wrapper">

					<? include( locate_template('/includes/flexible-content.php') ); ?>

					<div class="container">
						<?
							// Needed for the Gravity Forms user activation page
							the_content(); 
						?>
					</div>

					<? 
						if( comments_open() ):
							comments_template( '', true ); 
						endif; 
					?>

				</div>
			</div>

		<? endwhile;
	endif;
?>

<? if( !request_is_AJAX() ): ?>
	<? include( locate_template('/includes/footer.php') ); ?>
<? endif; ?>