<?
	// Will search for a page that matches the archive slug and will pull header content from it
	include( locate_template('/includes/archives/archive-header.php') ); 
?>

<?
	$anchor_links = get_field('show_anchor_bar', get_queried_object()) == 'true' ? true : false;
?>

<div class="main-content u-padding-bottom-0">
	<div class="site-wrapper">

		<? if( have_posts() ):	?>

			<div class="c-timeline <? echo $anchor_links ? 'has-anchors' : '' ; ?>">
				<div class="c-timeline__wrapper">

					<?
						$previous_date = '';
						$row_count = 0;

						// We will push months into this and use it as our month anchor bar
						$month_list = array();

						while( have_posts() ): the_post();

							$style 			= get_field('style') ? get_field('style') : '';
							$format			= get_field('format') ? get_field('format') : '';
							$type 			= get_field('type') ? get_field('type') : 'date';

							$heading 		= get_field('heading') ? get_field('heading') : '';
							$title 			= get_the_title();
							$description 	= get_field('description') ? get_field('description') : '';
							$link				= get_field('link') ? get_field('link') : '';

							$background_image = $style == 'two-col' ? 'background-image: url(' . get_field('image')['sizes']['container-width'] . ')' : '';

							if( $type == 'date' ):
								$date 			= get_field('date', false, false);
								$date				= new DateTime($date);

								$formatted_date = '<div class="day">';
									$formatted_date .= $date->format('j');
								$formatted_date .= '</div>';
								$formatted_date .= '<div class="month">';
									$formatted_date .= $date->format('M');
								$formatted_date .= '</div>';
								$formatted_date .= '<div class="year">';
									$formatted_date .= $date->format('Y');
								$formatted_date .= '</div>';
							else:
								$date 			= get_field('date');
							endif;

							$content = '<div class="c-timeline__content">';
								$content .= $heading ? '<h6>' . $heading . '</h6>' : '';

								$content .= !empty( $link ) ? '<a href="' . $link['url'] . '" target="_blank">' : '';
									$content .= '<h3>' . $title . '</h3>';
								$content .= !empty( $link ) ? '</a>' : '';

								$content .= $description ? '<div class="u-font-size-small">' . $description . '</div>' : '';
							
								if( is_array($link) && !empty( implode($link) ) ):
									$content .= '<a href="' . $link['url'] . '" target="_blank" class="c-button red">';
										$content .= $link['title'] . '&nbsp;›';
									$content .= '</a>';
								endif;
							$content .= '</div>';

							$feature = '<div class="c-timeline__feature ' . $format . '">';
								$feature .= '<div class="c-timeline__date">';
									$feature .= '<div class="c-timeline__date-wrapper">';
										$feature .= $type == 'date' ? $formatted_date : $date;
									$feature .= '</div>';
								$feature .= '</div>';

								if( get_field('video') ):
									$feature .= '<div class="c-timeline__video">';
										$feature .= get_field('video');
									$feature .= '</div>';
								endif;

								if( get_field('image') ):
									$feature .= '<div class="c-timeline__image">';
										$feature .= '<img src="' . get_field('image')['sizes']['container-width'] . '" />';
									$feature .= '</div>';
								endif;
							$feature .= '</div>';

							// Jan, Feb, Mar. A, B, C. 1, 2, 3.
							$temp_date = $type == 'date' ? substr($date->format('F'), 0, 3) : $date;
							$curr_date = $type == 'date' ? $date->format('y') : $date;

							$current_month = $temp_date;
							$current_year 	= $curr_date;

							if( $type == 'date' ):
								$current_date	= $current_month . ' ' . $current_year;
							else:
								$current_date	= $current_month;
							endif;
							// echo $current_date;

							if( $current_date !== $previous_date ): ?>

								<? echo $previous_date ? '</div>' : '' ?>

								<div id="<? echo str_replace(' ', '-', strtolower($current_date)); ?>">

								<? 
									$previous_date = $current_date; 
									$month_list[] = $current_date; 
								?>

							<? endif; ?>

								<? include( locate_template( 'includes/archives/timeline-items/' . $style . '.php' ) ); ?>			

							<? $row_count++;
						endwhile; 
					?>
					</div><!-- close last month -->
					
				</div>

				<? if( $anchor_links ): ?>

					<div class="c-timeline__months">
						<div class="container">
							<? foreach( $month_list as $month ): ?>
								<a href="#<? echo str_replace(' ', '-', strtolower($month)); ?>" class="c-timeline__month">
									<div class="month">
										<? echo $month; ?>
									</div>
								</a>
							<? endforeach; ?>
						</div>
					</div>

				<? endif; ?>

			</div>

		<? endif; ?>

	</div>
</div>


<?
	function make_months_fixed() {
?>
		<script>
			$(window).scroll(function() {
				var currentScroll = $(document).scrollTop();
				var timelineTop 	= $('.c-timeline').offset().top;

				// If we have scrolled past the months, make them fixed
				if( currentScroll > timelineTop ) {
					$('.c-timeline__months').addClass('fixed');
				} else {
					$('.c-timeline__months').removeClass('fixed');
				}
			})
		</script>
<?
	}
	if( $anchor_links ):
		add_action('wp_footer', 'make_months_fixed', 20);
	endif;
?>