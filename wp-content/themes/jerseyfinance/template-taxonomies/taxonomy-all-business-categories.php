<? if( !request_is_AJAX() ): ?>

	<?
		// Will search for a page that matches the archive slug and will pull header content from it
		include( locate_template('/includes/archives/archive-header.php') ); 
	?>

	<div class="main-content u-padding-top-0 u-padding-bottom-0">
		<div class="site-wrapper">

			<? 
				// Inlcude the post type filters for taxonomies
				get_template_partial( 'includes/archives/filters', array(
					'related_post_types'	=> array(
						// 'all-sectors',
						'all-markets',
					),
				)); 
			?>

			<div class="u-pattern-wrapper fill">
				<div class="container u-position-relative u-padding-top-40 u-padding-bottom-70">
					<div id="ajax-container">
						<div class="c-grid flex ontablet-middle-make-col-6 onmobile-make-col-12 item-container">

<? endif; ?>

							<? // If the request is AJAX make sure no items appear large
								$i = request_is_AJAX() && is_paged() ? 10 : 0;

								while( have_posts() ): the_post();
									setup_postdata($post);

									include( locate_template( 'includes/archives/business-item.php' ) );								

									$i++;
								endwhile;
							?>

							<? if( request_is_AJAX() ): ?>
								<? include( locate_template('/includes/archives/pagination.php') ); ?>
							<? endif; ?>

	<? if( !request_is_AJAX() ): ?>

						</div>

						<div class="pagination-container">
							<? if( !request_is_AJAX() ): ?>
								<? include( locate_template('/includes/archives/pagination.php') ); ?>
							<? endif; ?>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

<? endif; ?>

