<? if( !request_is_AJAX() ): ?>

	<?
		// Will search for a page that matches the archive slug and will pull header content from it
		include( locate_template('/includes/archives/archive-header.php') ); 
	?>

	<div class="main-content u-padding-top-0 u-padding-bottom-0">
		<div class="site-wrapper">

			<? 
				// Inlcude the post type filters for taxonomies
				get_template_partial( 'includes/archives/filters', array(
					'post_type'	=> true,
					'modifiers'	=> true
				)); 
			?>

			<div class="container">
				<div id="ajax-container">
					<div class="c-feature-grid c-feature-grid__grid-view item-container">

<? endif; ?>

						<? // If the request is AJAX make sure no items appear large
							$i = request_is_AJAX() && is_paged() ? 10 : 0;

							while( have_posts() ): the_post();
								setup_postdata($post);
								
								include( locate_template('/includes/archives/post-item.php') );

								$i++;
							endwhile;
						?>

						<? if( request_is_AJAX() ): ?>
							<? include( locate_template('/includes/archives/pagination.php') ); ?>
						<? endif; ?>

<? if( !request_is_AJAX() ): ?>

					</div>

					<div class="pagination-container">
						<? if( !request_is_AJAX() ): ?>
							<? include( locate_template('/includes/archives/pagination.php') ); ?>
						<? endif; ?>
					</div>
				</div>
			</div>

		</div>
	</div>

<? endif; ?>

