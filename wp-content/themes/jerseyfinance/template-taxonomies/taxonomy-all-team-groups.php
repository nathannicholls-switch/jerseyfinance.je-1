<? if( !request_is_AJAX() ): ?>

	<?
		// Will search for a page that matches the archive slug and will pull header content from it
		include( locate_template('/includes/archives/archive-header.php') ); 
	?>

	<div class="main-content u-padding-bottom-0">
		<div class="site-wrapper">

			<?
				// Not the board
				if( get_queried_object_id() !== 106 ):
					get_template_partial( 'includes/archives/filters', array(
						'taxonomies'	=> array(
							'all-team-teams' 	=> 'term_order',
						)
					)); 
				endif;
			?>

			<div class="u-pattern-wrapper fill">
				<div class="container u-position-relative u-padding-top-40 u-padding-bottom-70">
					<div id="ajax-container">
						<div class="c-grid flex ontablet-middle-make-col-4 onmobile-make-col-12 item-container">

<? endif; ?>

						<?
							if( have_posts() ):
								while( have_posts() ): the_post();
									setup_postdata($post); ?>
									
									<div class="c-grid__col-3 u-margin-bottom-20">
										<div class="c-person u-background-white u-full-width u-box-shadow-light">
											<? if( get_field('profile_image') ): ?>
												<a href="<? the_permalink(); ?>" class="c-person__image">
													<img src="<? echo get_field('profile_image')['sizes']['logo-square']; ?>" alt="<? the_title(); ?>" />
												</a>
											<? endif; ?>

											<div class="c-person__details">
												<a href="<? the_permalink(); ?>" class="h5 u-margin-bottom-0 u-display-block u-align-centre">
													<? the_title(); ?>
												</a>

												<p class="u-colour-dark-grey u-align-centre u-font-size-small"><? echo get_field('job_title'); ?></p>

												<p class="u-font-size-small links">
													<? if( get_field('email') ): ?>
														<a href="mailto:<? echo get_field('email'); ?>">&gt; Email <? echo explode(' ', get_the_title())[0]; ?></a>
														<br>
													<? endif; ?>

													<?/* if( get_field('telephone') ): ?>
														<a href="tel:<? echo get_field('telephone'); ?>">&gt; T <? echo get_field('telephone'); ?></a>
														<br>
													<? endif; */?>

													<a href="<? the_permalink(); ?>">&gt; Full Profile</a>
												</p>
											</div>
										</div>
									</div>

								<? endwhile;
							else:
								return_no_posts_message();
							endif;
						?>

<? if( !request_is_AJAX() ): ?>

						</div>
					</div>
				</div>
			</div>

			<? 
				// Will search for a page that matches the archive slug and will pull page content from it
				include( locate_template('/includes/archives/archive-page-content.php') ); 
			?>

		</div>
	</div>

<? endif; ?>