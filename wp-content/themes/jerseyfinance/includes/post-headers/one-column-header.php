<?
	// Content group field (contains below fields, it can be a clone so we 
	// have to check if it exists inside the parent 'header_content' field)
	$content = array_key_exists( 'header_content', get_field('header_content') ) ? get_field('header_content')['header_content'] : get_field('header_content');

	// Individual fields
	$heading 		= isset($template_args['heading']) ? $template_args['heading'] : $content['heading'];
	$title 			= isset($template_args['title']) ? $template_args['title'] : $content['title'];
	$text 			= isset($template_args['text']) ? $template_args['text'] : $content['text'];
	$button 			= $content['button'];
	$key_contact 	= $content['key_contact'];

	$image 			= get_field('header_image');
	$hide_overlay 	= get_field('hide_gradient');

	if( !get_field('move_header_content') ):
		$text_colour 	= $image ? 'u-colour-white' : '';
	else:
		$text_colour = '';
	endif;

	$classes 	 = $image ? 'image' : 'u-align-centre';

	if( $image ):
		$classes 	.=	$hide_overlay ? '' : ' overlay';
	endif;
?>

<div class="c-hero-header one-column <? echo $classes; ?>" <? echo $image ? 'style="background-image: url(' . $image['sizes']['hero-image'] . ');"' : ''; ?>>
	<div class="container">
		<? 
			if( !get_field('move_header_content') ):
				include_breadcrumbs( $image ? 'white' : 'red' );
			endif;
		?>

		<div class="c-hero-header__wrapper">
			<div class="c-hero-header__text">

				<? 
					if( !get_field('move_header_content') ):
						include( locate_template('/includes/post-headers/header-content.php') );
					endif; 
				?>

			</div>
		</div>
	</div>
</div>