<?
	// Hide the animations on localhost
	$whitelist = array(
		'127.0.0.1',
		'::1'
	);
?>

<div class="c-hero-header c-home-header overlay" style="background-image: url(<? echo get_field('background_image')['sizes']['hero-image']; ?>);">

	<? // Background video ?>

	<div class="video-wrapper">
		<video id="home-video" playsinline autoplay muted loop data-object-fit="cover">

		</video>
	</div>

	<div class="container">

		<? /*
		<svg id="ring" viewBox="0 0 60 60" style="width: 60px; height: 60px;">
			<g class="pulse">
				<path d="M16.35,32.69A16.34,16.34,0,0,1,4.79,4.79,16.34,16.34,0,1,1,27.9,27.9,16.15,16.15,0,0,1,16.35,32.69Zm0-32.08a15.72,15.72,0,1,0,11.13,4.6A15.68,15.68,0,0,0,16.35.61Z" style="fill: #fff"/>
				<path d="M16.33,28.86a12.52,12.52,0,1,1,12.4-12.51A12.47,12.47,0,0,1,16.33,28.86Zm0-24.41A11.91,11.91,0,1,0,28.11,16.36,11.86,11.86,0,0,0,16.33,4.45Z" style="fill: #fff"/>
				<path d="M16.35,25.15a8.81,8.81,0,1,1,8.8-8.8A8.82,8.82,0,0,1,16.35,25.15Zm0-17a8.19,8.19,0,1,0,8.18,8.19A8.19,8.19,0,0,0,16.35,8.16Z" style="fill: #fff"/>
				<path d="M16.35,21.56a5.22,5.22,0,1,1,5.21-5.21A5.22,5.22,0,0,1,16.35,21.56Zm0-9.81a4.6,4.6,0,1,0,4.59,4.6A4.6,4.6,0,0,0,16.35,11.75Z" style="fill: #fff"/>
				<path d="M16.35,18.43a2.09,2.09,0,1,1,2.08-2.08A2.09,2.09,0,0,1,16.35,18.43Zm0-3.55a1.48,1.48,0,1,0,1.48,1.48A1.48,1.48,0,0,0,16.35,14.88Z" style="fill: #fff"/>
			</g>
		</svg>
		*/ ?>

		<? // Header content ?>

		<div class="c-hero-header__wrapper">
			<div class="c-hero-header__text">
				<h1 data-aos="fade-in" data-aos-delay="200"><? the_field('heading'); ?></h1>
				<h2 data-aos="fade-in" data-aos-delay="800"><? the_field('subheading'); ?></h2>
			</div>
			<div class="c-hero-header__image animated-globe">
				<div class="u-position-relative">
					<div class="animated-globe__paths bottom"></div>
					
					<img src="<? echo get_stylesheet_directory_uri(); ?>/dist/images/home-globe.png" alt="Jersey Finance" />

					<svg class="ring">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ring"></use>
					</svg>

					<div class="animated-globe__paths top"></div>
				</div>
			</div>
		</div>

		<? // News ticker ?>

		<div class="c-news-ticker">
			<div class="c-news-ticker__title">
				Latest
			</div>

			<?
				$tax_query = [];	

				// Only put news from certain categories in the ticker
				$tax_query[] = array(
					'taxonomy' => 'all-news-types',
					'field' 		=> 'term_id',
					'terms'		=> array(117,70), // Our new and Jersey industry News
					'operator'	=> 'IN'
				);

				$args = array(
					'post_type'			=> 'all-news',
					'posts_per_page'	=> 10,
					'tax_query'			=> $tax_query
				);

				$latest_query = new WP_Query($args);
			?>

			<div class="c-news-ticker__items">

				<? while( $latest_query->have_posts() ): $latest_query->the_post(); ?>

					<?
						$published_by = get_field('business');

						if( $published_by ):
							$business_id = $published_by->ID;

							$logo = get_field('social_logo', $business_id) ? get_field('social_logo', $business_id) : get_field('logo', $business_id);

							if( $logo ):
								$company_image = $logo['sizes']['logo-small'];
							else:
								$company_image = get_stylesheet_directory_uri() . '/dist/images/jfl-thumbnail.png';
							endif;
						else:
							$company_image = get_stylesheet_directory_uri() . '/dist/images/jfl-thumbnail.png';
						endif;
					?>
					
					<a class="c-news-ticker__item u-decoration-none u-colour-white" href="<? the_permalink(); ?>">

						<div class="u-table u-table__fixed u-table__full-width">

							<div class="c-news-ticker__logo u-table-cell u-valign-middle">
								<img src="<? echo $company_image; ?>" alt="<? the_title(); ?>" />
							</div>

							<div class="c-news-ticker__text u-table-cell u-valign-middle">
								<? echo shorten_text(get_the_title(), 45, false); ?>&nbsp;›
							</div>

						</div>

					</a>
					
				<? endwhile; 
				wp_reset_postdata(); ?>

			</div>
		</div>
		
	</div>
</div>

<? if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)) { ?>

	<? function globeAnimation() { ?>
		<script src="<? echo get_template_directory_uri() . "/dist/js/lottie.min.js"; ?>" ></script>

		<script>
			$(window).load(function() {
				// Only load the video once the page has finished loading
				$('#home-video').html('<source src="https://player.vimeo.com/external/297344823.hd.mp4?s=bdd5dabb0b67299650dfeb5e5cb9272a1283ec60&profile_id=175" type="video/mp4">');

				// === The Lottie animations for both globe animation layers === //

					var globeAnimation1;
					// var globeAnimation2;

					globeAnimation1 = lottie.loadAnimation({
						container: $('.animated-globe__paths.top')[0],
						renderer: 'canvas',
						loop: true,
						autoplay: true,
						path: '<? echo get_template_directory_uri() . "/dist/js/json/data.json"; ?>',
						rendererSettings: {
							progressiveLoad: true, // Boolean, only svg renderer, loads dom elements when needed. Might speed up initialization for large number of elements.
						}
					});

					// globeAnimation2 = lottie.loadAnimation({
					// 	container: $('.animated-globe__paths.bottom')[0],
					// 	renderer: 'canvas',
					// 	loop: true,
					// 	autoplay: true,
					// 	path: '<? echo get_template_directory_uri() . "/dist/js/json/data.json"; ?>'
					// });

				// === END === //

				// === The function that determines whether or not to pause/play the animation === //

					var resizeTimer;

					function pausePlayAnimation() {
						clearTimeout(resizeTimer);
						resizeTimer = setTimeout(function() {

							// Pause the animation on smaller devices
							if( $(window).width() < 850 ) {
								globeAnimation1.pause();
								// globeAnimation2.pause();
							} else {
								globeAnimation1.play();
								// globeAnimation2.play();
							}				
										
						}, 500);
					}

					$(window).on('resize', function(e) {
						pausePlayAnimation();
					});

					$(function() {
						pausePlayAnimation();
					});

				// === END === //

			});

		</script>
	<? } ?>
<? add_action('wp_footer', 'globeAnimation', 20); ?>

<? } ?>