<? if( get_post_type() == 'all-news' || get_post_type() == 'all-events' || get_post_type() == 'all-work' ): ?>
	<br>
	<div class="c-header-share <? echo $text_colour; ?>" data-aos="fade-in" data-aos-delay="800">
		<div class="u-table">
			<div class="u-table-cell">
				Share:
			</div>
			<div class="u-table-cell">
				<a href="https://www.linkedin.com/shareArticle?mini=true&url=<? the_permalink(); ?>&title=<? the_title(); ?>&summary=&source=">
					<svg class="linkedin">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#linkedin"></use>
					</svg>
				</a>
			</div>
			<div class="u-table-cell">
				<a href="https://twitter.com/home?status=<? the_permalink(); ?>">
					<svg class="twitter">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#twitter"></use>
					</svg>
				</a>
			</div>
			<div class="u-table-cell">
				<a href="https://www.facebook.com/sharer/sharer.php?u=<? the_permalink(); ?>">
					<svg class="facebook">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#facebook"></use>
					</svg>
				</a>
			</div>
		</div>
	</div>
<? endif; ?>