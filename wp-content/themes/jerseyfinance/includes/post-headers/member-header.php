<?
	/*
		Used on membership, login
	*/

	$heading 		= '';
	$title 			= get_field('logo') ? '' : get_the_title();
	$text 			= get_field('introduction');
	$button 			= '';
	$key_contact 	= '';

	$image 			= get_field('social_logo') ? get_field('social_logo') : get_field('logo');

	$alternate_h1 = !get_field('social_logo') && get_field('logo') ? true : false;

	// Information
	$address 		= get_field('address');
	$email 			= get_field('email_address');
	$telephone 		= get_field('telephone_number');
	$website			= get_field('website_address');
	$website_url 	= get_field('website_address');

	// Datalayer click event
	$onclick_event = 'onclick="dataLayer.push({ \'memberContact\': \'' . get_the_title() . '\' })"';

	$disallowed = array('http://', 'https://');

	foreach( $disallowed as $d ):
		$website_url = str_replace($d, '', $website_url);
	endforeach;
?>

<div class="c-two-column-header c-member-header">
	<div class="container">
		
		<? include_breadcrumbs(); ?>

		<? if( $image ): ?>
			<img class="member-logo u-margin-bottom-20 <? echo get_field('social_logo') ? 'u-box-shadow-light thumb' : ''; ?>" src="<? echo $image['sizes']['logo-full']; ?>" alt="<? the_title(); ?>"/>
		<? endif; ?>

		<div class="c-two-column-header__wrapper">

			<div class="c-two-column-header__content">
				<div class="c-two-column-header__text">
				
					<? include( locate_template('/includes/post-headers/header-content.php') ); ?>

					<?
						/*
							$business_categories = get_the_terms( get_the_ID(), 'all-business-categories' );

							if( $business_categories ): ?>
								<span class="u-colour-dark-grey u-font-size-12 u-margin-top-10 u-display-inline-block" data-aos="fade-in" data-aos-delay="800">
									<?
										$i = 0;

										foreach( $business_categories as $category ):
											echo $i++ !== 0 ? ', ' : '' ;
											echo $category->name;
										endforeach;
									?>
								</span>
							<? endif; 
						*/
					?>
				</div>
			</div>

			<div class="c-two-column-header__image">
				<div class="c-two-column-header__image-wrapper">
					<div class="c-member-header__information u-background-white u-box-shadow-light">

						<h5>Contact Details</h5>

						<? if( $alternate_h1 ): ?>
							<h1 class="destyle-heading">
								<strong class="u-colour-red">
									<? the_title(); ?>
								</strong>
								<br><br>
							</h1>
						<? endif; ?>

						<p class="u-colour-dark-grey">
							<? if( !$alternate_h1 ): ?>
								<strong class="u-colour-red">
									<? the_title(); ?>
								</strong><br><br>
							<? endif; ?>
												
							<? if( $address ): ?>
								<? echo $address; ?>
							<? endif; ?>
						</p>

						<? if( $email ): ?>
							<p class="u-colour-red">
								<? echo $email ? '<strong>E</strong> <a href="mailto:' . $email . '" ' . $onclick_event . '>' . $email . '</a>' : ''; ?>
							</p>
						<? endif; ?>

						<? if( $telephone ): ?>
							<p class="u-colour-red">
								<? echo $telephone ? '<strong>T</strong> <a href="tel:' . $telephone . '">' . $telephone . '</a>' : ''; ?><br>
							</p>
						<? endif; ?>

						<? if( $website ): ?>
							<p class="u-colour-red">
								<? echo $website ? '<strong>W</strong> <a href="' . $website  .'" target="_blank" ' . $onclick_event . '>' . $website_url . '</a>' : ''; ?>
							</p>
						<? endif; ?>

						<?
							// Social
							$linkedin 	= get_field('linkedin');
							$facebook 	= get_field('facebook');
							$twitter 	= get_field('twitter');
						?>

						<? if( $linkedin || $twitter || $facebook ): ?>

							<div class="social">
								<? if( $linkedin ): ?>
									<a href="<? echo $linkedin; ?>" target="_blank" <? echo $onclick_event; ?>>
										<svg class="linkedin">
											<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#linkedin"></use>
										</svg>
									</a>
								<? endif; ?>

								<? if( $twitter ): ?>
									<a href="<? echo $twitter; ?>" target="_blank" <? echo $onclick_event; ?>>
										<svg class="twitter">
											<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#twitter"></use>
										</svg>
									</a>
								<? endif; ?>

								<? if( $facebook ): ?>
									<a href="<? echo $facebook; ?>" target="_blank" <? echo $onclick_event; ?>>
										<svg class="facebook">
											<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#facebook"></use>
										</svg>
									</a>						
								<? endif; ?>
							</div>

						<? endif; ?>
						
					</div>
				</div>
			</div>

		</div>	

	</div>
</div>