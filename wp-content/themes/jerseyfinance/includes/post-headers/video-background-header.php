<?
	// Content group field (contains below fields, it can be a clone so we 
	// have to check if it exists inside the parent 'header_content' field)
	$content = array_key_exists( 'header_content', get_field('header_content') ) ? get_field('header_content')['header_content'] : get_field('header_content');

	// Individual fields
	$heading 		= isset($template_args['heading']) ? $template_args['heading'] : $content['heading'];
	$title 			= isset($template_args['title']) ? $template_args['title'] : $content['title'];
	$text 			= isset($template_args['text']) ? $template_args['text'] : $content['text'];
	$button 			= $content['button'];
	$key_contact 	= $content['key_contact'];

	$text_colour 	= 'u-colour-white';

	// get iframe HTML
	$video = get_field('background_video');
?>

<div class="c-hero-header one-column overlay video">
	<div class="video-wrapper">
		<video autoplay muted loop>
			<source src="<? echo $video; ?>" type="video/mp4">
		</video>
	</div>

	<div class="container">
		<? include_breadcrumbs( 'white' ); ?>

		<div class="c-hero-header__wrapper">
			<div class="c-hero-header__text">

				<? include( locate_template('/includes/post-headers/header-content.php') ); ?>

			</div>
		</div>
	</div>
</div>