<?
	// Content group field (contains below fields, it can be a clone so we 
	// have to check if it exists inside the parent 'header_content' field)
	$content = array_key_exists( 'header_content', get_field('header_content') ) ? get_field('header_content')['header_content'] : get_field('header_content');

	// Individual fields
	$heading 		= $content['heading'];
	$title 			= $content['title'];
	$text 			= $content['text'];
	$button 			= $content['button'];
	$key_contact 	= $content['key_contact'];
?>

<div class="c-two-column-header">
	<div class="container">
		
		<? include_breadcrumbs(); ?>

		<div class="c-two-column-header__wrapper">

			<div class="c-two-column-header__content">
				<div class="c-two-column-header__text">
				
					<? include( locate_template('/includes/post-headers/header-content.php') ); ?>

				</div>
			</div>

			<div class="c-two-column-header__image">
				<? // Add a floating element if this is a markets page ?>
				<? echo get_post_type() == 'all-markets' ? '<div class="animation-float">' : ''; ?>
					<?
						// Will include extra animation elements/styles if the file exists 
						$animation_path = '/includes/post-headers/animations/' . get_post_type() . '-animation.php';

						if( locate_template( $animation_path ) ):
							include( locate_template( $animation_path ) );
						endif;
					?>

					<div class="c-two-column-header__image-wrapper">
						<img src="<? echo get_field('header_image')['sizes']['two-column-header-image']; ?>" alt="Jersey Finance" data-aos="fade-left" />

						<p class="u-caption">
							<? echo get_field('header_image')['caption']; ?>
						</p>
					</div>
				<? echo get_post_type() == 'all-markets' ? '</div>' : ''; ?>

				<? echo get_post_type() == 'all-markets' ? '<div class="shadow"></div>' : ''; ?>
			</div>

		</div>	

	</div>
</div>