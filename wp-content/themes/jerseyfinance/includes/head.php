<!DOCTYPE html>
<html lang="en">
<head>
	<script>window.dataLayer=window.dataLayer||[];</script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MHKZPG');</script>
	<!-- End Google Tag Manager -->

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<link rel="apple-touch-icon" sizes="180x180" href="<? echo get_stylesheet_directory_uri(); ?>/dist/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<? echo get_stylesheet_directory_uri(); ?>/dist/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<? echo get_stylesheet_directory_uri(); ?>/dist/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="<? echo get_stylesheet_directory_uri(); ?>/dist/images/favicon/site.webmanifest">
	<link rel="mask-icon" href="<? echo get_stylesheet_directory_uri(); ?>/dist/images/favicon/safari-pinned-tab.svg" color="#cb181c">
	<link rel="shortcut icon" href="<? echo get_stylesheet_directory_uri(); ?>/dist/images/favicon/favicon.ico">
	<meta name="msapplication-TileColor" content="#cb181c">
	<meta name="msapplication-config" content="<? echo get_stylesheet_directory_uri(); ?>/dist/images/favicon/browserconfig.xml">
	<meta name="theme-color" content="#cb181c">

	<? wp_head(); ?>
	
</head>
<body <? echo body_class(); ?>>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MHKZPG"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<? 
		// Include a file that pushes member data and login events to the datalayer
		include( locate_template('functions/custom-seo/member-datalayer.php') ); 
	?>

	<div style="display: block; visibility: hidden; height: 0; width: 0;">
		<? 
			// Output SVG sprite	
			readfile( get_template_directory_uri() . '/dist/images/sprite.svg' ); 
		?>
	</div>

<? include( locate_template('includes/header.php') ); ?>