<?
	$tabs 	= isset($template_args['tabs']) ? $template_args['tabs'] : '';
	$anchors = isset($template_args['anchors']) ? $template_args['anchors'] : false;
	
	$i = 0;

	if( $tabs ): ?>

		<div class="c-tabs <? echo $anchors ? 'anchors' : 'content-toggle' ; ?>">
			<div class="container">
								
				<? foreach( $tabs as $key => $tab ): 
					?><a <? echo $anchors ? 'href="#' . $tab . '"' : 'data-target="' . $tab . '"'; ?> class="c-tabs__tab <? echo $i == 0 ? 'active' : ''; ?>">
						<span class="c-tabs__text">
							<? echo $key; ?>
						</span>
					</a><?
				$i++; endforeach; ?>

			</div>
		</div>

	<? endif; 
?>