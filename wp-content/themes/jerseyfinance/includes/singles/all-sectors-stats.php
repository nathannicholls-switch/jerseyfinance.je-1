<?
	/*
		A stats block is included on the single template for sectors
	*/
?>

<?	if( have_rows('statistics') ): ?>
	<div class="c-sector-stats">

		<? while( have_rows('statistics') ): the_row(); ?>

			<?
				// Include individual stat - uses default sub fields
				include( locate_template('includes/partials/stat.php') );
			?>

		<? endwhile; ?>

	</div>
<? endif; ?>
