<?
	$tax_query = [];
	$meta_query = [];

	$topics 	= get_the_terms(get_the_ID(), 'all-topics');
	$types 	= get_the_terms(get_the_ID(), 'all-news-types');

	if( $topics ):
		foreach( $topics as $topic ):
			$tax_query[] = array(
				'taxonomy' 	=> 'all-topics',
				'field'		=> 'term_id',
				'terms'		=> $topic,
				'operator' => 'IN',
			);
		endforeach;
	endif;

	if( $types ):
		foreach( $types as $type ):
			$tax_query[] = array(
				'taxonomy' 	=> 'all-news-types',
				'field'		=> 'term_id',
				'terms'		=> $type,
				'operator' => 'IN',
			);
		endforeach;
	endif;

	// Don't show past events
	if( get_post_type() == 'all-events' ):
		$today = date('Ymd');

		$meta_query[] = array(
			'key'			=> 'hide_date',
			'value'		=> $today,
			'compare'	=> '>'	
		);
	endif;

	$args = array(
		'post_type' 		=> get_post_type(),
		'posts_per_page'	=> 4,
		'tax_query'			=> $tax_query,
		'meta_query'		=> $meta_query,
		'post__not_in'		=> array(get_the_ID())
	);

	// Sort events by date
	if( get_post_type() == 'all-events' ):
		$args['order'] 		= 'ASC';
		$args['orderby'] 		= 'meta_value';
		$args['meta_key'] 	= 'dates_0_start_date';
	endif;

	$related_posts = new WP_Query($args);

	// If there are no related posts, be a little more lenient
	if( !$related_posts->have_posts() ):
		$tax_query['relation'] = 'OR';

		$args = array(
			'post_type' 		=> get_post_type(),
			'posts_per_page'	=> 4,
			'tax_query'			=> $tax_query,
			'meta_query'		=> $meta_query,
			'post__not_in'		=> array(get_the_ID())
		);

		$related_posts = new WP_Query($args);
	endif;

	// If there are less than 4 just grab the latest posts
	if( $related_posts->found_posts < 4 ):
		$args = array(
			'post_type' 		=> get_post_type(),
			'posts_per_page'	=> 4,
			'post__not_in'		=> array(get_the_ID()),
			'meta_query'		=> $meta_query
		);

		$related_posts = new WP_Query($args);
	endif;

	// Output items
	if( $related_posts->have_posts() ): ?>

		<div class="container">

			<div class="c-title u-margin-bottom-20">
				More <? echo get_post_type_object($post_type)->label; ?>
			</div>

			<div class="c-feature-grid__grid-view u-margin-bottom-50">
				
				<? 
					while( $related_posts->have_posts() ): $related_posts->the_post();

						get_template_partial('/includes/archives/post-item', array(
							'grid_size' => 'small'
						));
					
					endwhile; 
					wp_reset_postdata();
				?>

			</div>
		</div>

	<? endif; 
?>