<div class="c-text">
	<div class="container">
		<span class="h6">In brief</span>

		<?
			$post_introduction = get_field('post_introduction');

			$heading 		= $post_introduction['heading'];
			$introduction 	= $post_introduction['introduction'];

			echo $heading ? '<span class="h3">' . $heading . '</span>' : '';

			echo $introduction ? $introduction : '';
		?>
	</div>
</div>

<div class="c-promo-block">
	<div class="u-pattern-wrapper u-padding-bottom-50">
		<div class="container">
			<div class="c-promo-block__wrapper">
				<div class="c-promo-block__content">
					<div class="c-promo-block__box u-background-white u-box-shadow-light">
						<span class="h6 u-margin-bottom-10">Business Directory</span> 
						
						<span class="h3 u-margin-bottom-10">
							<a href="/business-directory/?markets[]=<? echo get_the_ID(); ?>"> Find a Business&nbsp;›</a> 
						</span>
					
						<div class="u-font-size-small">
							<p>We work with a whole range of national and international financial services firms based in Jersey. Browse our business directory for more information, or to search for a specific organisation.</p>
						</div>
					</div>
				</div>
				<div class="c-promo-block__link"> 	
					<a class="c-button red" href="/business-directory/?markets[]=<? echo get_the_ID(); ?>"> Find Businesses › </a>
				</div>
			</div>
		</div>
	</div>
</div>