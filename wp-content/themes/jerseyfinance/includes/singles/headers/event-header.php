<!-- Article headers with progress meter -->

<header class="post-header"> 
	<div class="read-progress">
		<div class="progress"></div>
	</div>

	<div class="post-header__wrapper">
		<div class="post-header__title">
			<? the_title(); ?>
		</div>

		<? if( $tabs ): ?>
			<div class="post-header__anchors">
				<div class="post-header__anchors-wrapper">
					<div class="post-header__anchors-container">
						<? foreach( $tabs as $key => $tab ): ?>
							<a href="#<? echo $tab; ?>" class="tab-<? echo $tab; ?>">
								<span class="c-tabs__text">
									<? echo $key; ?>
								</span>
							</a>
						<? endforeach; ?>
					</div>

					<div class="expand">
						<ul>
							<? foreach( $tabs as $key => $tab ): ?>
								<li>
									<a href="#<? echo $tab; ?>" class="tab-<? echo $tab; ?>">
										<span class="c-tabs__text">
											<? echo $key; ?>
										</span>
									</a>
								</li>
							<? endforeach; ?>
						</ul>
					</div>
				</div>
			</div>
		<? endif; ?>

		<? include( locate_template('/includes/singles/headers/actions.php') ); ?>
	</div>
</header>