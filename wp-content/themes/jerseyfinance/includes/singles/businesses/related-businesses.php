<? 	
	// Output subsidiaries
	$related_businessses = get_field('related_businesses');
	
	if( $related_businessses ): ?>

		<div class="container">

			<div class="c-title">
				Subsidiaries of <? the_title(); ?>
			</div>

			<div class="c-grid flex ontablet-middle-make-col-6 onmobile-make-col-12">
				<? foreach( $related_businessses as $post ): 
					setup_postdata($post);

					include( locate_template( 'includes/archives/business-item.php' ) );

				endforeach; ?>
				<? wp_reset_postdata(); ?>
			</div>

		</div>

	<? endif; 
?>

<? 	
	// See if any businesses have marked this one as a child
	$parents = get_posts(array(
		'post_type' => 'all-businesses',
		'meta_query' => array(
			array(
				'key' => 'related_businesses', // name of custom field
				'value' => '"' . get_the_ID() . '"', // matches exactly "123", not just 123. This prevents a match for "1234"
				'compare' => 'LIKE'
			)
		)
	));

	if( $parents ): ?>

		<div class="container">

			<div class="c-title">
				Parent of <? the_title(); ?>
			</div>

			<div class="c-grid flex ontablet-middle-make-col-6 onmobile-make-col-12">
				<? foreach( $parents as $post ): 
					setup_postdata($post);

					include( locate_template( 'includes/archives/business-item.php' ) );

				endforeach; ?>
				<? wp_reset_postdata(); ?>
			</div>

		</div>

	<? endif; 
?>