
<?
	$business_id 	= get_the_ID();
	$post_types 	= array('all-news', 'all-events', 'all-vacancies');

	foreach($post_types as $post_type): 
		$meta_query = [];

		$meta_query[] = array(
			'key'			=> 'business',
			'value'		=> $business_id,
			'compare'	=> 'IN'
		);

		$args = array(
			'post_type'			=> $post_type,
			'posts_per_page' 	=> '4',
			'meta_query'		=> $meta_query
		);

		$posts_query = new WP_Query($args);

		if( $posts_query->have_posts() ): ?>

			<div class="container">

				<div class="c-title u-margin-bottom-20">
					<? the_title(); ?>'s <? echo get_post_type_object($post_type)->labels->name; ?>
				</div>

				<div class="c-feature-grid c-feature-grid__grid-view u-margin-bottom-50">
					<? $i = 10; ?>

					<? while( $posts_query->have_posts() ) : $posts_query->the_post(); ?>

						<? include( locate_template('includes/archives/post-item.php') ); ?>

					<? endwhile; ?>
					<? wp_reset_postdata(); ?>
				
				</div>

			</div>

		<? endif; ?>

	<? endforeach;
?>
