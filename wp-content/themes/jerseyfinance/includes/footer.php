	<? 
		if( get_post_type() !== 'all-news' && get_post_type() !== 'all-events' ):
			include( locate_template('includes/components/c-footer-contact.php') ); 
		endif;
	?>

		<div class="c-footer-banner">
			<div class="container">
				<div class="c-footer-banner__wrapper">
					<div class="c-footer-banner__text h3">
						Search for a Jersey business
					</div>

					<div class="c-footer-banner__link">
						<a href="/business-directory/" class="c-button white">Directory</a>
					</div>
				</div>
			</div>
		</div>

		<? 
			// Include cookie notice
			if( !get_query_var( 'amp' ) ):
				include( locate_template('includes/components/c-cookie-notice.php') ); 
			endif;
		?>

		<footer class="site-footer">
			<div class="container">
				<div class="site-footer__wrapper">
					
					<div class="site-footer__social">
						<a href="/">
							<svg class="lion">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#lion"></use>
							</svg>
						</a>

						<a href="http://twitter.com/jerseyfinance/" target="_blank">
							<svg class="twitter">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#twitter"></use>
							</svg>
						</a>
						<a href="http://www.linkedin.com/company/jersey-finance/" target="_blank">
							<svg class="linkedin">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#linkedin"></use>
							</svg>
						</a>
						<a href="http://www.youtube.com/user/JerseyFinance/" target="_blank">
							<svg class="youtube">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#youtube"></use>
							</svg>
						</a>
						<a href="http://www.facebook.com/jerseyfinanceeducation/" target="_blank">
							<svg class="facebook">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#facebook"></use>
							</svg>
						</a>
						<a href="https://www.instagram.com/jerseyfinance/" target="_blank">
							<svg class="instagram">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#instagram"></use>
							</svg>
						</a>
					</div>
					
					<div class="site-footer__legal">
						<ul>
							<li>Copyright <? echo date('Y'); ?> Jersey Finance Limited</li>
							<?
								wp_nav_menu(array(
									'menu'         => 'Footer Links',
									'container'    => false,
									'items_wrap' 	=> '%3$s'
								));
							?>
						</ul>
					</div>

				</div>
			</div>
		</footer>
		
   	<? wp_footer(); ?>
	</body>
</html>