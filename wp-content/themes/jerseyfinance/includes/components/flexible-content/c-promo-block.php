<? 
	$promo_content = get_sub_field('promo_content');

	$heading = $promo_content['heading'];
	$title 	= $promo_content['title'];
	$text 	= $promo_content['text'];

	$link 	= get_sub_field('link');
?>

<div class="c-promo-block">
	<div class="u-pattern-wrapper u-padding-bottom-50">
		<div class="container">

			<div class="c-promo-block__wrapper">

				<div class="c-promo-block__content" data-aos="fade-right">
					<div class="c-promo-block__box u-background-white u-box-shadow-light">
						<span class="h6 u-margin-bottom-10">
							<? echo $heading; ?>
						</span>

						<span class="h3 u-margin-bottom-10">
							<? echo $link ? '<a href="' . $link['url'] . '">' : ''; ?>
								<? echo $title; ?>&nbsp;›
							<? echo $link ? '</a>' : ''; ?>
						</span>

						<div class="u-font-size-small">
							<? echo $text; ?>
						</div>
					</div>
				</div>

				<? if( $link ): ?>
					<div class="c-promo-block__link" data-aos="fade-left">
						<a class="c-button red" href="<? echo $link['url']; ?>">
							<? echo $link['title']; ?>&nbsp;›
						</a>
					</div>
				<? endif; ?>

			</div>

		</div>
	</div>
</div>