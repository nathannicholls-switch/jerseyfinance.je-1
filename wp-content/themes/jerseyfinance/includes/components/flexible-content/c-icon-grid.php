<? 
	$masonry 	= get_sub_field('use_masonry_layout');

	$col_count 	= count(get_sub_field('items')); 

	if( have_rows('items') ): ?>

		<? 
			// generate a unique ID for this component - used to trigger the AOS animation
			$element_id = 'component-' . md5(uniqid(rand(), true));

			// A delay for AOS that we will increment
			$delay = 0; 
		?>

		<div class="container">
			<div id="<? echo $element_id; ?>" class="c-icon-grid">
				<div class="c-grid <? echo $masonry == 'true' ? 'masonry' : ''; ?> large-gutter ontablet-middle-make-col-6 onmobile-make-col-12">

					<? while ( have_rows('items') ) : the_row(); ?>

						<div class="c-grid__col-<? echo $col_count > 2 ? '4' : '6'; ?> c-icon-grid__item <? echo $masonry == 'true' ? 'masonry-item' : ''; ?>" data-aos="fade-up" data-aos-delay="<? echo $delay ? $delay : '200'; ?>" <? echo $element_id ? 'data-aos-anchor="#' . $element_id . '"' : ''; ?>>

							<? if( get_sub_field('icon') ): ?>
								<div class="c-icon-grid__icon">
									<svg class="<? the_sub_field('icon'); ?>">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#<? the_sub_field('icon'); ?>"></use>
									</svg>
								</div>
							<? endif; ?>

							<div class="c-icon-grid__title u-font-family-bright u-font-weight-bold u-margin-bottom-15">
								<? echo get_sub_field('title'); ?>
							</div>

							<? echo get_sub_field('text')  ? '<div class="c-icon-grid__text">' . get_sub_field('text') . '</div>' : '' ; ?>
						
						</div>

						<?
							// increase delay 
							$delay = $delay + 200;
						?>

					<? endwhile; ?>

				</div>
			</div>
		</div>

	<? endif;
?>