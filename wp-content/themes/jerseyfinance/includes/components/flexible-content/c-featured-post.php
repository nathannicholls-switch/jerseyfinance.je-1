<?
	$featured_post = get_sub_field('post');
	$size 			= get_sub_field('style');

	if( $featured_post ):
		$post = $featured_post;
		setup_postdata( $post ); 
		
		$featured_image = get_field('featured_image'); ?>
		
		<div class="container <? echo $size == 'large' ? 'wide' : '' ; ?>">
			<div class="c-featured-post <? echo $size ?>">
				<div class="c-featured-post__wrapper">

					<div class="c-featured-post__image" data-aos="fade-right">

						<? if( $featured_image ): ?>
							<a href="<? the_permalink(); ?>">
								<img src="<? echo $featured_image['sizes']['featured-post-' . $size]; ?>" alt="<? $featured_image['alt']; ?>" />
							</a>
						<? endif;?>
					
					</div>

					<div class="c-featured-post__content" data-aos="fade-left" data-aos-delay="500">
						<div class="c-featured-post__content_wrapper">
							
							<h6 class="c-featured-post__heading">
								<? echo get_post_type() !== 'page' ? get_post_type_object( get_post_type() )->label : 'Featured'; ?>
							</h6>

							<div class="c-featured-post__title">
								<? the_title(); ?>
							</div>
						
							<div class="c-featured-post__text u-font-size-small">
								
								<? if( get_field('featured_summary') ): ?>
									<? the_field('featured_summary'); ?>
								<? endif; ?>

								<div class="c-featured-post__link">
									<a href="<? the_permalink(); ?>">Read more ›</a>
								</div>
							</div>
						
						</div>						
					</div>

				</div>
			</div>
		</div>

		<? wp_reset_postdata(); ?>

	<? endif;
?>