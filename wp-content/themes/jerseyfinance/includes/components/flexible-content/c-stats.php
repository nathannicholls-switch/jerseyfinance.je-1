<div class="c-stats">

	<? if( have_rows('stats') ): ?>

		<?
			// generate a unique ID for this component - used to trigger the AOS animation
			$element_id = 'component-' . md5(uniqid(rand(), true));

			// A delay for AOS that we will increment
			$delay = 0; 
		?>

		<div class="container wide">
			<div id="<? echo $element_id; ?>" class="c-stats__wrapper">
				<? while( have_rows('stats') ): the_row();
		
					// Include individual stat - using default subfields
					get_template_partial('includes/partials/stat', array(
						'element_id' 	=> $element_id,
						'delay'			=> $delay
					)); 	

					// increase delay 
					$delay = $delay + 200;										

				endwhile; ?>
			</div>
		</div>
	<? endif; ?>

</div>