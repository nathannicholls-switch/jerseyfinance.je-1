<? 
	$number_of_days = count(get_sub_field('days')); 
	
	// Title between days is set via data-title, we just need the starting title
	$starting_title = get_sub_field('days')[0]['title'];
?>

<? if( have_rows('days') ): 
	$day_count = 1; 
	?>

	<div class="c-agenda">
		<div class="u-pattern-wrapper less u-padding-bottom-50">
			<div class="container">

				<div class="c-agenda__slider">

					<!-- The title and navigation -->

					<div class="c-agenda__nav">

						<div class="c-agenda__wrapper">
							<div class="c-agenda__row">
								<div class="c-agenda__cell"></div>

								<div class="c-agenda__cell c-agenda__day">
								
									<div class="c-agenda__day-box">
										<div class="prev disabled"></div>

										<div class="day">
											<? echo !empty($starting_title) ? $starting_title : 'Day ' . $day_count; ?>
										</div>

										<div class="next <? echo $number_of_days <= 1 ? 'disabled' : ''; ?>"></div>
									</div>

								</div>

								<div class="c-agenda__cell"></div>
							</div>
						</div>

					</div>

					<? while( have_rows('days') ): the_row(); ?>

						<?
							$title = get_sub_field('title') ? get_sub_field('title') : 'Day ' . $day_count;
						?>

						<div 
							class="c-agenda__slide <? echo $day_count == 1 ? 'active' : '' ; ?>" 
							<? echo $day_count == 1 ? 'style="display: block;"' : '' ; ?>
							data-title="<? echo $title; ?>"
							>

							<!-- Agenda slides -->

							<? if( have_rows('items') ):
								$row_count = 0; ?>

								<div class="c-agenda__timeline">
									<div class="c-agenda__wrapper">
								
										<? while( have_rows('items') ): the_row(); ?>
											<? 
												$time 			= get_sub_field('time');
												
												// If the user has provided a time, format it with spaces between the ":"
												if( preg_match( '/\d{1,2}:\d{2}/', $time ) ):
													$time	= date( 'h : i', strtotime($time) );
												endif;

												// Save the description HTML as a var so we can output it left/right later on
												$description = '<div class="c-agenda__description-box u-background-white u-box-shadow-light">';
													$description .= '<div class="c-agenda__text">';
														$description .= '<div class="text-height u-font-size-small">';
															$description .= get_sub_field('description');
														$description .= '</div>';
													$description .= '</div>'; 
													$description .= '<div class="c-agenda__more"><span class="label">More</span> <svg class="arrow"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow"></use></svg></div>';
												$description .= '</div>'; 
											?>

											<div class="c-agenda__row">
												<div class="c-agenda__cell c-agenda__description left" data-aos="fade-left">
													<? echo $row_count % 2 !== 0 ? $description : '' ; ?>
												</div>

												<div class="c-agenda__cell c-agenda__time">
													<div class="c-agenda__time-box u-box-shadow-light">
														<? echo $time; ?>
													</div>
												</div>

												<div class="c-agenda__cell c-agenda__description right" data-aos="fade-right">
													<? echo $row_count % 2 !== 0 ? '' : $description ; ?>
												</div>
											</div>
										
											<? $row_count++; ?>

										<? endwhile; ?>

									</div>
								</div>

							<? endif;?>

						</div>

						<? $day_count++ ?>

					<? endwhile; ?>

				</div>
				
			</div>
		</div>
	</div>

<? endif; ?>