<?
	$post_type = get_sub_field('post_type');

	if( $post_type == 'custom-selection' ):
		$latest_posts = get_sub_field('posts');

	else:
		$args = array(
			'post_type'			=> $post_type,
			'posts_per_page'	=> 4
		);

		$latest_posts = new WP_Query( $args );

		// Grab only the posts, so we can `foreach` over them
		$latest_posts = $latest_posts->posts;
	endif;
?>

<div class="container">
	<div class="c-latest-posts u-margin-bottom-60">
		<div class="c-feature-grid c-feature-grid__grid-view">

			<? 
				$i = 10;

				foreach( $latest_posts as $post ):
					setup_postdata($post); 
					
					include( locate_template('/includes/archives/post-item.php') ); 
				endforeach; 
				
				wp_reset_postdata(); 
			?>

		</div>
	</div>	
</div>