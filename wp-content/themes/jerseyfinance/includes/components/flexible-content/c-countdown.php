<div class="container u-margin-bottom-60 size-<? echo get_sub_field('size'); ?>" data-aos="fade-up" data-delay="100">
	<?
		//	Include countdown component with flexible content date
		get_template_partial( 'includes/partials/work/countdown', array(
			'heading'			=> get_sub_field('heading'),
			'countdown_date' 	=> get_sub_field( 'countdown_date', false, false )
		));
	?>
</div>