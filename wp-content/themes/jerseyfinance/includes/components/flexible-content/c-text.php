<?
	// Search the HTML for any inline images and replace with amp-img
	// (also add closing tag and try to retain width and height attributes)
	$text = get_query_var('amp') ? ampify_html(get_sub_field('text')) : get_sub_field('text');
?> 

<div class="container">

	<? if( get_sub_field('two_column') !== 'yes' ): ?>

		<div class="c-text c-text__single" data-aos="fade-up">
			<? echo $text; ?>
		</div>

	<? else: ?>

		<?
			$two_text = get_sub_field('first_column_text') && get_sub_field('text') ? true : false;

			$heading = get_sub_field('heading') ? '<div class="c-text__heading h6">'. get_sub_field('heading') . '</div>' : '';
			$title = get_sub_field('title') ? '<div class="c-text__title h2">'. get_sub_field('title') . '</div>' : '';
		?>

		<div class="c-text c-text__two-column">

			<? 
				// If there is two columns of TEXT, put title and heading outside grid so that text is vertically aligned
				echo $two_text ? $heading . $title : ''; 
			?>

			<div class="c-grid onmobile-make-col-12">
				<div class="c-grid__col-6" data-aos="fade-right">
					<div class="left-col">
						<? echo !$two_text ? $heading . $title : ''; ?>

						<div class="<? echo !$two_text ? 'u-font-size-small' : '' ; ?>">
							<? echo get_sub_field('first_column_text'); ?>
						</div>
					</div>
				</div>
				<div class="c-grid__col-6" data-aos="fade-left" data-aos-delay="500">
					<div class="right-col <? echo !$two_text ? 'u-font-size-small' : '' ; ?>">
						<? echo $text; ?>
					</div>
				</div>
			</div>
		</div>

	<? endif; ?>
	
</div>