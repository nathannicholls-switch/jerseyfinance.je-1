<? 
	$google_map = get_sub_field('google_map');
?>

<div class="c-google-map">
	<div class="container wide">
		<div class="c-google-map__map acf-map">
			<div class="marker" data-lat="<?php echo $google_map['lat']; ?>" data-lng="<?php echo $google_map['lng']; ?>"></div>
		</div>
	</div>
</div>

<? if( $google_map ):  
	// Only include Google Maps once
	if( !function_exists('include_google_maps') ):
		function include_google_maps() { ?>
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_Qv26-GFSqOKim0Mw6Lu7km7D-3vSd5A"></script>

			<script type="text/javascript">
				!function(e){function n(n){var o,a,t=n.find(".marker"),g={zoom:16,center:new google.maps.LatLng(0,0),mapTypeId:google.maps.MapTypeId.ROADMAP},r=new google.maps.Map(n[0],g);return r.markers=[],t.each(function(){!function(e,n){var o=new google.maps.LatLng(e.attr("data-lat"),e.attr("data-lng")),a=new google.maps.Marker({position:o,map:n});if(n.markers.push(a),e.html()){var t=new google.maps.InfoWindow({content:e.html()});google.maps.event.addListener(a,"click",function(){t.open(n,a)})}}(e(this),r)}),o=r,a=new google.maps.LatLngBounds,e.each(o.markers,function(e,n){var o=new google.maps.LatLng(n.position.lat(),n.position.lng());a.extend(o)}),1==o.markers.length?(o.setCenter(a.getCenter()),o.setZoom(16)):o.fitBounds(a),r}e(document).ready(function(){e(".acf-map").each(function(){n(e(this))})})}(jQuery);
			</script>
		<? }
		
		add_action( 'wp_footer', 'include_google_maps', 100 );
	endif;
endif; ?>