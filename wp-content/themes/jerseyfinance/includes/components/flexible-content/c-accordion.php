<? if( have_rows('accordion') ): ?>

	<? 
		// generate a unique ID for this component - used to trigger the AOS animation
		$element_id = 'component-' . md5(uniqid(rand(), true));

		// A delay for AOS that we will increment
		$delay = 0; 
	?>

	<div class="container">
		<<? echo get_query_var( 'amp' ) ? 'amp-accordion disable-session-states' : 'div' ; ?> id="<? echo $element_id; ?>" class="c-accordion">

			<? while( have_rows('accordion') ): the_row(); ?>

				<? if( get_query_var( 'amp' ) ): ?>

					<section class="c-accordion__block">
						<header class="c-accordion__title">
							<? echo get_sub_field('title'); ?>
						</header>
						<div class="c-accordion__content">
							<div class="c-accordion__content-wrapper u-font-size-small">
								<? echo get_sub_field('content'); ?>
							</div>
						</div>
					</section>

				<? else: ?>

					<div class="c-accordion__block" data-aos="fade-up" data-aos-delay="<? echo $delay ? $delay : '200'; ?>" <? echo $element_id ? 'data-aos-anchor="#' . $element_id . '"' : ''; ?>>
						<div class="c-accordion__title">
							<? echo get_sub_field('title'); ?>
						</div>

						<div class="c-accordion__content">
							<div class="c-accordion__content-wrapper u-font-size-small">
								<? echo get_sub_field('content'); ?>
							</div>
						</div>
					</div>

					<?
						// increase delay 
						$delay = $delay + 200;
					?>

				<? endif; ?>

			<? endwhile; ?>

		</<? echo get_query_var( 'amp' ) ? 'amp-accordion disable-session-states' : 'div' ; ?>>
	</div>
<? endif; ?>