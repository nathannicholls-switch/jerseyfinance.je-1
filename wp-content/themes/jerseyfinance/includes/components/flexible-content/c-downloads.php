<?
	// New component
	if( have_rows('download_list') ): ?>
		<?
			if( !isset($download_count) ):
				// Used to create anchor links for each download
				$download_count = 1;
			endif; 
		?>

		<div class="container">
			<div id="<? echo $element_id; ?>" class="c-downloads">

				<? while( have_rows('download_list') ): the_row(); ?>

					<div class="c-downloads__download" id="download-<? echo $download_count++; ?>" data-aos="fade-right">
						<a href="<? echo get_sub_field('file')['url']; ?>" target="_blank">
							<? echo get_sub_field('title'); ?> ›
						</a>

						<span class="c-downloads__file-type">
							<? echo convertMime( get_sub_field('file')['mime_type'] ); ?>
						</span>
					</div>

				<? endwhile; ?>
			
			</div>
		</div>
	
	<? endif;
?>