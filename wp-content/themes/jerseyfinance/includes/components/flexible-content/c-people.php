<? if( have_rows('people') ): ?>
	<? 
		// generate a unique ID for this component - used to trigger the AOS animation
		$element_id = 'component-' . md5(uniqid(rand(), true));

		// A delay for AOS that we will increment
		$delay = 0; 
	?>

	<div class="container">
		<div id="<? echo $element_id; ?>">
			<div class="c-grid ontablet-middle-make-col-6 onmobile-make-col-12 flex center u-margin-bottom-40">
				<? 
					while( have_rows('people') ): the_row(); 
				
						if( get_sub_field('type') == 'jfl' ):

							$post = get_sub_field('person');

							if( $post->post_status == 'publish' ): ?>

								<div class="c-grid__col-4 u-margin-bottom-30" data-aos="fade-up" data-aos-delay="<? echo $delay ? $delay : '200'; ?>" <? echo $element_id ? 'data-aos-anchor="#' . $element_id . '"' : ''; ?>>
							
									<? include( locate_template('includes/partials/key-contact.php') ); ?>
								
								</div>

							<? endif;

							wp_reset_postdata();

						else:

							$image = 			get_sub_field('custom_person')['image'];
							$name = 				get_sub_field('custom_person')['name'];
							$job_title = 		get_sub_field('custom_person')['job_title'];
							$email = 			get_sub_field('custom_person')['email'];
							$direct_line = 	get_sub_field('custom_person')['direct_line'];
							// $social_media = 	get_sub_field('custom_person')['social_media']; ?>

							<div class="c-grid__col-4 u-margin-bottom-30" data-aos="fade-up" data-aos-delay="<? echo $delay ? $delay : '200'; ?>" <? echo $element_id ? 'data-aos-anchor="#' . $element_id . '"' : ''; ?>>

								<?
									get_template_partial( 'includes/partials/key-contact', array(
										'image' 			=> $image,
										'permalink' 	=> false,
										'name' 			=> $name,
										'job_title' 	=> $job_title,
										'email' 			=> $email,
										'phone' 			=> $direct_line,
									));
								?>

							</div>

						<? endif; 

						// increase delay 
						$delay = $delay + 200;

					endwhile;
				?>
			</div>
		</div>
	</div>

<? endif; ?>