<div class="container wide <? echo get_sub_field('reduce_width') ? 'narrow' : ''; ?>">
	<div class="c-image" data-aos="fade-up">
		<div class="c-image__wrapper">
			<?
				$link = get_sub_field('link'); 
			?>

			<? echo $link ? '<a href="' . $link['url'] . '" target="' .  $link['target'] . '">' : '' ; ?>
			 	<? if( get_query_var( 'amp' ) ): ?>
					<amp-img 
						alt="<? echo get_sub_field('image')['alt']; ?>" 
						src="<? echo get_sub_field('image')['sizes']['full-width']; ?>"  
						layout="responsive"  
						width="<? echo get_sub_field('image')['sizes']['full-width-width']; ?>" 
						height="<? echo get_sub_field('image')['sizes']['full-width-height']; ?>"
						lightbox>
					</amp-img>
				<? else: ?>
					<img src="<? echo get_sub_field('image')['sizes']['full-width']; ?>" alt="<? echo get_sub_field('image')['alt']; ?>" />
				<? endif; ?>
			<? echo $link ? '</a>' : '' ; ?>

			<div class="u-caption">
				<? echo get_sub_field('image')['caption']; ?>
			</div>
		</div>
	</div>
</div>