<?
	if( have_rows('links') ): ?>

		<? 
			// generate a unique ID for this component - used to trigger the AOS animation
			$element_id = 'component-' . md5(uniqid(rand(), true));

			// A delay for AOS that we will increment
			$delay = 0; 
		?>

		<div class="container">
			<div id="<? echo $element_id; ?>" class="c-link-grid">
				<div class="c-grid ontablet-portrait-make-col-4 onmobile-make-col-12">

					<? while ( have_rows('links') ) : the_row(); ?>

						<div class="c-grid__col-3 c-link-grid__item" data-aos="fade-up" data-aos-delay="<? echo $delay ? $delay : '200'; ?>" <? echo $element_id ? 'data-aos-anchor="#' . $element_id . '"' : ''; ?>>
					
							<a 
								class="c-link-grid__link u-font-family-bright u-font-weight-bold u-margin-bottom-10" 
								href="<? echo get_sub_field('link')['url']; ?>"
								<? echo get_sub_field('link')['target'] ? 'target="_blank"' : '' ; ?>>
									<? echo get_sub_field('link')['title']; ?>&nbsp;›
							</a>

							<? echo get_sub_field('text')  ? '<div class="c-link-grid__text u-font-size-small">' . get_sub_field('text') . '</div>' : '' ; ?>
						
						</div>

						<?
							// increase delay 
							$delay = $delay + 200;
						?>

					<? endwhile; ?>

				</div>
			</div>
		</div>

	<? endif;
?>