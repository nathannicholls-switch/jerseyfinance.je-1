<?
	$iframe = get_sub_field('video');

	if( $iframe ):

		$muted = array_key_exists('muteVideos', $_COOKIE) && $_COOKIE['muteVideos'] == 'true' ? true : false;

		if( $muted ):
			// use preg_match to find iframe src
			preg_match('/src="(.+?)"/', $iframe, $matches);
			$src = $matches[1];

			// Set the video to muted based on user cookie
			$params = array(
				'muted'			=> 1
			);

			$new_src = add_query_arg($params, $src);

			$iframe = str_replace($src, $new_src, $iframe);  
		endif; ?>

		<div class="container <? echo get_sub_field('reduce_width') ? 'narrow' : ''; ?>">
			<div class="c-video" data-aos="fade-up">

				<div class="c-video__details">
					<? if( get_sub_field('title') ): ?>
						<span class="detail">
							<strong>Video:</strong> <? echo get_sub_field('title'); ?>
						</span>

						<span class="divider">|</span>
					<? endif; ?>

					<? if( get_sub_field('duration') ): ?>
						<span class="detail">
							<svg class="duration">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#duration"></use>
							</svg>
							Est time to watch: <strong class="u-colour-red"><? echo get_sub_field('duration'); ?></strong>
						</span>

						<span class="divider">|</span>
					<? endif; ?>

					<? if( !get_query_var( 'amp' ) ): ?>

						<span class="detail">
							Sound: 
							<? echo $muted ? '<strong class="mute-state">No</strong>' : '<strong class="mute-state">Yes</strong>'; ?>
						</span>

						<span class="divider">|</span>

						<span class="detail u-colour-red u-decoration-underline">
							<strong>
								<a href="#" class="mute-toggle" onclick="return false;">
									<? echo $muted ? 'Unmute all videos' : 'Mute all videos'; ?>
								</a>
							</strong>
						</span>
					
					<? endif; ?>
				</div>

				<div class="c-video__wrapper">
					<? 
						if( get_query_var( 'amp' ) ):
							$iframe = str_replace('<iframe', '<amp-iframe layout="responsive" sandbox="allow-scripts allow-same-origin"', $iframe);  
							$iframe = str_replace('</iframe', '</amp-iframe', $iframe);  
							echo $iframe;						
						else:
							echo $iframe;
						endif; 
					?>
				</div>		
			</div>
		</div>

	<? endif; 
?>