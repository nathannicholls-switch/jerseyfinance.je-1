<div class="c-cookie-notice notice" data-cookie="cookie-notice">
	<svg class="alert">
		<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#alert"></use>
	</svg>
	
	<p>
		Jersey Finance uses cookies to deliver better functionality and to enhance your experience of our website.<br>
		Read about <a href="/cookie-policy/" target="_blank">how we use cookies</a> and how you can control them. Continued use of this site indicates that you accept this policy.
	</p>

	<div class="c-cookie-notice__close">
		<svg class="close">
			<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#close"></use>
		</svg>
	</div>
</div>