<? 
	$crop_image = isset($template_args['crop_image']) ? $template_args['crop_image'] : true;
	$image 		= isset($template_args['image']) ? $template_args['image'] : get_field('profile_image');

	// If we want the image cropped into a circle, use the thumbnail crop size, otherwise assume it's a logo
	if( $image && $crop_image ):
		$image 			= $image['sizes']['thumbnail']; 
		$image_class 	= 'rounded';
	else:
		$image 			= $image['sizes']['logo-full'];
	endif;

	$heading 	= isset($template_args['heading']) ? $template_args['heading'] : '';
	$permalink 	= isset($template_args['permalink']) ? $template_args['permalink'] : get_the_permalink();
	$name 		= isset($template_args['name']) ? $template_args['name'] : get_the_title();
	$job_title 	= isset($template_args['job_title']) ? $template_args['job_title'] : get_field('job_title');
	$email 		= isset($template_args['email']) ? $template_args['email'] : get_field('email');
	$phone 		= isset($template_args['phone']) ? $template_args['phone'] : get_field('phone');
	
	// if( get_post_type() == 'all-news' || get_post_type() == 'all-news' || get_post_type() == 'all-news' ):
	// 	$onclick_name = 'Author';
	// elseif( get_post_type() == 'all-events' ):
	// 	$onclick_name = 'Speaker';
	// elseif():
	// 	$onclick_name = 'Author';
	// else:
	// 	$onclick_name = 'Key Contact';
	// endif;

	// $onclick_event = 'onclick="dataLayer.push({ \'memberContact\': \'' . get_the_title() . '\' })"';
?>

<div class="c-key-contact">
	<div class="c-key-contact__wrapper">

		<? if( $image ): ?>
			<div class="c-key-contact__image">
				<div class="c-key-contact__image-wrapper <? echo $image_class ? $image_class : ''; ?>">
					<? echo $permalink ? '<a href="' . $permalink . '">' : ''; ?>
						<img src="<? echo $image; ?>" alt="<? echo $name; ?>" data-object-fit="contain" /> 
					<? echo $permalink ? '</a>' : ''; ?>
				</div>
			</div>
		<? endif; ?>

		<div class="c-key-contact__text">
			<p class="u-colour-dark-grey">
				<? if( $heading ): ?>
					<strong class="<? echo $text_colour ? $text_colour : 'u-colour-black'; ?>"><? echo $heading; ?></strong>
					<br>
				<? endif; ?>

				<? echo $permalink ? '<a href="' . $permalink . '"' : '<span '; ?> class="h5 u-colour-red u-margin-bottom-5">
					<? echo $name; ?><? echo $permalink ? '&nbsp;›' : ''; ?>
				</<? echo $permalink ? 'a' : 'span'; ?>>

				<? echo $job_title ? $job_title . '<br>' : ''; ?>

				<? if( $permalink ): ?>

					<? if( $email ): ?>
						<a href="mailto:<? echo $email; ?>">email ›</a>
					<? endif; ?>
					
					<span class="u-colour-red">/</span> 
					<a href="<? the_permalink(); ?>">profile ›</a>
					
				<? else: ?>

					<? if( $email ): ?>
						<a href="mailto:<? echo $email; ?>"><? echo $email; ?></a><br>
					<? endif; ?>

					<? if( $phone ): ?>
						<a href="tel:<? echo $phone; ?>"><? echo $phone; ?></a>
					<? endif; ?>

				<? endif; ?>
			</p>
		</div>

	</div>

	<?
		if( get_sub_field('bio') ): ?>
			<p class="u-margin-top-20">
				<? the_sub_field('bio'); ?>
			</p>
		<? endif; 
	?>
</div>