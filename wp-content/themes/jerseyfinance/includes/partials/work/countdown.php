<?	
	$heading 			= isset($template_args['heading']) ? $template_args['heading'] : get_field('heading');
	
	if( empty($heading) && get_post_type() == 'all-work' ):
		$heading 		= 'Time to respond';
	endif;

	$countdown_date 	= isset($template_args['countdown_date']) ? $template_args['countdown_date'] : get_field('countdown_date', false, false);

	// Convert to date time object so we can compare
	$countdown_datetime 	= new DateTime( $countdown_date );
	$now 						= new DateTime( "now" );

	// Get the difference between the dates so we can output the starting days/hours/minutes/seconds values
	$interval = $now->diff($countdown_datetime);
?>

<div class="c-countdown" data-date="<? echo $countdown_datetime->format('U'); ?>">
	<div class="c-countdown__wrapper">
		<? if( $heading ): ?>
			<span class="h6"><? echo $heading; ?></span>
		<? endif; ?>

		<div class="u-table">
			<div class="c-countdown__days u-table-cell">
				<span class="value"><? echo $interval->days; ?></span>
				<span class="label">Days</span>
			</div>
			<div class="c-countdown__hours u-table-cell">
				<span class="value"><? echo $interval->h; ?></span>
				<span class="label">Hours</span>
			</div>
			<div class="c-countdown__minutes u-table-cell">
				<span class="value"><? echo $interval->i; ?></span>		
				<span class="label">Minutes</span>
			</div>
			<div class="c-countdown__seconds u-table-cell">
				<span class="value"><? echo $interval->s; ?></span>
				<span class="label">Seconds</span>
			</div>
		</div>
	</div>
</div>