<? 
	$label 	= isset($template_args['label']) ? $template_args['label'] : '';
	$value 	= isset($template_args['value']) ? $template_args['value'] : '';
	$name 	= isset($template_args['name']) ? $template_args['name'] : '';

	$terms = filter_input( INPUT_GET, $name, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_REQUIRE_ARRAY ); 

	if( $label && $value ): ?>
		<div class="custom-checkbox">
			<input 
				id="<? echo $name . '_' . $value; ?>" 
				type="checkbox" 
				name="<? echo $name; ?>[]" 
				value="<? echo $value; ?>" 
				<? echo !empty($terms) && in_array($value, $terms)? 'checked' : ''; ?> 
			/>

			<label for="<? echo $name . '_' . $value; ?>" class="checkbox">				
				<? echo $label; ?>
			</label>
		</div>
	<? endif; 
?>