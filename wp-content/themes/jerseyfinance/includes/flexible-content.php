<? 
	if( have_rows('fields') ):
		while ( have_rows('fields') ) : the_row();

			if( get_row_layout() == 'title' ):

				include( locate_template('/includes/components/flexible-content/c-title.php' ) );

			elseif( get_row_layout() == 'text' ): 

				include( locate_template('/includes/components/flexible-content/c-text.php' ) );

			elseif( get_row_layout() == 'promo_block' ): 

				include( locate_template('/includes/components/flexible-content/c-promo-block.php' ) );

			elseif( get_row_layout() == 'image' ): 
				
				include( locate_template('/includes/components/flexible-content/c-image.php' ) );

			elseif( get_row_layout() == 'gallery' ): 
				
				include( locate_template('/includes/components/flexible-content/c-gallery.php' ) );

			elseif( get_row_layout() == 'video' ): 
				
				include( locate_template('/includes/components/flexible-content/c-video.php' ) );

			elseif( get_row_layout() == 'video_grid' ): 
				
				include( locate_template('/includes/components/flexible-content/c-video-grid.php' ) );

			elseif( get_row_layout() == 'downloads' ): 
				
				include( locate_template('/includes/components/flexible-content/c-downloads.php' ) );

			elseif( get_row_layout() == 'statistics' ): 
				
				include( locate_template('/includes/components/flexible-content/c-stats.php' ) );

			elseif( get_row_layout() == 'link_grid' ): 
				
				include( locate_template('/includes/components/flexible-content/c-link-grid.php' ) );

			elseif( get_row_layout() == 'icon_grid' ): 
				
				include( locate_template('/includes/components/flexible-content/c-icon-grid.php' ) );

			elseif( get_row_layout() == 'logo_grid' ): 
				
				include( locate_template('/includes/components/flexible-content/c-logo-grid.php' ) );

			elseif( get_row_layout() == 'people' ): 
				
				include( locate_template('/includes/components/flexible-content/c-people.php' ) );

			elseif( get_row_layout() == 'agenda' ): 
				
				include( locate_template('/includes/components/flexible-content/c-agenda.php' ) );

			elseif( get_row_layout() == 'image_and_text' ): 
				
				include( locate_template('/includes/components/flexible-content/c-image-text.php' ) );

			elseif( get_row_layout() == 'video_and_text' ): 
				
				include( locate_template('/includes/components/flexible-content/c-video-text.php' ) );

			elseif( get_row_layout() == 'accordion' ): 
				
				include( locate_template('/includes/components/flexible-content/c-accordion.php' ) );

			elseif( get_row_layout() == 'featured_post' ): 
				
				include( locate_template('/includes/components/flexible-content/c-featured-post.php' ) );

			elseif( get_row_layout() == 'latest_posts' ): 
				
				include( locate_template('/includes/components/flexible-content/c-latest-posts.php' ) );

			elseif( get_row_layout() == 'countdown' ): 
				
				include( locate_template('/includes/components/flexible-content/c-countdown.php' ) );

			endif;

		endwhile;
	endif;
?>