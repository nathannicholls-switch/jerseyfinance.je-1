<div class="site-overlay"></div>


<<? echo get_query_var('amp') ? 'amp-sidebar id="sidebar1" layout="nodisplay" side="right"' : 'div' ; ?> class="site-menu">
	<div class="site-menu__wrapper">
		<div class="site-menu__header">

			<? 
				// Include login/signed in link WITHOUT account menu
				get_template_partial('includes/components/account-menu', [
					'menu'   => false
				]); 
			?>

			<? if( get_query_var('amp') ): ?>
				<div class="site-menu__close" role="button" aria-label="close sidebar" on="tap:sidebar1.toggle" tabindex="0">
					<svg class="close menu-toggle">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#close"></use>
					</svg>
				</div>
			<? else: ?>
				<div class="site-menu__close">
					<svg class="close menu-toggle">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#close"></use>
					</svg>
				</div>
			<? endif; ?>
		</div>

		<?
			wp_nav_menu( array(
				'menu'         => 'Main Menu',
				'container'    => 'nav',
				'before'   		=> '<div class="expand"><svg class="arrow"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow"></use></svg></div>',
			) );
		?>

		<? if( !get_query_var('amp') ): ?>
			<div class="site-menu__footer">
				<a href="http://twitter.com/jerseyfinance/" target="_blank">
					<svg class="twitter">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#twitter"></use>
					</svg>
				</a>
				<a href="http://www.linkedin.com/company/jersey-finance/" target="_blank">
					<svg class="linkedin">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#linkedin"></use>
					</svg>
				</a>
				<a href="http://www.youtube.com/user/JerseyFinance/" target="_blank">
					<svg class="youtube">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#youtube"></use>
					</svg>
				</a>
				<a href="http://www.facebook.com/jerseyfinanceeducation/" target="_blank">
					<svg class="facebook">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#facebook"></use>
					</svg>
				</a>
				<a href="https://www.instagram.com/jerseyfinance/" target="_blank">
					<svg class="instagram">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#instagram"></use>
					</svg>
				</a>
			</div>
		<? endif; ?>

	</div>
</<? echo get_query_var('amp') ? 'amp-sidebar' : 'div' ; ?>>