<?
	$archive_page_slug = get_post_type_object( get_post_type() )->rewrite['slug'];

	$args = array(
		'pagename' => $archive_page_slug
	);

	$page_query = new WP_Query($args);

	if( $page_query->have_posts() ):
		while( $page_query->have_posts() ): $page_query->the_post(); ?>

			<? 
				// Include content from archive page
				include( locate_template('/includes/flexible-content.php') ); 
			?>

		<? endwhile;
	endif;
?>