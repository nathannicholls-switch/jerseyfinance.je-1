<div class="container">
	<div class="c-filters__modifiers">

		<div class="c-filters__modifiers-view">
			<div class="view grid-view selected" onclick="dataLayer.push({ 'resultsView': 'grid' });">
				<svg class="grid">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#grid"></use>
				</svg>

				<span>Grid</span>
			</div>

			<div class="view list-view" onclick="dataLayer.push({ 'resultsView': 'list' });">
				<svg class="list">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#list"></use>
				</svg>

				<span>List</span>
			</div>

			<? if( is_post_type_archive('all-events') ): ?>
				<div class="view calendar-view" onclick="dataLayer.push({ 'resultsView': 'calendar' });">
					<svg class="calendar-alt">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#calendar-alt"></use>
					</svg>

					<span>Calendar</span>
				</div>
			<? endif; ?>
		</div>

		<div class="c-filters__modifiers-sort">
			Sort by: 

			<?
				$sort	= filter_input( INPUT_GET, 'sort-by', FILTER_SANITIZE_SPECIAL_CHARS ); 
			?>

			<? if( is_single() || is_post_type_archive('all-news') || is_post_type_archive('all-work') || is_tax() ): ?>
				<a href="#" class="<? echo $sort == 'publish-date' || empty($sort) ? 'selected' : ''; ?>" data-value="publish-date">Latest</a> | 
			<? endif; ?>

			<? if( is_post_type_archive('all-events') ): ?>
				<a href="#" class="<? echo $sort == 'event-date' || empty($sort) ? 'selected' : ''; ?>" data-value="event-date">Upcoming</a> | 
			<? endif; ?>

			<? /* if( is_single() || is_post_type_archive('all-news') || is_post_type_archive('all-work' || is_post_type_archive('all-events') || is_tax() ) ): ?>
				<a href="#" class="<? echo $sort == 'popularity' ? 'selected' : ''; ?>" data-value="popularity">Most Popular</a>
			<? endif; */ ?>

			<input id="sort-by" name="sort-by" type="hidden" value="<? echo $sort; ?>" />
		</div>
	
	</div>
</div>