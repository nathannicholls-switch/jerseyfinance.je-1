<div class="c-timeline__item two-col <? echo $row_count % 2 == 0 ? 'even' : 'odd'; ?> <? echo $type; ?>">
	<div class="col left">
		<? echo $row_count % 2 == 0 ? $content : $feature; ?>
	</div>

	<div class="col right">
		<? echo $row_count % 2 == 0 ? $feature : $content; ?>
	</div>
</div>