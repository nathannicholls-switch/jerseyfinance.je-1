<?
	$image = get_field('social_logo') ? get_field('social_logo') : get_field('logo');

	$introduction = get_field('introduction');

	// Used to output anchored alphabet ID's
	$previous_letter 	= !isset($previous_letter) ? '' : $previous_letter;
	$first_letter 		= strtoupper( substr( get_the_title(), 0, 1) );

	if( $previous_letter !== $first_letter):
		$alphabet_anchors[] = $first_letter;
	endif;
?>

<div <? echo $previous_letter !== $first_letter ? 'id="letter-' . $first_letter . '"' : '' ; ?> class="c-grid__col-4 u-margin-bottom-20">
	<div class="c-grid__item c-business">

		<div class="c-business__logo">
			<? if( $image ): ?>
				<div class="c-business__image">
					<a href="<? the_permalink(); ?>">
						<img src="<? echo $image['sizes']['logo-full']; ?>" alt="<? the_title(); ?>" data-object-fit="contain" />
					</a>
				</div>
			<? endif; ?>

			<div class="c-business__title">
				<a href="<? the_permalink(); ?>" class="h5 u-margin-bottom-0 u-display-block u-colour-black">
					<? the_title(); ?>&nbsp;›
				</a>
			</div>
		</div>

		<? if( $introduction ): ?>
			<p class="u-font-size-small">
				<? echo shorten_text( $introduction, 200, false ); ?>
			</p>
		<? endif; ?>

		<?
			/*
				$business_categories = get_the_terms( get_the_ID(), 'all-business-categories' );

				if( $business_categories ): ?>
					<span class="u-colour-dark-grey u-font-size-12 u-margin-bottom-10">
						<?
							$i = 0;

							foreach( $business_categories as $category ):
								echo $i++ !== 0 ? ', ' : '' ;
								echo $category->name;
							endforeach;
						?>
					</span>
				<? endif; 
			*/
		?>
	</div>
</div>

<?
	$previous_letter = $first_letter;
?>