<? 
	// Let us use get_template_partial to over-ride the grid size (e.g. for related news)
	$grid_size = isset($template_args['grid_size']) ? $template_args['grid_size'] : false;	

	// If no grid size has been set, grab the settings from the post
	if( !$grid_size || empty($grid_size) ) {
		$grid_size 	= get_field('grid_size') !== 'large' ? get_field('grid_size') : 'small' ;

		// If this is the first two posts, make them large
		$grid_size = $i < 2 ? 'large' : $grid_size;
	} else {
		$grid_size = 'small';
	}

	// The image crop we need to use based on grid size
	$image_size = 'archive-grid-' . $grid_size;

	// Do we want to add a flag for "New"
	$is_new 		= get_the_date('U') > strtotime('-1 day') ? true : false;
	$is_new 		= false;
	
	// Reset to empty
	$post_categories 	= '';
	$categories 		= '';

	if( get_post_type() !== 'all-events' ):
		
		if( get_post_type() == 'all-vacancies' || get_post_type() == 'all-jfl-vacancies' ):
			$date = '';
			$subtext = '';

			// Output header text on vacancies
			$subtext .= !empty(get_field('header_content')['text']) ? get_field('header_content')['text'] . '<br>' : '';

			$subtext .= get_field('reference_number') ? '<strong>Reference:</strong> ' . get_field('reference_number') . '<br>' : '';

			$terms = get_the_terms(get_the_ID(), 'all-vacancy-terms');
			$types = get_the_terms(get_the_ID(), 'all-vacancy-types');

			if( $terms ):
				$subtext .= '<strong>Terms:</strong> ';
				$i = 0;
				foreach( $terms as $term ):
					$subtext .= $i++ > 0 ? ', ' : '';
					$subtext .= $term->name;
				endforeach;
				$subtext .= '<br>';
			endif;

			if( $types ):
				$subtext .= '<strong>Types:</strong> ';
				$i = 0;
				foreach( $types as $type ):
					$subtext .= $i++ > 0 ? ', ' : '';
					$subtext .= $type->name;
				endforeach;
				$subtext .= '<br>';
			endif;
			
		else:
			$date = get_the_date('j F Y');

			$subtext = '';
		endif;
		
	else:
		$dates = get_field('dates')[0];
		
		$start_date = date_create_from_format('d/m/Y', $dates['start_date']); 
		$end_date 	= date_create_from_format('d/m/Y', $dates['end_date']); 

		$date = $dates['start_date'];
		$date .= $dates['end_date'] && $dates['start_date'] !== $dates['end_date'] ? ' - ' : '';
		$date .= $dates['start_date'] !== $dates['end_date'] ? $dates['end_date'] : '';

		$subtext = get_field('location') ? '<strong>Location: </strong>' . get_field('location') : '';
	endif;

	$data_attr = '';

	// If pinned, set featured text
	$data_attr = isset($pinned) && $pinned ? 'data-label="Featured"' : ''; 

	// If it's an event fetch any appropriate labels
	if( get_post_type() == 'all-events' ):
		$dates = get_field('dates');

		$today 				= date('Ymd');

		$this_week_start 	= date('Ymd', strtotime('+1 day'));
		$this_week_end 	= date('Ymd', strtotime("next sunday", strtotime("last sunday") )); // get "this sunday" by using "last sunday"
		
		$next_week_start 	= date('Ymd', strtotime('next monday'));
		$next_week_end 	= date('Ymd', strtotime("next sunday", strtotime('next monday') ));

		$next_month_start = date('Ymd', strtotime("first day of +1 month"));
		$next_month_end 	= date('Ymd', strtotime("last day of +1 month"));

		if( have_rows('dates') ):
			while( have_rows('dates') ): the_row();
				$start_date = get_sub_field('start_date', false);
				$end_date  	= get_sub_field('end_date', false);

				// Is the event heppening right now?
				if( $today == $start_date || ($today >= $start_date && $today <= $end_date) ):

					$data_attr = 'data-label="Today"';

					break;
				elseif( ($start_date >= $this_week_start && $start_date <= $this_week_end) || ($start_date >= $this_week_start && $start_date <= $this_week_end) ):	

					$data_attr = 'data-label="This Week"';

					break;
				elseif( ($start_date >= $next_week_start && $start_date <= $next_week_end) || ($start_date >= $next_week_start && $start_date <= $next_week_end) ):	

					$data_attr = 'data-label="Next Week"';

					break;
				elseif( ($start_date >= $next_month_start && $start_date <= $next_month_end) || ($start_date >= $next_month_start && $start_date <= $next_month_end) ):	

					$data_attr = 'data-label="Next Month"';

					break;
				endif;
			endwhile;
		endif;
	endif;

	// Work with countdowns
	$countdown_date = get_field('countdown_date', false, false);

	if( $countdown_date ):
		$countdown_datetime = new DateTime(get_field('countdown_date', false, false));

		// Make the grid item medium as it's cramped otherwise (if it's not already large)
		if( $countdown_date && $countdown_datetime->format('U') > date('U') ):
			$grid_size = $grid_size !== 'large' ? 'medium' : $grid_size;
		endif;
	endif;

	/* ==== Determine whether or not we are going to display the date ==== */

		$hide_archive_date = get_field('hide_archive_date');
		
	/* === END === */

	/* ==== Get the categories from the post, also get icons and other settings from the term ==== */

			if( get_post_type() == 'all-work' ):
				$categories = get_the_terms(get_the_ID(), 'all-work-types');
			elseif( get_post_type() == 'all-news' ):
				$categories = get_the_terms(get_the_ID(), 'all-news-types');
			endif;

			// Output categories and any associated icons
			if( isset($categories) && $categories ):
				$count = 0;

				foreach( $categories as $category ):

					$icon 			= get_field('icon', $category);
					$icon_colour 	= ( $grid_size == 'large' || $grid_size == 'medium' ) ? 'white' : 'red';

					// Check if we want to hide the date for items tagged with this category
					$hide_archive_date = get_field('hide_dates_on_archives', $category);

					if( $icon && $grid_size !== 'small') :
						$post_categories .= '<svg class="post_categories__icon ' . $icon_colour . ' ' . $icon . '">';
							$post_categories .= '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#' . $icon . '"></use>';
						$post_categories .= '</svg>';
					endif;

					$post_categories .= $count++ > 0 ? ', ' : '';
					$post_categories .= $category->name;
				endforeach;
			endif;

			// If the post doesn't have a thumbnail, check if any of it's categories have a default image set so we can use that instead
			if( get_post_type() == 'all-work' && !get_field('thumbnail') && isset($categories) ):
				foreach( $categories as $category ):
					$default_image = get_field('default_thumbnail', $category);
					
					// Once we reach a category that has an image, stop looping
					if( !empty($image) ):
						break;
					endif;
				endforeach;
			endif;
		
		/* === END === */

	// Set style attribute for background image
	if( get_post_type() == 'all-vacancies' ):

		// Get image from author business
		$business = get_field('business');
	
		if( $business ):
			$business_id = $business->ID;

			$logo = get_field('social_logo', $business_id) ? get_field('social_logo', $business_id) : get_field('logo', $business_id);

			if( $logo ):
				$style = 'style="background-image: url(' . $logo['sizes']['logo-full'] . '); background-size: contain;"';
			endif;
		endif;

		if( !isset($style) ):
			$style = '';
		endif;
		
	elseif( get_field('thumbnail') ):
		$style = 'style="background-image: url(' . get_field('thumbnail')['sizes'][$image_size] . ');"';
	elseif( $default_image ):
	 	$style = 'style="background-image: url(' . $default_image['sizes'][$image_size] . ');"';
	else:
		$style = ' ';
	endif;
?>

<div class="c-feature-grid__item <? echo get_post_type(); ?> <? echo $grid_size ? $grid_size : 'small'; ?> <? echo $data_attr ? 'flag' : ''; ?> <? echo $is_new ? 'flag new' : ''; ?>" <? echo $data_attr; ?> data-aos="fade-up" data-delay="100">
	<div class="c-feature-grid__item-wrapper">
		<? if( !empty($style) && get_post_type() !== 'all-jfl-vacancies' ): ?>
			<a href="<? the_permalink(); ?>" class="c-feature-grid__image" <? echo $style; ?> ></a>
		<? endif; ?>

		<div class="c-feature-grid__content">
			<a class="<? echo $grid_size == 'large' ? 'h3' : 'h5'; ?> c-feature-grid__title u-margin-bottom-5" href="<? the_permalink(); ?>">
				<? echo $countdown_date && $countdown_datetime->format('U') > date('U') ? '<span class="u-colour-red">Live:</span> ' : ''; ?><? the_title() ?> 
			</a>

			<? echo $subtext ? '<p class="u-font-size-small u-margin-bottom-15">' . $subtext . '</p>' : '' ; ?>

			<? echo $post_categories ? '<p class="post_categories">' . $post_categories . '</p>' : '' ; ?>

			<?
				// Output text if the post is a member event
				$published_by 	= get_field('published_by'); 
				$author			= get_the_terms(get_the_ID(), 'all-authors') ? array_column( get_the_terms(get_the_ID(), 'all-authors'), 'slug' ) : '';

				if( get_post_type() == 'all-events' && ( !empty($published_by) || ( is_array($author) && in_array('jersey-industry', $author) ) ) ): ?>
					<span class="c-feature-grid__date">
						A Jersey Industry Event
					</span>
				<? endif;
			?>

			<? if( !$hide_archive_date ): ?>
				<span class="c-feature-grid__date">
					<? echo $date; ?>
				</span>
			<? endif; ?>

			<?
				if( $countdown_date ):

					// Include JS countdown timer if field exists and date is in future
					if( $countdown_datetime->format('U') > date('U') ):
						include( locate_template('includes/partials/work/countdown.php') ); 
					endif;
				endif;
			?>
		</div>

	</div>
</div>