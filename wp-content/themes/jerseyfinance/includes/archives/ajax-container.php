<?
	$state = isset($template_args['grid_state']) ? $template_args['grid_state'] : 'grid'; 
?>

<? if( !request_is_AJAX() ): ?>

	<div class="container">
		<div id="ajax-container">

<? endif; ?>

			<? 
				include( locate_template('/includes/archives/topic-items.php') ); 
			?>

			<? // If the request is AJAX make sure no items appear large
				$i = request_is_AJAX() && is_paged() ? 10 : 0;
			?>
			
			<? 
				/* === Don't output grid of items if someone has ticked our fake spotlgiht on category === */
			if( !isset($show_all) ): ?>

<? if( !request_is_AJAX() ): ?>					
			<div class="c-feature-grid c-feature-grid__<? echo $state; ?>-view item-container">
<? endif; ?>

				<? if( isset($paged) && ( !$paged || $paged <= 1 ) ): 
					
					// Output a single JFL news item
					if( isset($jf_news_posts) && $jf_news_posts->have_posts() ):
						// Output "Featured" flag
						$pinned = true;
						
						while( $jf_news_posts->have_posts() ): $jf_news_posts->the_post();
							setup_postdata($post);
							
							include( locate_template('/includes/archives/post-item.php') );

							$i++;
						endwhile;
						wp_reset_postdata();

						$pinned = false;
					endif;

					// Output all pinned posts
					if( isset($pinned_posts) && $pinned_posts->have_posts() ):
						// Output "Featured" flag
						$pinned = true;
						
						while( $pinned_posts->have_posts() ): $pinned_posts->the_post();
							setup_postdata($post);
							
							include( locate_template('/includes/archives/post-item.php') );

							$i++;
						endwhile;
						wp_reset_postdata();

						$pinned = false;
					endif;
				endif; ?>

				<? // If the request is AJAX make sure no items appear large
					if( have_posts() ):
						while( have_posts() ): the_post();
							
							include( locate_template('/includes/archives/post-item.php') );

							$i++;
						endwhile;
					else:
						return_no_posts_message();
					endif;
				?>

				<? if( request_is_AJAX() ): ?>
					<? include( locate_template('/includes/archives/pagination.php') ); ?>
				<? endif; ?>

<? if( !request_is_AJAX() ): ?>

			</div>

			<div class="pagination-container">
				<? if( !request_is_AJAX() ): ?>
					<? include( locate_template('/includes/archives/pagination.php') ); ?>
				<? endif; ?>
			</div>

		<? endif; ?>
		</div>
	</div>

<? endif; ?>