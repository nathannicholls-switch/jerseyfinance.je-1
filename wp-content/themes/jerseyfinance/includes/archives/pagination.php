<?
	$archive_query = isset($template_args['archive_query']) ? $template_args['archive_query'] : ''; 

   // If a custom query is not provied, use the defauly wp_query
   // If the pagination needs to hook into a custom query instead of a regular wp_query then the user can set $archive_query which will be used by paginate_links to calc posts_per_page etc.
   if( empty($archive_query) ):
      $archive_query = $wp_query;
   endif;
	
	if( $archive_query->max_num_pages > 1 && $paged < $archive_query->max_num_pages  ): ?>

		<div class="pagination">
			<a class="c-button isotope" href="<? echo get_next_posts_page_link(); ?>">
				Load More
			</a>
		</div>

	<? endif; 
?>