<? 
	// Grab terms from the URL, check if they have a header image and output them if they do
	$searched_topics 		= filter_input( INPUT_GET, 'topics', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY ); 
	
	// If the spotlight on checkbox is ticked, fetch ALL hub terms and output a grid item for them
	$show_all 	= filter_input( INPUT_GET, 'spotlight-on', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY ); 
	
	// (Work has it's own spotlight on checkbox which we need to check against)
	$work_types = filter_input( INPUT_GET, 'work-types', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY ); 

	// The ID of the spotlight term
	$work_spotlight_id = !empty(get_term_by( 'name', 'Spotlight On', 'all-work-types' )) ? get_term_by( 'name', 'Spotlight On', 'all-work-types' )->term_id : '';
	
	// If they user has selected Spotlught on, get all terms, otherwise just use the ones from the URL
	if( $show_all[0] || ( !empty($work_types) && in_array($work_spotlight_id, $work_types) ) ):
		$all_topics = get_terms( array(
			'taxonomy' => 'all-topics',
			'hide_empty' => true,
		));
	else:
		$all_topics = $searched_topics;
	endif;
	
	// Loop through the topics and add them to an array if they have header images - we will output these in the grid
	if( $all_topics ):
		$topics = [];

		foreach( $all_topics as $topic ):
			// Fetch full term
			$term = get_term($topic);
			
			// If term has a header image, add to array to be output in grid
			if( get_field('header_image', $term) ):
				$topics[] = $topic;
			endif;
		endforeach;
	endif;
?>

<? if( !empty($topics) && !is_paged() ): ?>
	
	<div class="c-feature-grid c-feature-grid__grid-view topics">

		<? foreach( $topics as $topic ):

			$term = get_term( $topic );

			if( get_field('header_image', $term) ):
				$subtext = get_field('header_content', $term)['text']; ?>

				<div class="c-feature-grid__item large topic">
					<a href="<? echo get_term_link($term); ?>" class="c-feature-grid__item-wrapper">
						<div class="c-feature-grid__image" style="background-image: url(<? echo get_field('header_image', $term)['sizes']['archive-grid-large']; ?>);"></div>

						<div class="c-feature-grid__content">

							<span class="c-feature-grid__date">
								Spotlight On
							</span>

							<div class="c-feature-grid__title u-margin-bottom-5" href="<? echo get_term_link($term); ?>">
								<? echo $term->name; ?>
							</div>

							<? echo $subtext ? '<p class="u-font-size-small">' . shorten_text($subtext, 250, false) . '</p>' : '' ; ?>

							<!-- <p class="post_categories">Spotlight On</p> -->
						</div>

					</a>
				</div>

			<? endif;
		endforeach; ?>

	</div>

<? endif; ?>