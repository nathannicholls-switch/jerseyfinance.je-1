<?
	// Do we need to include date range for publish date (boolean)
	$publish_date 			= isset($template_args['publish_date']) ? $template_args['publish_date'] : false;

	// Do we need to include date range for event date (boolean)
	$event_date 			= isset($template_args['event_date']) ? $template_args['event_date'] : false;

	// The taxononimes we need to output checkboxes for (array)
	$taxonomies 			= isset($template_args['taxonomies']) ? $template_args['taxonomies'] : '';

	// Do we want to output the post type filters in the top filter bar
	$post_type 				= isset($template_args['post_type']) ? $template_args['post_type'] : false;

	// Which post types do we want to output the items from (e.g related sectors)?
	$related_post_types 	= isset($template_args['related_post_types']) ? $template_args['related_post_types'] : '';

	// Do we want to show grid/list and sort options?
	$modifiers 				= isset($template_args['modifiers']) ? $template_args['modifiers'] : false;

	// Get the url for the archive (either from the taxonomy or the post type object)
	$action 					= get_queried_object()->taxonomy ? get_term_link(get_queried_object()->term_id, get_queried_object()->taxonomy) : get_post_type_archive_link( get_queried_object()->name );
	
?>

<form class="c-filters enable-ajax" action="<? echo $action; ?>">

	<div class="c-filters__bar">
		<div class="container">

			<div class="c-filters__bar-wrapper">
				<div class="c-filters__bar-label u-font-family-bright u-colour-red">
					<div class="c-filters__bar-label-wrapper">
						<span>Filters</span>
						<div class="c-filters__toggle"></div>
					</div>
				</div>

				<div class="c-filters__bar-clear" style="display: none;">
					Clear
				</div>

				<div class="c-filters__bar-keyword">
					<svg class="search">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#search"></use>
					</svg>

					<?
						$keyword 	= filter_input( INPUT_GET, 'keyword', FILTER_SANITIZE_SPECIAL_CHARS ); 
					?>

					<input class="u-font-family-bright u-colour-red" type="text" name="keyword" placeholder="Keyword" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Keyword'" value="<? echo $keyword; ?>" />
				</div>
			</div>

		</div>
	</div>

	<? if( $publish_date || $event_date || $taxonomies || $post_type || $related_post_types ): ?>

		<div class="c-filters__filters">
			<div class="container">

				<div class="c-filters__filters-wrapper">

					<? if( $event_date ): ?>

						<?
							// Get the value from the query string - set field value if exists
							$event_start 	= filter_input( INPUT_GET, 'event-start', FILTER_SANITIZE_NUMBER_INT ); 
							$event_end 		= filter_input( INPUT_GET, 'event-end', FILTER_SANITIZE_NUMBER_INT ); 
						?>

						<div class="c-filters__filter">

							<? // Filter label ?>

							<div class="u-font-family-bright u-font-size-14 u-font-weight-bold u-colour-red u-margin-bottom-15">
								Dates
							</div>

							<div class="c-filters__filter-scroll">

								<div class="input-wrapper icon-right">

									<input name="event-start" class="flatpickr" type="text" placeholder="From" value="<? echo $event_start; ?>" >

									<svg class="calendar">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#calendar"></use>
									</svg>
								</div>

								<div class="input-wrapper icon-right">
									<input name="event-end" class="flatpickr" type="text" placeholder="Until" value="<? echo $event_end; ?>" >

									<svg class="calendar">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#calendar"></use>
									</svg>
								</div>
							
							</div>

						</div>

					<? endif; ?>


					<? if( $related_post_types ):
						foreach( $related_post_types as $related_post_type ):

							$args = array(
								'post_type' 		=> $related_post_type,
								'posts_per_page' 	=> -1
							);

							$related_post_query = new WP_Query($args);

							if( $related_post_query->have_posts() ): ?>

								<div class="c-filters__filter">

									<? // Filter label ?>

									<div class="u-font-family-bright u-font-size-14 u-font-weight-bold u-colour-red u-margin-bottom-15">
										<? echo get_post_type_object($related_post_type)->labels->singular_name; ?>
									</div>

									<div class="c-filters__filter-scroll">

										<? 
											while( $related_post_query->have_posts() ): $related_post_query->the_post();

												// Remove `all-` because the permalink would clash with the tax archive
												$name = str_replace('all-', '', $related_post_type);
											
												// Include custom checkbox with the below properties
												get_template_partial( 'includes/partials/checkbox', array(
													'name'	=> $name,
													'label' 	=> get_the_title(),
													'value'	=> get_the_ID()
												));

											endwhile;
											wp_reset_postdata();
										?>

										<?
											// Output fake "Rest of World" checkbox that will show items not tagged with a market
											if( $related_post_type == 'all-markets' ):
												// Include custom checkbox with the below properties
												get_template_partial( 'includes/partials/checkbox', array(
													'name'	=> 'rest-of-world',
													'label' 	=> 'Rest of World',
													'value'	=> '1'
												));
											endif;
										?>

									</div>

								</div>
							
							<? endif;
						endforeach;
					endif; ?>

					<? if( $taxonomies ):
						foreach( $taxonomies as $taxonomy => $order ): 

							// Only fetch terms assigned to posts in this post type
							if( is_post_type_archive() ):
								$terms = get_terms_by_post_type( array($taxonomy), array( get_queried_object()->name ), $order );
							else:
								$terms = get_terms_by_post_type( array($taxonomy), array( get_post_type() ), $order );
							endif;
						
							if( $terms ): ?>

								<div class="c-filters__filter">

									<? // Filter label ?>

									<div class="u-font-family-bright u-font-size-14 u-font-weight-bold u-colour-red u-margin-bottom-15">
										<? echo get_taxonomy($taxonomy)->labels->singular_name; ?>
									</div>
									
									<? // Filters ?>

									<div class="c-filters__filter-scroll">

										<? foreach( $terms as $term ):
											// Remove `all-` because the permalink would clash with the tax archive
											$name = str_replace('all-', '', $taxonomy);

											// Include custom checkbox with the below properties
											get_template_partial( 'includes/partials/checkbox', array(
												'name'	=> $name,
												'label' 	=> $term->name,
												'value'	=> $term->term_id
											));
										endforeach; ?>

										<?
											// Output fake "Spotlight On" checkbox that will show all Hub pages in results
											if( $taxonomy == 'all-news-types' || $taxonomy == 'all-work-types' ):
												// Include custom checkbox with the below properties
												get_template_partial( 'includes/partials/checkbox', array(
													'name'	=> 'spotlight-on',
													'label' 	=> 'Spotlight On',
													'value'	=> '1'
												));
											endif;
										?>
									
									</div>
									
								</div>
							
							<? endif;
						endforeach;
					endif; ?>

					<? if( $post_type ): ?>

						<div class="c-filters__filter">

							<? // Filter label ?>

							<div class="u-font-family-bright u-font-size-14 u-font-weight-bold u-colour-red u-margin-bottom-15">
								Type
							</div>
							
							<? // Filters ?>

								<? 
									// Fetch post types from the user, or just fetch all 3
									$post_types 	= get_user_meta(get_current_user_id(), 'custom_feed_post_types', true) ? explode(', ', get_user_meta(get_current_user_id(), 'custom_feed_post_types', true)) : array('all-news', 'all-events', 'all-work');
								?>

								<? foreach( $post_types as $post_type ):
									$label 	= get_post_type_object( $post_type )->label;
									$name 	= get_post_type_object( $post_type )->name;

									// Include custom checkbox with the below properties
									get_template_partial( 'includes/partials/checkbox', array(
										'name'	=> 'post-type',
										'label' 	=> $label,
										'value'	=> $name
									));
								endforeach; 
							?>
							
						</div>

					<? endif; ?>

					<? if( $publish_date ): ?>

						<?
							// Get the value from the query string - set field value if exists
							$publish_start 	= filter_input( INPUT_GET, 'publish-start', FILTER_SANITIZE_NUMBER_INT ); 
							$publish_end 		= filter_input( INPUT_GET, 'publish-end', FILTER_SANITIZE_NUMBER_INT ); 
						?>

						<div class="c-filters__filter">

							<? // Filter label ?>

							<div class="u-font-family-bright u-font-size-14 u-font-weight-bold u-colour-red u-margin-bottom-15">
								Dates
							</div>

							<div class="c-filters__filter-scroll">

								<div class="input-wrapper icon-right">

									<input name="publish-start" class="flatpickr" type="text" placeholder="From" value="<? echo $publish_start; ?>" >

									<svg class="calendar">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#calendar"></use>
									</svg>
								</div>

								<div class="input-wrapper icon-right">
									<input name="publish-end" class="flatpickr" type="text" placeholder="Until" value="<? echo $publish_end; ?>" >

									<svg class="calendar">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#calendar"></use>
									</svg>
								</div>

							</div>
						</div>

					<? endif; ?>

				</div>

				<button class="c-button red u-margin-top-30" type="submit">Submit</button>
			</div>
		</div>
	
	<? endif; ?>

	<? 
		if( $modifiers ):
			include( locate_template('includes/archives/filters-modifiers.php') ); 
		endif;
	?>

</form>