<div class="site-search">
	<div class="site-search__wrapper">
		
		<div class="site-search__header">
			<div class="site-search__search-wrapper">
				<div class="input-wrapper">
					<svg class="search">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#search"></use>
					</svg>

					<!-- Search Box -->
					<div id="site-search__searchbox"></div>
				</div>
			</div>

			<div class="site-search__close">
				<svg class="close site-search-toggle">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#close"></use>
				</svg>
			</div>
		</div>

		<div class="site-search__results-wrapper">

			<div class="site-search__filters">
				<!-- Post Type Filters -->
				<div class="c-title u-margin-bottom-20">
					Section
				</div>
				<div id="site-search__post-types"></div>		
			</div>

			<div class="site-search__results">
				<!-- Results -->
				<div id="site-search__hits"></div>
				<!-- Pagination -->
				<div id="site-search__pagination"></div>
			</div>
		</div>

	</div>
</div>