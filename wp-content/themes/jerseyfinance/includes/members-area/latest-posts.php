<?
	// $latest_query is set in the main single template as it's needed for other areas of the template.
	if( $latest_query ):
		if( !request_is_AJAX() ): ?>

			<div id="latest">
				<form class="c-filters enable-ajax" action="<? echo get_the_permalink(); ?>">

					<? get_template_partial( 'includes/archives/filters', array(
						'post_type' => true,
					)); ?>

				</form>

				<div class="main-content">
							
		<? endif; ?>

					<? 
						// Override $wp_query
						$temp_query = $wp_query;
						$wp_query   = NULL;
						$wp_query = $latest_query;
						
						// Includes the container and pagination used for AJAX
						include( locate_template('/includes/archives/ajax-container.php') ); 

						// Reset $wp_query
						$wp_query = NULL;
						$wp_query = $temp_query;
					?>

		<? if( !request_is_AJAX() ): ?>
						</div>
					</div>
				</div>
			</div>
		
		<? endif; 
	endif; 
?>