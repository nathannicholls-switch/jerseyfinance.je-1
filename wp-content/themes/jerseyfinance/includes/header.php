<header class="site-header">
	<div class="site-header__wrapper">

		<div class="site-header__logo">
			<a href="/">
				<svg class="logo-white">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#logo-white"></use>
				</svg>
			</a>
		</div>

		<div class="site-header__actions">
			
			<?
				// Include login/signed in link WITH account menu
				get_template_partial('includes/components/account-menu', [
					'menu'   => true
				]); 
			?>

			<?
				// Search Icon (don't load on AMP pages)
				if( !get_query_var('amp') ): ?>
					<div class="action site-search-toggle">
						<svg class="search">
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#search"></use>
						</svg>
					</div>
				<? endif; 
			?>

			<div class="action menu-toggle">
			 	<? if( get_query_var('amp') ): ?>
					<svg class="hamburger" role="button" on="tap:sidebar1.toggle" tabindex="0" >
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#hamburger"></use>
					</svg>
				<? else: ?>
					<svg class="hamburger">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#hamburger"></use>
					</svg>
				<? endif; ?>
			</div>
		</div>

	</div>	
</header>

<? 
	// Include site menu
	include( locate_template('includes/menu.php') ); 
?>

<? 
	// Include site search (don't load on AMP pages)
	if( !get_query_var('amp') ):
		include( locate_template('includes/site-search.php') ); 
	endif;
?>