<? if( !request_is_AJAX() ): ?>
	<? 
		// Get the one column header and put the users name in the title
		get_template_partial( '/includes/post-headers/one-column-header', array(
			'title' 	=> get_userdata(get_current_user_id())->first_name . "'s Dashboard"
		)); 
	?>

	<div class="main-content u-padding-top-30">
		<div class="site-wrapper">

			<? if( have_rows('notifications') ): ?>
				<div class="c-notifications">
					<div class="container">
						<? while( have_rows('notifications') ): the_row();
							/*	
								Set the cookie name to be the notification text converted to MD5 - means each one will have a unique name,
								and if the notification is updated the cookie name will no longer match
							*/
							$cookieName = md5( get_sub_field('notification') ); ?>

							<div class="notification notice" data-cookie="<? echo $cookieName; ?>" data-expiry="30">
								<svg class="alert">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#alert"></use>
								</svg>
								
								<? echo get_sub_field('notification'); ?>

								<div class="c-cookie-notice__close">
									<svg class="close">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#close"></use>
									</svg>
								</div>
							</div>
						<? endwhile; ?>
					</div>
				</div>
			<? endif; ?>

			<? include( locate_template('/includes/flexible-content.php') ); ?>

<? endif; ?>

			<?
				/* === The query for the "Latest" tab - hooks user preferences into "latest_query" === */
				$meta_query = [];
				$tax_query = [];

				// Use OR so that our tax_query works across multiple taxonimies and post types
				$tax_query['relation'] = 'OR';

				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

				// Check if any post types have been set in the filters, if not, grab from user table
				$post_types 	= filter_input( INPUT_GET, 'post-type', FILTER_SANITIZE_SPECIAL_CHARS, FILTER_REQUIRE_ARRAY ); 

				if( empty( $post_types ) ):
					$post_types 	= get_user_meta(get_current_user_id(), 'custom_feed_post_types', true) ? explode(', ', get_user_meta(get_current_user_id(), 'custom_feed_post_types', true)) : array('all-news', 'all-events', 'all-work');
				endif;		

				// Get the Gravity Forms data from the user and convert to an array (gets saved as a string)
				$topics 			= get_user_meta(get_current_user_id(), 'custom_feed_topics', true) ? explode(', ', get_user_meta(get_current_user_id(), 'custom_feed_topics', true)) : '';
				$news_types 	= get_user_meta(get_current_user_id(), 'custom_feed_news_types', true) ? explode(', ', get_user_meta(get_current_user_id(), 'custom_feed_news_types', true)) : '';
				$work_types 	= get_user_meta(get_current_user_id(), 'custom_feed_work_types', true) ? explode(', ', get_user_meta(get_current_user_id(), 'custom_feed_work_types', true)) : '';

				// We will push in post IDs to this which will be output onto the template
				$result_ids = [];

				// Generate a query for each post type which we will loop through and grab ID's from
				foreach( $post_types as $post_type ):
					/* === The query for the "Latest" tab - hooks user preferences into "latest_query" === */
					$meta_query = [];
					$tax_query = [];

					$keyword = filter_input( INPUT_GET, 'keyword', FILTER_SANITIZE_SPECIAL_CHARS );

					// Topics tax_query
					if( !empty( $topics ) ):
						foreach($topics as $topic):
							$tax_query[] = array(
								'taxonomy' 	=> 'all-topics',
								'field'		=> 'term_id',
								'terms'		=> $topic,
								'operator' => 'IN',
							);	
						endforeach;
					endif;

					if( $post_type == 'all-news' ):
					
						// News type tax_query
						if( !empty( $news_types ) ):
							foreach($news_types as $news_type):
								$tax_query[] = array(
									'taxonomy' 	=> 'all-news-types',
									'field'		=> 'term_id',
									'terms'		=> $news_type,
									'operator' => 'IN',
								);	
							endforeach;
						endif;	

						$args = array(
							'post_type' 		=> $post_type,
							'posts_per_page' 	=> 80,
							's'					=> $keyword,
							'tax_query'			=> $tax_query,
						);
						
						$news_query = new WP_Query( $args );	

						// Grab the ids from the results and push into our results array
						$query_ids 	= wp_list_pluck($news_query->posts, 'ID');
						$result_ids = array_merge( $query_ids, $result_ids );		

					elseif( $post_type == 'all-work' ):

						// Work type tax_query
						if( !empty( $work_types ) ):
							foreach($work_types as $work_type):
								$tax_query[] = array(
									'taxonomy' 	=> 'all-work-types',
									'field'		=> 'term_id',
									'terms'		=> $work_type,
									'operator' => 'IN',
								);	
							endforeach;
						endif;

						$args = array(
							'post_type' 		=> $post_type,
							'posts_per_page' 	=> 80,
							's'					=> $keyword,
							'tax_query'			=> $tax_query,
						);
						$work_query = new WP_Query( $args );

						// Grab the ids from the results and push into our results array
						$query_ids 	= wp_list_pluck($work_query->posts, 'ID');
						$result_ids = array_merge( $query_ids, $result_ids );	

					elseif( $post_type == 'all-events' ):

						$meta_query = [];

						$today = date('Ymd');

						// Only show future stuff
						$meta_query[] = array(
							'key'			=> 'hide_date',
							'value'		=> $today,
							'compare'	=> '>'	
						);

						$args = array(
							'post_type' 		=> $post_type,
							'posts_per_page' 	=> 40,
							's'					=> $keyword,
							'tax_query'			=> $tax_query,
							'meta_query'			=> $meta_query,
						);
						$events_query = new WP_Query( $args );

						// Grab the ids from the results and push into our results array
						$query_ids 	= wp_list_pluck($events_query->posts, 'ID');
						$result_ids = array_merge( $query_ids, $result_ids );	

					endif;	

				endforeach;

				// Take the array of ids from the above queries and push them into our results query to be displayed
				$args = array(
					'post_type' 		=> $post_types,
					'posts_per_page'	=> 18,
					'post__in'			=> $result_ids,
					'paged'				=> $paged
				);

				$latest_query = new WP_Query($args);
			?>

			<?
				// Include the grid of related news, events and work
				include( locate_template('/includes/members-area/latest-posts.php') );
			?>

<? if( !request_is_AJAX() ): ?>

		</div>
	</div>
<? endif; ?>