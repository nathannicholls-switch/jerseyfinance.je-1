<? 
	$header_template = locate_template( '/includes/post-headers/' . get_field('header_style') . '-header.php' );

	if( $header_template ):
		include( $header_template );
	endif; 
?>

<div class="main-content">
	<div class="site-wrapper">

		<?
			$meta_query = [];
			
			$meta_query[] = array(
				'key' 		=> 'users',
				'value'		=> '"' . get_current_user_id() . '"',
				'compare'	=> 'LIKE'
			);

			$args = array(
				'posts_per_page'	=> -1,
				'post_type'			=> 'all-working-groups',
				'meta_query'		=> $meta_query
			);

			$working_group_query = new WP_Query( $args );

			if( $working_group_query->have_posts() ): ?>

				<div class="container">
					<ul class="button-list">
				
						<? while( $working_group_query->have_posts() ): $working_group_query->the_post(); ?>
							<li>
								<a href="<? the_permalink(); ?>">
									<? the_title(); ?>
								</a>
							</li>						
						<? endwhile; 
						wp_reset_postdata();
						?>

					</ul>
				</div>
			
			<? else: ?>

				<div class="container">
					<h4 class="u-align-centre u-margin-bottom-70">
						You are not currently part of any working groups.
					</h4>
				</div>

			<? endif;
		?>

		<? include( locate_template('/includes/flexible-content.php') ); ?>

	</div>
</div>