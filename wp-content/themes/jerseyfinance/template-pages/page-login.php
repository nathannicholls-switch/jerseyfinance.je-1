<? 
	$header_template = locate_template( '/includes/post-headers/' . get_field('header_style') . '-header.php' );

	if( $header_template ):
		include( $header_template );
	endif; 
?>

<div class="main-content u-padding-top-20">
	<div class="site-wrapper">

		<? include( locate_template('/includes/flexible-content.php') ); ?>
		
		<div class="container">
			<div class="custom-login gform_wrapper">

				<?php wp_login_form(); ?>

				<p class="login-links">
					<a href="/membership/join">
						Register
					</a>
					|
					<a href="/wp-login.php?action=lostpassword">
						Lost your password?
					</a>
				</p>
			
			</div>
		</div>

	</div>
</div>
