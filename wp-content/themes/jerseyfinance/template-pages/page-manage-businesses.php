<? 
	$header_template = locate_template( '/includes/post-headers/' . get_field('header_style') . '-header.php' );

	if( $header_template ):
		include( $header_template );
	endif; 
?>

<div class="main-content">
	<div class="site-wrapper">

		<div class="container">

			<?
				$has_businesses = false;			

				// Businesses that the user has opted into
				$current_user_id = get_current_user_id();

				// Get the businesses that this user is an owner of
				$businesses = get_joined_businesses( $current_user_id, true );

				if( $businesses ) : $has_businesses = true; ?>
					<div class="c-title u-margin-bottom-0">
						Businesses you manage
					</div>
					
					<ul class="button-list u-margin-top-0 c-business-list">

						<? foreach( $businesses as $business_name => $business_id ):

							// Get the ID of owners from the business
							$owners = get_field( 'owner', $business_id ) ? array_column(get_field( 'owner', $business_id ), 'ID') : array(); // an array containing the owners

							// Check whether or not the user exists in the user fields of the business
							if( isset($owners) && in_array( $current_user_id, $owners ) ):
								$user_is_owner = true;
							else:
								$user_is_owner = false;
							endif; 
							
							$image = get_field('social_logo', $business_id) ? get_field('social_logo', $business_id) : get_field('logo', $business_id);
							?>

							<li class="u-table">
								<div class="u-table-cell logo">
									<? if( $image ): ?>
										<a href="<? the_permalink(); ?>">
											<img class="square-logo" src="<? echo $image['sizes']['logo-full']; ?>" alt="<? the_title(); ?>" />
										</a>
									<? endif; ?>
								</div>

								<div class="u-table-cell">
									<a class="h5 u-margin-bottom-0" href="<? echo get_the_permalink( $business_id ); ?>">
										<? echo $business_name; ?>
									</a>
								</div>

								<div class="u-table-cell">
									<? if( $user_is_owner ): ?>
										<a href="/dashboard/manage-businesses/update-business?post-id=<? echo $business_id; ?>" class="c-button red">
											Edit Business
										</a>
									<? endif; ?>
								</div>
							</li>
							
						<? endforeach; ?>

					</ul>
				<? endif; 
			?>


			<?
				// Businesses that the user can still opt in to
				$current_user_id = get_current_user_id();

				// Get busineses that the user is not already a part of
				$businesses = get_matching_businesses( get_userdata( $current_user_id )->user_email, true );

				if( $businesses && get_field('country', 'user_' . get_current_user_id()) == 'Jersey'  ) : $has_businesses = true;  ?>

					<div class="c-title u-margin-bottom-0">
						Businesses you can join
					</div>

					<ul class="button-list u-margin-top-0 c-business-list">

						<? foreach( $businesses as $business_name => $business_id ):							
							$image = get_field('social_logo', $business_id) ? get_field('social_logo', $business_id) : get_field('logo', $business_id);
							?>

							<li class="u-table">
								<div class="u-table-cell logo">
									<? if( $image ): ?>
										<a href="<? the_permalink(); ?>">
											<img class="square-logo" src="<? echo $image['sizes']['logo-full']; ?>" alt="<? the_title(); ?>" />
										</a>
									<? endif; ?>
								</div>

								<div class="u-table-cell">
									<a class="h5 u-margin-bottom-0" href="<? echo get_the_permalink( $business_id ); ?>">
										<? echo $business_name; ?>
									</a>
								</div>

								<div class="u-table-cell">
									<a href="#" data-id="<? echo $business_id; ?>" class="c-button red js-opt-in">
										Accept invite
									</a>
								</div>
							</li>
							
						<? endforeach; ?>

					</ul>
				<? endif; 
			?>

			<? echo $has_businesses ? '' : return_no_posts_message() ; ?>
		</div>

		<? include( locate_template('/includes/flexible-content.php') ); ?>

	</div>
</div>


<?
	function opt_in_to_business_js() { ?>
		<script>
			$('.js-opt-in').click(function() {
				var business_id = $(this).data('id');

				// The data we want to push to our function
				var data = {
					action: 'opt_in',				// Will run the action `wp_ajax_opt_in`
					business_id: business_id
				};

				// Make an ajax request with our data - will put the current user into the users field of the business
				jQuery.post('/wp-admin/admin-ajax.php', data, function(response) {
					// Reload the page once done
					window.location.reload();
				});

				return false;
			});
	
		</script>
	<? }
	add_action('wp_footer', 'opt_in_to_business_js', 20);
?>
