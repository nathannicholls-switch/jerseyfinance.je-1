<? 
	$header_template = locate_template( '/includes/post-headers/' . get_field('header_style') . '-header.php' );

	if( $header_template ):
		include( $header_template );
	endif; 
?>

<div class="main-content">
	<div class="site-wrapper">

		<? include( locate_template('/includes/flexible-content.php') ); ?>

		<div class="container">
			<?
				// Needed for the Gravity Forms user activation page
				the_content(); 
			?>
		</div>

		<? 
			if( comments_open() ):
				comments_template( '', true ); 
			endif; 
		?>

	</div>
</div>