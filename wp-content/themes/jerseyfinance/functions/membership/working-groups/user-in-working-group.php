<?
	/*
		This function is uesd on the working group templates, it will determine whether or not the user is allowed to view the content.
	*/

	function user_in_working_group() {
		// Don't allow access if the user is not logged in.
		if( !get_current_user_id() ):
			return false;
		endif;

		// Allow admins to see everything
		if( current_user_can('administrator') || current_user_can('jfl_administrator') ) {
			return true;
		}

		$allowed_users = array_column(get_field('users'), 'user_email');

		if( in_array( get_userdata( get_current_user_id() )->user_email, $allowed_users ) ):
			return true;
		endif;
	}
?>