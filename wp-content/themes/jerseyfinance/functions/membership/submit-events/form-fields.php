<?
	/*
		An array of all our form field identifiers, entry array keys and acf field keys - used for our hooks/functions.
		
			input_id:
				The form input ID, made up of `form ID`_`field ID`_`column index`.

			entry_key: 
				The array key of the posted data.

			acf_key:
				The acf field key.
	*/

	$event_fields = array(
		'title' => array(
			'input_id' 		=> '1',
			'entry_key'		=> '1',
			'acf_key'		=> 'field_5bd0f181a7308',
		),
		// Import image field into two fields
		'header_image' => array(
			'input_id' 		=> '2',
			'entry_key'		=> '2',
			'acf_key'		=> 'field_5bcef88edd309', // (two column image field)
			'type'			=> 'image'
		),
		'thumbnail' => array(
			'input_id' 		=> '2',
			'entry_key'		=> '2',
			'acf_key'		=> 'field_5be04c8c3dacd',
			'type'			=> 'image'
		),
		'content' => array(
			'input_id' 		=> '3',
			'entry_key'		=> '3',
			'acf_key'		=> 'field_5beac8e5e496a',
			'type'			=> 'content' // map to main flexbile content
		),
		'video' => array(
			'input_id' 		=> '21',
			'entry_key'		=> '21',
			'acf_key'		=> 'field_5beac8e5e496a',
		),
		'dates' => array(
			'input_id' 		=> '10',
			'entry_key'		=> '10',
			'acf_key'		=> 'field_5beac32d1206e',
			'type'			=> 'list', // map to main flexbile content

			// Column names need to match Gravity Forms labels, and be in the same order as ACF
			'columns'		=> array(
				'Start Date' => array(
					'input_id' 		=> '10_1',
					'entry_key'		=> '10.1',
					'acf_key'		=> 'field_5beac3641206f'
				),
				'End Date' => array(
					'input_id' 		=> '10_2',
					'entry_key'		=> '10.2',
					'acf_key'		=> 'field_5beac47b12070'
				),
				'Start Time' => array(
					'input_id' 		=> '10_3',
					'entry_key'		=> '10,3',
					'acf_key'		=> 'field_5beac49012071'
				),
				'End Time' => array(
					'input_id' 		=> '10_4',
					'entry_key'		=> '10.4',
					'acf_key'		=> 'field_5beac4be12072'
				)
			)
		),
		// Locations taxonomy
		'country' => array(
			'input_id' 		=> '15',
			'entry_key'		=> '15',
		),
		'topics' => array(
			'input_id' 		=> '7',
			'entry_key'		=> '7',
		),
		'business' => array(
			'input_id' 		=> '9',
			'entry_key'		=> '9',
			'acf_key'		=> 'field_5bfbbb5c52d63',
		)
	);
?>