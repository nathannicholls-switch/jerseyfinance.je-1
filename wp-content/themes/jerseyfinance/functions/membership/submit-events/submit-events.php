<?
	// Manipulate the submitted Gravity Forms data so that it fits in ACF
	function after_event_creation( $entry, $form ) {
		global $event_fields;

		// echo '<pre>';
		// print_r($entry);
		// echo '</pre>';

		// Add the content to our flexible content.
		add_flexible_content( $entry['post_id'], $entry[$event_fields['content']['entry_key']], $entry[$event_fields['video']['entry_key']], $event_fields['content']['acf_key'] );	

		// If image is present set it as the two col header image
		set_header_image( $entry, $event_fields['header_image'] );

		// Set the business that published the post
		updateGravityFormsBasicField( $entry['post_id'], 'input_' . $event_fields['business']['input_id'], $event_fields['business']['acf_key'] );

		// Locations (Topics are done automatically)
		wp_set_post_terms( $entry['post_id'], $entry[$event_fields['country']['entry_key']], 'all-event-locations', false);

		// Save dates to our repeater
		save_event_dates( $entry['post_id'], $entry[$event_fields['dates']['entry_key']] );
	}
	add_action( 'gform_after_submission_13', 'after_event_creation', 10, 2 );		

	/* 
		Before the form has rendered - gives oportunity to manupulate the form prior to displaying it:

			- Set our checkbox choices
	*/
	function pre_event_render( $form ) {
		global $event_fields;

		// Get the form fields
		$fields = $form['fields'];

		// Loop through the form fields
		foreach( $form['fields'] as &$field ) {

			/* === Set checkbox choices === */
		
				// If "Related sectors" field
				if ( $field->id == $event_fields['business']['entry_key'] ) {
					// Populate business dropdown with CEO connect businesses only
					populate_business_dropdown( $field, true );
				}

				if ( $field->id == $event_fields['topics']['entry_key'] ) {
					sort_choices($field);
				}

			/* === END === */
		}

		return $form;
	}
	add_filter( 'gform_pre_render_13', 'pre_event_render' );
	add_filter( 'gform_pre_validation_13', 'pre_event_render' );
	add_filter( 'gform_pre_submission_filter_13', 'pre_event_render' );
	add_filter( 'gform_admin_pre_render_13', 'pre_event_render' );

	/* 
		When the page is loaded:

			- Change first fields in dates field to datepickers, modifiers for date/times
	*/

	function submit_events_loaded() {
		global $event_fields;

		/* === Convert dates list field into datepickers === */

			// Start Date
			$filter_name = 'gform_column_input_content_13_' . $event_fields['dates']['columns']['Start Date']['input_id'];
			add_filter( $filter_name, 'make_list_datepicker', 10, 6 );
			
			// End Date
			$filter_name = 'gform_column_input_content_13_' . $event_fields['dates']['columns']['End Date']['input_id'];
			add_filter( $filter_name, 'make_list_datepicker', 10, 6 );

			// Start Time
			$filter_name = 'gform_column_input_content_13_' . $event_fields['dates']['columns']['Start Time']['input_id'];
			add_filter( $filter_name, 'make_list_timepicker', 10, 6 );
			
			// End Time
			$filter_name = 'gform_column_input_content_13_' . $event_fields['dates']['columns']['End Time']['input_id'];
			add_filter( $filter_name, 'make_list_timepicker', 10, 6 );

	}

	add_action( 'init', 'submit_events_loaded', 10, 2 );


?>