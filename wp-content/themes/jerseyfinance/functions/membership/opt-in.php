<?
	function opt_in_to_business( $business_id ) {
		$business_id = intval( $_POST['business_id'] );

		// Get the current list of users ID's that are allowed to edit
		$allowed_users = array_column(get_field('users', $business_id), 'ID');

		// Add the new user to the array, ACF expects an array of strings
		$allowed_users[] = (string)get_current_user_id();

		// Push the new list of user IDs into the "Users" field of the business listing.
		update_field('users', $allowed_users, $business_id);

		die(); // this is required to return a proper result
	}

	add_action('wp_ajax_opt_in', 'opt_in_to_business');
?>