<?
	function after_job_creation( $entry, $form ) {
		global $vacancy_fields;

		// Add the content to our flexible content.
		add_flexible_content( $entry['post_id'], $entry[$vacancy_fields['content']['entry_key']], false, false );	

		// Set the business that published the post
		updateGravityFormsBasicField( $entry['post_id'], 'input_' . $vacancy_fields['business']['input_id'], $vacancy_fields['business']['acf_key'] );

		// Types
		set_post_terms( $entry, 'all-vacancy-types', $vacancy_fields['type']['entry_key'] );

		// Terms
		set_post_terms( $entry, 'all-vacancy-terms', $vacancy_fields['term']['entry_key'] );

		// Set expiry for the post based on closing date field
		$expiry = $entry[$vacancy_fields['closing_date']['entry_key']];

		if( !empty( $expiry ) ):
			set_post_expiry( $entry['post_id'], $expiry );
		endif;
	}
	add_action( 'gform_after_submission_14', 'after_job_creation', 10, 2 );		

	/* 
		Before the form has rendered - gives oportunity to manupulate the form prior to displaying it:

			- Set our checkbox choices
	*/
	function pre_job_render( $form ) {
		global $vacancy_fields;

		// Get the form fields
		$fields = $form['fields'];

		// Loop through the form fields
		foreach( $form['fields'] as &$field ) {

			/* === Set choices === */

				// Pull types from taxonomy
				if ( $field->id == $vacancy_fields['type']['entry_key'] ) {

					$choices = [];

					$terms = get_terms(array(
						'taxonomy'		=> 'all-vacancy-types',
						'hide_empty'	=> false
					));

					if( $terms ):
						foreach($terms as $term ):
							$choices[] = array(
								'text' => $term->name,
								'value' => $term->term_id
							);
						endforeach;
					endif;

					// Set the field choices
					$field->choices = $choices;
				}

				// Pull terms from taxonomy
				if ( $field->id == $vacancy_fields['term']['entry_key'] ) {

					$choices = [];

					$terms = get_terms(array(
						'taxonomy'		=> 'all-vacancy-terms',
						'hide_empty'	=> false
					));

					if( $terms ):
						foreach($terms as $term ):
							$choices[] = array(
								'text' => $term->name,
								'value' => $term->term_id
							);
						endforeach;
					endif;

					// Set the field choices
					$field->choices = $choices;
				}
		
				// Business Select
				if ( $field->id == $vacancy_fields['business']['entry_key'] ) {
					// Populate business dropdown with CEO connect businesses only
					populate_business_dropdown( $field, true );
				}

			/* === END === */
		}

		return $form;
	}
	add_filter( 'gform_pre_render_14', 'pre_job_render' );
	add_filter( 'gform_pre_validation_14', 'pre_job_render' );
	add_filter( 'gform_pre_submission_filter_14', 'pre_job_render' );
	add_filter( 'gform_admin_pre_render_14', 'pre_job_render' );




	function submit_vacancies_loaded() {
		global $vacancy_fields;

			// Closing Date
			$filter_name = 'gform_field_input_14_' . $vacancy_fields['closing_date']['input_id'];
			add_filter( $filter_name, 'make_datepicker', 10, 6 );
	}

	add_action( 'init', 'submit_vacancies_loaded', 10, 2 );
?>