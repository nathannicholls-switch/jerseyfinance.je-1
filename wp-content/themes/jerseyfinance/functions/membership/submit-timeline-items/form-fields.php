<?
	/*
		An array of all our form field identifiers, entry array keys and acf field keys - used for our hooks/functions.
		
			input_id:
				The form input ID, made up of `form ID`_`field ID`_`column index`.

			entry_key: 
				The array key of the posted data.

			acf_key:
				The acf field key.
	*/

	$timeline_2018_fields = array(
		'title' => array(
			'input_id' 		=> '1',
			'entry_key'		=> '1',
		),
		'description' => array(
			'input_id' 		=> '2',
			'entry_key'		=> '2',
			'acf_key'		=> 'field_5c1382670ed6d',
		),
		'date' => array(
			'input_id' 		=> '3',
			'entry_key'		=> '3',
			'acf_key'		=> 'field_5c1381eb0ed6a',
		),
		'link' => array(
			'input_id' 		=> '5',
			'entry_key'		=> '5',
			'acf_key'		=> 'field_5c13827c0ed6e',
		),
		'image' => array(
			'input_id' 		=> '4',
			'entry_key'		=> '4',
			'acf_key'		=> 'field_5c1382990ed6f',
		),
	);
?>