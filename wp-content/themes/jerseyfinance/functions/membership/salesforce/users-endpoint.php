<?
	// When a user updates their profile, update the last_modified meta field so we can output it inot the below feed for Salesforce
	function my_profile_update( $user_id, $old_user_data ) {
		$time_now = date('U');

		update_user_meta($user_id, 'last_modified', $time_now);
	}
	add_action( 'profile_update', 'my_profile_update', 10, 2 );

	function fetch_updated_users( $query ) {
		if( array_key_exists('json/updated-users', $query->query_vars) ) {

			/* ==== Do the authentication ==== */

				$valid_passwords = array (
					"jfl_admin" => "W]*dGtQB3YmR9*y"
				);

				$valid_users = array_keys($valid_passwords);

				$user = $_SERVER['PHP_AUTH_USER'];
				$pass = $_SERVER['PHP_AUTH_PW'];

				$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

				if (!$validated) {
					header('WWW-Authenticate: Basic realm="My Realm"');
					header('HTTP/1.0 401 Unauthorized');
					die ('Access Denied');
				}
			
			/* === END === */

			/* ==== Fetch and output the data ==== */

				$updated_users = get_users();

				$json_users = [];

				foreach( $updated_users as $updated_user ):
					$user_id		= $updated_user->ID;
					$user_info 	= get_userdata($updated_user->ID);

					$json_users[] = array(
						'last_modified'		=> get_user_meta($user_id, 'last_modified', true),
						'email'					=> $updated_user->user_email,
						'first_name'			=> $user_info->first_name,
						'last_name'				=> $user_info->last_name,
						'city'					=> get_user_meta($user_id, 'city', true),
						'country'				=> get_user_meta($user_id, 'country', true),
						'job_title'				=> get_user_meta($user_id, 'job_title', true),
						'phone'					=> get_user_meta($user_id, 'phone', true),
						'market_interests'	=> get_user_meta($user_id, 'market_interests'),
						'pillars'				=> get_user_meta($user_id, 'pillars'),
						'newsletters'			=> get_user_meta($user_id, 'newsletters'),
						'hear_about'			=> get_user_meta($user_id, 'hear_about'),
					);
				endforeach;

				// Send to JSON object
				wp_send_json(
					$json_users
				);

			/* === END === */


		}
	}
	add_action( 'pre_get_posts', 'fetch_updated_users', 20 );


?>