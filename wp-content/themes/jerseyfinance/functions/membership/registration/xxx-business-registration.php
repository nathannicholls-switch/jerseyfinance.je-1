<?
	/*
		This file fires a series of functions when a new business is created.
	*/

	/*
		Fired once a business is created - sets the post "Owner" to be the person who submitted the form.

			- Once the form is submitted, grab the ID of the business listing.
			- Check if the user was already logged in or if a new user was created.
			- Grab the user ID.
			- Push the user ID into the "Owner" field of the business listing.
	*/

		function set_business_owner( $entry ) {
			global $registration_fields;

			// Get the ID of the business listing.
			$post = get_post( $entry['post_id'] );

			// If a business was created, set the "Owner".
			if( $post ) {

				// Check if the user was already logged in or if a new user was created and grab their ID.
				if( get_current_user_id() ) {
					$user_ID = get_current_user_id();
				} else {
					$user_ID = get_user_by( 'email', $entry[$registration_fields['user']['email']['entry_key']] )->ID;
				}

				// Push the user ID into the "Owner" field of the business listing.
				add_post_meta($entry['post_id'], 'owner', $user_ID);
			}
		}

		/*
			When the registration form is submitted:
				
				- Set the owner of the new business listing
				- Run all of our functions that convert the submitted Gravity Forms data to ACF data.
		*/
		function after_registration_submission( $entry, $form ) {
			global $registration_fields;

			// Set the owner of the new business listing
			set_business_owner( $entry );

			/* === Converted the submitted checkbox info to ACF field format === */
				
				// Related Sectors
					gf_to_acf_checkboxes( $entry['post_id'], $registration_fields['business']['related_sectors'] );

				// Related Markets
					gf_to_acf_checkboxes( $entry['post_id'], $registration_fields['business']['related_markets'] );

			/* === END === */

			/* === Convert Gravity Forms list data into ACF repeater field format === */

				// Domain Names List
					gf_to_acf_list( $entry['post_id'], $registration_fields['business']['domain_names'], 'domain_names' );

				// Key Contacts
					gf_to_acf_list( $entry['post_id'], $registration_fields['business']['key_contacts'], 'key_contacts' );

			/* === END === */

			// echo '<h1>ENTRY</h1><pre>';
			// print_r($entry);
			// echo '</pre>';

			// die();

		}
		add_action( 'gform_after_submission_1', 'after_registration_submission', 10, 2 );		

		/* 
			Before the form has rendered - gives oportunity to manupulate the form prior to displaying it:

				- Set our checkbox choices
		*/
		function pre_registration_render( $form ) {
			global $registration_fields;

			// Get the form fields
			$fields = $form['fields'];

			// Loop through the form fields
			foreach( $form['fields'] as &$field ) {

				/* === Set checkbox choices === */
			
					// If "Related sectors" field
					if ( $field->id == $registration_fields['business']['related_sectors']['entry_key'] ) {
						set_choices_from_post_type( $field, 'all-sectors' );
					}

					// If "Related markets" field
					if ( $field->id == $registration_fields['business']['related_markets']['entry_key'] ) {
						set_choices_from_post_type( $field, 'all-markets' );
					}

				/* === END === */
			}

			return $form;
		}
		add_filter( 'gform_pre_render_3', 'pre_registration_render' );
		add_filter( 'gform_pre_validation_3', 'pre_registration_render' );
		add_filter( 'gform_pre_submission_filter_3', 'pre_registration_render' );
		add_filter( 'gform_admin_pre_render_3', 'pre_registration_render' );

		/* 
			When the page is loaded:

				- Change key contact email field to type="email"
		*/

		function registration_loaded() {
			global $registration_fields;

			/* === Convert key contact email field into [type="email"] === */

				$filter_name = 'gform_column_input_content_3_' . $registration_fields['business']['key_contacts']['columns']['email']['input_id'];
				add_filter( $filter_name, 'convert_to_email', 10, 6 );

			/* === END === */
		}

		add_action( 'init', 'registration_loaded', 10, 0 );

?>