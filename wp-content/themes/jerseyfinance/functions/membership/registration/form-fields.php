<?
	/*
		An array of all our form field identifiers, entry array keys and acf field keys - used for our hooks/functions.
		
			input_id:
				The form input ID, made up of `form ID`_`field ID`_`column index`.

			entry_key: 
				The array key of the posted data.

			acf_key:
				The acf field key.
	*/

	$registration_fields = array(
		// User Fields
		'user' => array(
			'first_name' => array(
				'input_id' 		=> '1_3',
				'entry_key'		=> '1.3'
			),
			'last_name' => array(
				'input_id' 		=> '1_6',
				'entry_key'		=> '1.6'
			),
			'job_title' => array(
				'input_id' 		=> '4',
				'entry_key'		=> '4',
				'acf_key'		=> 'field_5bf6da3b5cb85'
			),
			'email' => array(
				'input_id' 		=> '2',
				'entry_key'		=> '2'
			),
			'phone' => array(
				'input_id' 		=> '5',
				'entry_key'		=> '5',
				'acf_key'		=> 'field_5bf6da425cb86'
			),
			'password' => array(
				'input_id' 		=> '3',
				'entry_key'		=> '3'
			),
			'confirm_password' => array(
				'input_id' 		=> '3_2',
				'entry_key'		=> '3.2'
			),			
			'city' => array(
				'input_id' 		=> '6',
				'entry_key'		=> '6',
				'acf_key'		=> 'field_5bf6da4b5cb87'
			),
			'country' => array(
				'input_id' 		=> '7',
				'entry_key'		=> '7',
				'acf_key'		=> 'field_5bf6da515cb88'
			),
			'business_notice' => array(
				'input_id' 		=> '39',
				'entry_key'		=> '39'
			),
			'market_interests' => array(
				'input_id' 		=> '8',
				'entry_key'		=> '8'
			),
			'pillars' => array(
				'input_id' 		=> '9',
				'entry_key'		=> '9'
			),
			'newsletters' => array(
				'input_id' 		=> '10',
				'entry_key'		=> '10'
			),
			'hear_about' => array(
				'input_id' 		=> '11',
				'entry_key'		=> '11'
			),
			'country_notice' => array(
				'input_id' 		=> '40',
				'entry_key'		=> '40'
			),
		),
		// Join a business fields
		'join' => array(
			'input_id'			=> '49',
			'entry_key'			=> '49'
		),
		// Register a business Fields
		'business' => array(
			'company_name' => array(
				'input_id' 		=> '34',
				'entry_key'		=> '34'
			),
			'introduction' => array(
				'input_id' 		=> '50',
				'entry_key'		=> '50',
				'acf_key'		=> 'field_5bfeb0ad889c1'
			),
			'overview' => array(
				'input_id' 		=> '51',
				'entry_key'		=> '51',
				'acf_key'		=> 'field_5bfeb16477c6d'
			),
			'logo' => array(
				'input_id' 		=> '35',
				'entry_key'		=> '35',
				'acf_key'		=> 'field_5bf80bdc4ca77'
			),
			'social_logo' => array(
				'input_id' 		=> '36',
				'entry_key'		=> '36',
				'acf_key'		=> 'field_5bf80c074ca78'
			),
			'domain_names' => array(
				'input_id' 		=> '37',
				'entry_key'		=> '37',
				'acf_key'		=> 'field_5bf80c294ca79',

				'columns'		=> array(
					'domain_name' => array(
						'input_id' 		=> '37_1',
						'entry_key'		=> '37.1',
					)
				)
			),
			'website' => array(
				'input_id' 		=> '38',
				'entry_key'		=> '38',
				'acf_key'		=> 'field_5bf80c624ca7b'
			),
			'email' => array(
				'input_id' 		=> '39',
				'entry_key'		=> '39',
				'acf_key'		=> 'field_5bf80c8c4ca7c'
			),
			'phone' => array(
				'input_id' 		=> '40',
				'entry_key'		=> '40',
				'acf_key'		=> 'field_5bf80ca14ca7d'
			),
			'address' => array(
				'input_id' 		=> '41',
				'entry_key'		=> '41',
				'acf_key'		=> 'field_5bf80cbb4ca7e'
			),
			'key_contacts' => array(
				'input_id' 		=> '42',
				'entry_key'		=> '42',
				'acf_key'		=> 'field_5bf80cc84ca7f',

				'columns'		=> array(
					'name' => array(
						'input_id' 		=> '42_1',
						'entry_key'		=> '42.1',
					),
					'job_title' => array(
						'input_id' 		=> '42_2',
						'entry_key'		=> '42.2',
					),
					'email' => array(
						'input_id' 		=> '42_3',
						'entry_key'		=> '42,3',
					),
					'direct_line' => array(
						'input_id' 		=> '42_4',
						'entry_key'		=> '42.4',
					),
					'image' => array(
						'type'			=> 'image',
						'input_id' 		=> '42_5',
						'entry_key'		=> '42.5',
					)
				)
			),
			'twitter' => array(
				'input_id' 		=> '43',
				'entry_key'		=> '43',
				'acf_key'		=> 'field_5bf80ce74ca80'
			),
			'linkedin' => array(
				'input_id' 		=> '44',
				'entry_key'		=> '44',
				'acf_key'		=> 'field_5bf80d174ca82'
			),
			'facebook' => array(
				'input_id' 		=> '45',
				'entry_key'		=> '45',
				'acf_key'		=> 'field_5bf80d164ca81'
			),
			'related_sectors' => array(
				'input_id' 		=> '52',
				'entry_key'		=> '52',
				'acf_key'		=> 'field_5bf80d304ca83'
			),
			'related_markets' => array(
				'input_id' 		=> '53',
				'entry_key'		=> '53',
				'acf_key'		=> 'field_5bf80d4b4ca84'
			)
		),
	);