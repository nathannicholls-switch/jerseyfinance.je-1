<?
   /* ==== Setup endpoint that will allow us to look up which businesss a user can opt-in to managing, returns a json object to be used in JS ==== */

		// I create the endpoint in the full-calendar.php file. I have to create the enedpoints int eh same place for some reason.

   /* === END === */

	/* ==== A function that generates the JSON on our endpoint ==== */

		function generate_business_list( $query ) {
			if( array_key_exists('json/businesses', $query->query_vars) ) {
				$user_email = filter_input( INPUT_GET, 'email', FILTER_SANITIZE_SPECIAL_CHARS ) ? filter_input( INPUT_GET, 'email', FILTER_SANITIZE_SPECIAL_CHARS ) : ''; 

				if( $user_email ) {
					$businesses = get_matching_businesses( $user_email );
				} else {
					$businesses = array();
				}

				// Send to JSON object
				wp_send_json(
					$businesses
				);
			}
		}
		add_action( 'pre_get_posts', 'generate_business_list', 20 );

	/* === END === */

	/* ==== A function that grab an array of matching businesses and adds the current user in to the post ==== */

		function add_user_to_businesses( $user_ID ) {
			
			// Get the list of businesses that have added the users email domain to their allowed list (Endpoint that returns array)
			$matching_businesses = get_matching_businesses( get_userdata( $user_ID )->user_email );

			// Get an array of ID's of businesses the user is currently a part of (we will remove them if they don't match anymore)
			$joined_businesses = get_joined_businesses($user_ID);
			
			/*
				If any of the joined businesses are not in the matching businesses array it means the users email no longer
				matches their domain rules so we need to remove them from the users field
			*/
			if( !empty($joined_businesses) ):
				// Check which ID's aren't in the matching businesses array
				$non_matching_businesses = array_diff( $joined_businesses, $matching_businesses );

				// Loop through them and remove the user
				foreach( $non_matching_businesses as $business_name => $business_id ):
					$allowed_users = get_field('users', $business_id);
					$new_allowed_users = array();

						// Push existing users into the new array
						if( !empty($allowed_users) ):
							foreach( $allowed_users as $allowed_user ):
								$new_allowed_users[] = (string) $allowed_user['ID'];
							endforeach;
						endif;

						// If the user is in the array , remove them as their domain no longer matches
						if( in_array( $user_ID, $new_allowed_users ) ):

							// Search for the users id in the array and remove it
							if (($key = array_search($user_ID, $new_allowed_users)) !== false) {
								unset($new_allowed_users[$key]);
							}

								/* 
									Uncomment this if you want to output a list of who has been removed from what (will only show to admins) 
									(might need to throw a die() somewhere)
								
									if( current_user_can('administrator') ):
										echo get_userdata($user_ID)->user_email . ' removed from ' . get_the_title($business_id) . '.<br>';
									endif;
								*/					

						endif;

					update_field('users', $new_allowed_users, $business_id);
				endforeach;
			endif;

			// If there are matching busineses
			if( !empty($matching_businesses) ):

				// Loop through each of the businesses that match the user email and push the new user ID into the users field
				foreach( $matching_businesses as $business_name => $business_id ):
					if( $business_id ):
						// Get the current list of users ID's that are allowed to edit
						$allowed_users = get_field('users', $business_id);
						$new_allowed_users = array();

						// Push existing users into the new array
						if( !empty($allowed_users) ):
							foreach( $allowed_users as $allowed_user ):
								$new_allowed_users[] = (string) $allowed_user['ID'];
							endforeach;
						endif;

						// If the user has set their country to Jersey, add them into businesses
						if( get_user_meta($user_ID, 'country', true) == 'Jersey' ):

							// Add the new user to the array (if they aren't already in it)
							if( !in_array( $user_ID, $new_allowed_users ) ):

								/* 
									Uncomment this if you want to output a list of who has been added to what (will only show to admins) 
								
									if( current_user_can('administrator') ):
										echo get_userdata($user_ID)->user_email . ' added to ' . get_the_title($business_id) . '.<br>';
									endif;
								*/								

								$new_allowed_users[] = (string) $user_ID;
							endif;

						else:

							// If the user is in the array , remove them as their country is no longer Jersey
							if( in_array( $user_ID, $new_allowed_users ) ):

								// Search for the users id in the array and remove it
								if (($key = array_search($user_ID, $new_allowed_users)) !== false) {
									unset($new_allowed_users[$key]);
								}
							endif;

						endif;

						// echo 'User ID:' . $user_ID . '<br>';
						// echo get_user_meta($user_ID, 'country', true) . '<br>';
						// print_r($matching_businesses);
						// echo '<br>';
						// print_r($allowed_users);

						// Push the new list of user IDs into the "Users" field of the business listing.
						update_field('users', $new_allowed_users, $business_id);

					endif;
				endforeach;
			endif;
		}

		// Will fire once a user activates their account - will then grab their ID and add them into matching businesses
		function after_user_activation( $user_id ) {
			global $registration_fields;

			if( isset($user_id) ):
				// Take the ID of the activated account and add them into businesses
				add_user_to_businesses( $user_id );
			endif;

		}
		add_action( 'gform_user_registered', 'after_user_activation', 20 ); 	// run when a user is created through Gravity Forms
		add_action( 'user_register', 'after_user_activation', 20 );				// run when a user is created through Wordpress
		add_action( 'profile_update', 'after_user_activation', 20 );			// run when a user is updated (needed for WP All Import + to re-check users country when they update thier profiles)


	/* === END === */

	/*
		Function that includes some JS that outputs a list of businessses that the user will be added into on the registration form.
	*/
	function display_eligible_businesses() {
		// Add the below scripts to the footer, if we are in form #9
		function display_businesses() {
			global $registration_fields; ?>
			<script>
				function list_businesses( userEmail, userCountry ) {

					var emailField 		= $("#input_10_<? echo $registration_fields['user']['email']['input_id']; ?>");
					var countryField 		= $("#input_10_<? echo $registration_fields['user']['country']['input_id']; ?>");

					// The field we output the country notice into
					var country_notice_container = $("#field_10_<? echo $registration_fields['user']['country_notice']['input_id']; ?>");
					var country_notice = '';

					// The field we output the country notice into
					var business_notice_container = $("#field_10_<? echo $registration_fields['user']['business_notice']['input_id']; ?>");
					var business_notice = '';

					/* ==== Country notice ==== */

						if( userCountry == 'Jersey' ) {
							// Empty and hide country notice
							country_notice_container.html( '' );
							country_notice_container.hide();
						} else {
							country_notice += '<div class="gfield_description custom-notice">';
								country_notice += 'As you are not based in Jersey you cannot sign up to Jersey Finance membership, you can proceed as a guest and enjoy our website.';
							country_notice += '</div>';

							// Output and show country explanation
							country_notice_container.html( country_notice );
							country_notice_container.show();
						}
					
					/* === END === */

					/* ==== Business Notice ==== */

						// Only run JS if they have an email and the correct country
						if( userEmail && userCountry == 'Jersey' ) {

							// Generate the URL to our JSON
							var ajaxUrl = '/json/businesses?email=' + userEmail;

							ajaxBusinesses = $.ajax({
								'url': 	ajaxUrl,
								dataType: 'json',
								cache: 	true
							})
							.done(function(data) {
								
								// Hide the last two sets of checkboxes if the user has no business matches
								var newsletters 	= $("#field_10_<? echo $registration_fields['user']['newsletters']['input_id']; ?>");
								var hear_about 	= $("#field_10_<? echo $registration_fields['user']['hear_about']['input_id']; ?>");

								/* === Load business list into field ==== */

									// If there are matching businesses in our JSON
									if( Object.keys(data).length ) {
										var i = 1;

										business_notice += '<div class="gfield_description custom-notice">';
											business_notice += '<p>A company you are joining is a Jersey Finance member so you can enjoy all of the benefits of Jersey Finance membership.</p>';

											// Generate business list
											business_list = '<ul class="checklist">';

												$.map(data, function(val, key) {
													
														business_list += '<li class="u-font-size-small">' + key + '</li>';

													i++;
												});
											
											business_list += '</ul>';

											business_notice += business_list;

										business_notice += '</div>';

										// Show list of businesses
										business_notice_container.show();
										business_notice_container.html( business_notice );

										newsletters.show();
										hear_about.show();
									} else {
										// No businesses found, so hide list
										business_notice_container.hide();
										business_notice_container.html('');

										newsletters.hide();
										hear_about.hide();
										
										// Untick everything
										newsletters.find(':checkbox').prop('checked', false).prop('selected', false);
										// Untick everything
										hear_about.find(':checkbox').prop('checked', false).prop('selected', false);

									}

								/* === END === */
							});
						} else {
							// Hide list of businesses as they don't have the right country
							business_notice_container.hide();
						}
					
					/* === END === */
				}

				// On load and when the business checkboxes are ticked, push them into a hidden text field
				// We do this because Gravity Forms cant send submitted data properly when the choices are set with JS
				$(document).on('gform_post_render', function(){
					var emailField 		= $('#input_10_<? echo $registration_fields['user']['email']['input_id']; ?>');
					var countryField 		= $('#input_10_<? echo $registration_fields['user']['country']['input_id']; ?>');

					var userEmail 			= emailField.val();
					var userCountry 		= countryField.val();

					// Hide the last two sets of checkboxes by default
					var newsletters 	= $("#field_10_<? echo $registration_fields['user']['newsletters']['input_id']; ?>");
					var hear_about 	= $("#field_10_<? echo $registration_fields['user']['hear_about']['input_id']; ?>");

					newsletters.hide();
					hear_about.hide();

					// On load (in case values are prefilled?)
					list_businesses( userEmail, userCountry );

					// When the email is changed trigger a change event on the country field so our JS runs (with timeout so that browser autofill doesn't mix things up)
					emailField.change(function() {

						setTimeout( function() {
							countryField.trigger('change');
						}, 150);
					});

					// When the country field is changed, fetch a JSON feed to businesses that match the users email address
					countryField.change(function() {
						var userEmail 			= emailField.val();
						var userCountry 		= countryField.val();

						list_businesses( userEmail, userCountry );
					});
				});
				

			</script>

		<? } add_action('wp_footer', 'display_businesses', 20);
	}
	add_action( 'gform_enqueue_scripts_10', 'display_eligible_businesses', 10, 2 );



	// Disable autocomplete as it messes with the JS
	function gform_form_tag_autocomplete( $form_tag, $form ) {
		if ( is_admin() ) return $form_tag;
		if ( GFFormsModel::is_html5_enabled() ) {
			$form_tag = str_replace( '>', ' autocomplete="off">', $form_tag );
		}
		return $form_tag;
	}
	add_filter( 'gform_form_tag_10', 'gform_form_tag_autocomplete', 11, 2 );

	function gform_form_input_autocomplete( $input, $field, $value, $lead_id, $form_id ) {
		if ( is_admin() ) return $input;
		if ( GFFormsModel::is_html5_enabled() ) {
			$input = preg_replace( '/<(input|textarea)/', '<${1} autocomplete="off" ', $input ); 
		}
		return $input;
	}
	add_filter( 'gform_field_content', 'gform_form_input_autocomplete', 11, 5 ); 


	/* 
		This is a one-time-use function that re-saves all user fields on businesses in the correct format.
		ACF incorrectly saved users via update_field so they were saved as integers instead of strings.
		Meaning meta_query stopped working on all permissions checks.
	*/
	function fix_acf_user_fields() {
		$args = array(
			'posts_per_page'	=> -1,
			'post_type' 		=> 'all-businesses'
		);

		$businesses = new WP_Query( $args );

		if( $businesses->have_posts() ):
			while( $businesses->have_posts() ): $businesses->the_post();

				$business_id = get_the_ID();

				/* === Owner === */

					$users = get_field('owner');

					if( $users ):
						$users = array_column( $users, 'ID' );
						
						$new_users = array();

						foreach( $users as $user ):
							$new_users[] = (string) $user;
						endforeach;

						update_field('owner', $new_users, $business_id);
					endif;

				/* === END === */

				/* === Users === */

					$users = get_field('users');

					if( $users ):
						$users = array_column( $users, 'ID' );
						
						$new_users = array();

						foreach( $users as $user ):
							$new_users[] = (string) $user;
						endforeach;

						update_field('users', $new_users, $business_id);
					endif;
					
				/* === END === */

				/* === Additional Users === */

					$users = get_field('additional_users');

					if( $users ):
						$users = array_column( $users, 'ID' );
						
						$new_users = array();

						foreach( $users as $user ):
							$new_users[] = (string) $user;
						endforeach;

						update_field('additional_users', $new_users, $business_id);
					endif;
					
				/* === END === */

			endwhile; wp_reset_postdata();
		endif;
	}
	
	// Run the fix function if the current user is an administrator
	if( current_user_can('administrator') ):
		// fix_acf_user_fields();
	endif;









	/* 
		This is a one-time-use function that re-tags all users against businesses.
		I used this to ensure all users who matched a domain had been tagged.
	*/
	function re_tag_users_to_businesses() {
		// Limit the results so we don't timeout
		$args = array(
			'number' => 1,
			'offset'	=> 0			// Increment me by the "number amount"
		);

		$all_users = get_users( $args );

		foreach( $all_users as $user ):
			add_user_to_businesses( $user->ID );
		endforeach;
	}
	
	// Run the fix function if the current user is an administrator
	if( current_user_can('administrator') & !is_admin() ):
		// re_tag_users_to_businesses();
	endif;
?>