<?
	function user_can_view( $level ) {

		// Allow admins & JFL staff to see everything
		if( current_user_can('administrator') || current_user_can('jfl_administrator') || current_user_can('jfl_subscriber') || current_user_can('editor') ) {
			return true;
		}

		// If a permissions level has been set, determine whether or not the current user can view the post
		if( $level ):
			if( $level == 'guest' ):
				
				$result = is_user_logged_in() ? true : false;

			elseif( $level == 'full' || $level == 'ceo' ):

				/*
					PR users should only be allowed to submit content, they shouldn't be able to VIEW content, 
					so pass in an allowed array of ID's for submission pages, and refuse access to anything else
				*/
				if( current_user_can('pr_subscriber') ): 
					$allowed_pages = array(
						8663, // Manage Posts
						6796, // Manage Businesses
						6797, // Update Business
						7351, // Submit News timeline
						5107, // Submit News
						5109, // Submit Events
						5110, // Submit Jobs
					);

					// if the current post id matches any of the allowed ID's
					if( in_array( get_the_ID(), $allowed_pages ) ):
						return true; // allow them access
					else:
						return false; // stop them
					endif;
				endif;

				$current_user_id = get_current_user_id();

				$meta_query = [];

				$meta_query[] = array(
					'relation'	=> 'OR',
					array(
						'key' 		=> 'owner',
						'value'		=> '"' . $current_user_id . '"',
						'compare'	=> 'LIKE'
					),
					array(
						'key' 		=> 'users',
						'value'		=> '"' . $current_user_id . '"',
						'compare'	=> 'LIKE'
					),
					array(
						'key' 		=> 'additional_users',
						'value'		=> '"' . $current_user_id . '"',
						'compare'	=> 'LIKE'
					)
				);

				if( $level == 'ceo' ):
					$meta_query[] = array(
						'key' 		=> 'ceo_connect_status',
						'value'		=> 'yes',
						'compare'	=> '='
					);
				endif;

				$args = array(
					'post_type' 		=> 'all-businesses',
					'posts_per_page'	=> -1,
					'meta_query'		=> $meta_query
				);

				$eligible_businesses = new WP_Query( $args );

				if( $eligible_businesses->have_posts() ):
					$result = true;
				else:
					$result = false;
				endif;
			 
			endif;
		else:
			$result = true;
		endif;

		return $result;
	}
?>