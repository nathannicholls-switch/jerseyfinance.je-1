<?
	function after_news_creation( $entry, $form ) {
		global $news_fields;

		// echo '<pre>';
		// print_r($entry);
		// echo '</pre>';

		// Add the content to our flexible content.
		add_flexible_content( $entry['post_id'], $entry[$news_fields['content']['entry_key']], $entry[$news_fields['video']['entry_key']], false );	

		// If image is present set it as the two col header image
		set_header_image( $entry, $news_fields['header_image'] );

		// Set the business that published the post
		updateGravityFormsBasicField( $entry['post_id'], 'input_' . $news_fields['business']['input_id'], $news_fields['business']['acf_key'] );

		// Types (Topics are done automatically)
		set_post_terms( $entry, 'all-news-types', $news_fields['type']['entry_key'] );
	}
	add_action( 'gform_after_submission_11', 'after_news_creation', 10, 2 );		

	/* 
		Before the form has rendered - gives oportunity to manupulate the form prior to displaying it:

			- Set our checkbox choices
	*/
	function pre_news_render( $form ) {
		global $news_fields;

		// Get the form fields
		$fields = $form['fields'];

		// Loop through the form fields
		foreach( $form['fields'] as &$field ) {

			/* === Set checkbox choices === */
		
				// If "Related sectors" field
				if ( $field->id == $news_fields['business']['entry_key'] ) {
					populate_business_dropdown( $field );
				}


			/* === END === */
		}

		return $form;
	}
	add_filter( 'gform_pre_render_11', 'pre_news_render' );
	add_filter( 'gform_pre_validation_11', 'pre_news_render' );
	add_filter( 'gform_pre_submission_filter_11', 'pre_news_render' );
	add_filter( 'gform_admin_pre_render_11', 'pre_news_render' );
?>