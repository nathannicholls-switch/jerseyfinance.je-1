<?
	/*
		An array of all our form field identifiers, entry array keys and acf field keys - used for our hooks/functions.
		
			input_id:
				The form input ID, made up of `form ID`_`field ID`_`column index`.

			entry_key: 
				The array key of the posted data.

			acf_key:
				The acf field key.
	*/

	$news_fields = array(
		'title' => array(
			'input_id' 		=> '1',
			'entry_key'		=> '1',
			'acf_key'		=> 'field_5bd0f181a7308',
		),
		// Import image field into two fields
		'header_image' => array(
			'input_id' 		=> '2',
			'entry_key'		=> '2',
			'acf_key'		=> 'field_5bcef88edd309', // (two column image field)
			'type'			=> 'image'
		),
		'thumbnail' => array(
			'input_id' 		=> '2',
			'entry_key'		=> '2',
			'acf_key'		=> 'field_5be04c8c3dacd',
			'type'			=> 'image'
		),
		'content' => array(
			'input_id' 		=> '3',
			'entry_key'		=> '3',
			'acf_key'		=> 'field_5bf6da3b5cb85',
			'type'			=> 'content' // map to main flexbile content
		),
		'video' => array(
			'input_id' 		=> '10',
			'entry_key'		=> '10',
		),
		'type' => array(
			'input_id' 		=> '6',
			'entry_key'		=> '6',
		),
		'topics' => array(
			'input_id' 		=> '7',
			'entry_key'		=> '7',
		),
		'business' => array(
			'input_id' 		=> '9',
			'entry_key'		=> '9',
			'acf_key'		=> 'field_5bfbbb5c52d63',
		)
	);
?>