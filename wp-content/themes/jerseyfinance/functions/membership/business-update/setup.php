<?
		$edit_post_id = filter_input( INPUT_GET, 'post-id', FILTER_VALIDATE_INT );

		/* 
			When the page is loaded:

				- Change key contact email field to type="email"
		*/

		function business_update_loaded() {
			global $business_update_fields;

			/* === Convert key contact email field into [type="email"] === */

				$filter_name = 'gform_column_input_content_12_' . $business_update_fields['key_contacts']['columns']['Email']['input_id'];
				add_filter( $filter_name, 'convert_to_email', 10, 6 );

			/* === END === */
		}

		add_action( 'init', 'business_update_loaded', 10, 0 );

	 
		// function that adds the JS for logo updating
		function get_current_logos() { 
			global $edit_post_id;
		
			$main_logo		= get_field('logo', $edit_post_id)['sizes']['logo-full'];
			$social_logo 	= get_field('social_logo', $edit_post_id)['sizes']['logo-square'];

			$html = '<div class="c-grid current-logos">';
				$html .= '<div class="c-grid__col-6 full-logo">';
					$html .= 'Full Logo <span class="remove-logo u-colour-red">(remove)</span>';
					$html .= '<img style="max-width: 100px;" src="' . $main_logo . '" />';
				$html .= '</div>';
				
				$html .= '<div class="c-grid__col-6 square-logo">';
					$html .= 'Square Logo <span class="remove-logo u-colour-red">(remove)</span>';
					$html .= '<img style="max-width: 100px;" src="' . $social_logo . '" />';
				$html .= '</div>';							
			$html .= '</div>'; ?>

			<script>
				$(".site-wrapper").on("click", ".remove-logo", function(){
					// Remove current image from page
					$(this).hide();
					$(this).siblings('img').remove();
				});

				$(".site-wrapper").on("click", ".full-logo .remove-logo", function(){
					// Change value to 1 so that the image will be removed from the post
					$('#input_12_20').val('1');
				});

				$(".site-wrapper").on("click", ".square-logo .remove-logo", function(){
					// Change value to 1 so that the image will be removed from the post
					$('#input_12_21').val('1');
				});								

				var html = '<? echo $html; ?>';
				$('.js-current-logos').html( html );
			</script>

		<? }


		/* 
			Before the form has rendered - gives oportunity to manupulate the form prior to displaying it:

				- Set our checkbox choices
		*/
		function pre_update_business_render( $form ) {
			global $edit_post_id;
			global $business_update_fields;

			// Get the form fields
			$fields = $form['fields'];

			// Loop through the form fields
			foreach( $form['fields'] as &$field ) {

				/* === Set checkbox choices === */
			
					// If "Related sectors" field
					if ( $field->id == $business_update_fields['related_sectors']['entry_key'] ) {
						set_choices_from_post_type( $field, 'all-sectors' );
					}

					// If "Related markets" field
					if ( $field->id == $business_update_fields['related_markets']['entry_key'] ) {
						set_choices_from_post_type( $field, 'all-markets' );
					}

					// Output current logos
					if ( $field->id == $business_update_fields['current_logos']['entry_key'] ) {
						add_action('wp_footer', 'get_current_logos', 20);
					}

				/* === END === */
			}

			return $form;
		}
		add_filter( 'gform_pre_render_12', 'pre_update_business_render' );
		add_filter( 'gform_pre_validation_12', 'pre_update_business_render' );
		add_filter( 'gform_pre_submission_filter_12', 'pre_update_business_render' );
		add_filter( 'gform_admin_pre_render_12', 'pre_update_business_render' );		
?>