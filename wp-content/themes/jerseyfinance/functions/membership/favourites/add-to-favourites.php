<?
	function add_to_favourites() {
		$post_id = intval( $_POST['post_id'] );

		// Only do this if the user id and post id are set
		if( is_user_logged_in() && !empty($post_id) ):
			
			// Fetch array of favourites
			$favourites = fetch_favourites();
			
			// If the post already exists in the array, remove it, otherwise push new post into the array
			if( ($key = array_search($post_id, $favourites)) !== false ):
				// Unset value
				unset($favourites[$key]);
			else:
				// Add to array
				$favourites[] = $post_id; 
			endif;

			// Update the users favourites
			update_user_meta( get_current_user_id(), 'favourites', $favourites );
		endif;

		die(); // this is required to return a proper result
	}

	add_action('wp_ajax_add_to_favourites', 'add_to_favourites');
?>