<?
	/*
		An array of all our form field identifiers, entry array keys and acf field keys - used for our hooks/functions.
		
			input_id:
				The form input ID, made up of `form ID`_`field ID`_`column index`.

			entry_key: 
				The array key of the posted data.

			acf_key:
				The acf field key.
	*/

	$custom_feed_fields = array(
		'post_types' => array(
			'input_id' 		=> '1',
			'entry_key'		=> '1',
		),
		'topics' => array(
			'input_id' 		=> '2',
			'entry_key'		=> '2',
		),
		'news_types' => array(
			'input_id' 		=> '5',
			'entry_key'		=> '5',
		),
		'work_types' => array(
			'input_id' 		=> '10',
			'entry_key'		=> '10',
		),
	);
?>