<?
	/* 
		Before the form has rendered - gives oportunity to manupulate the form prior to displaying it:

			- Set our checkbox choices
	*/
	function pre_custom_feed_render( $form ) {
		global $custom_feed_fields;

		// Get the form fields
		$fields = $form['fields'];

		// Loop through the form fields
		foreach( $form['fields'] as &$field ) {

			/* === Set checkbox choices === */
		
				// Pull topics from taxonomy
				if ( $field->id == $custom_feed_fields['topics']['entry_key'] ) {

					$choices = [];

					$terms = get_terms(array(
						'taxonomy'		=> 'all-topics',
						'hide_empty'	=> false
					));

					if( $terms ):
						foreach($terms as $term ):
							$choices[] = array(
								'text' => $term->name,
								'value' => $term->term_id
							);
						endforeach;
					endif;

					// Set the field choices
					$field->choices = $choices;
				}

				// Pull news types from taxonomy
				if ( $field->id == $custom_feed_fields['news_types']['entry_key'] ) {

					$choices = [];

					$terms = get_terms(array(
						'taxonomy'		=> 'all-news-types',
						'hide_empty'	=> false
					));

					if( $terms ):
						foreach($terms as $term ):
							$choices[] = array(
								'text' => $term->name,
								'value' => $term->term_id
							);
						endforeach;
					endif;

					// Set the field choices
					$field->choices = $choices;
				}

				// Pull work types from taxonomy
				if ( $field->id == $custom_feed_fields['work_types']['entry_key'] ) {

					$choices = [];

					$terms = get_terms(array(
						'taxonomy'		=> 'all-work-types',
						'hide_empty'	=> false
					));

					if( $terms ):
						foreach($terms as $term ):
							$choices[] = array(
								'text' => $term->name,
								'value' => $term->term_id
							);
						endforeach;
					endif;

					// Set the field choices
					$field->choices = $choices;
				}

			/* === END === */
		}

		return $form;
	}
	add_filter( 'gform_pre_render_8', 'pre_custom_feed_render' );
	add_filter( 'gform_pre_validation_8', 'pre_custom_feed_render' );
	add_filter( 'gform_pre_submission_filter_8', 'pre_custom_feed_render' );
	add_filter( 'gform_admin_pre_render_8', 'pre_custom_feed_render' );
?>