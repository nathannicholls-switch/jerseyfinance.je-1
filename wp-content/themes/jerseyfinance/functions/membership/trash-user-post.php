<?
	function trash_user_post( $post_id ) {
		$post_id = intval( $_POST['post_id'] );

		if( $post_id ):

			// Change post status to binned (so it can be recovered if deleted by accident)
			wp_trash_post( $post_id );
		endif;

		die(); // this is required to return a proper result
	}

	add_action('wp_ajax_trash_user_post', 'trash_user_post');
?>