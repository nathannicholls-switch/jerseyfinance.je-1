<?
	function fetch_all_users( $business_id ) {
		$owner 				= get_field('owner', $business_id) ? array_column( get_field('owner', $business_id), 'ID' ) : array('');
		$users 				= get_field('users', $business_id) ? array_column( get_field('users', $business_id), 'ID' ) : array('');
		$additional_users = get_field('additional_users', $business_id) ? array_column( get_field('additional_users', $business_id), 'ID' ) : array('');

		$allowed_users = array_merge( $owner, $users, $additional_users );

		return $allowed_users;
	}
?>