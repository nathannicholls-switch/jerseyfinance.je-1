<?
	/*
		Function that takes the users email, checks if they have an acccount, and generates a password reset url:
		https://www.jerseyfinance.je/login?action=rp&key=passwordresetkey&login=dan@switch.je
	*/

	function generate_reset_url( $user_email ) {
		if( $user_email ):
		
			// Get the site url?
			$url = is_multisite() ? get_blogaddress_by_id( (int) $blog_id ) : home_url('', 'http');
			
			$user = get_user_by( 'email', $user_email );

			// $user = new WP_User( (int) $user_id );

			if( $user ):
				$adt_rp_key = get_password_reset_key( $user );
				
				$user_login = $user->user_login;

				$rp_link = network_site_url("login?action=rp&key=$adt_rp_key&login=" . rawurlencode($user_login), 'login');
				
				// Return the password reset link
				return $rp_link;
			else:
				// They don't have an account, so do nothing
				return false;
			endif;
		else:
			return false;
		endif;
	}

	/*
		Generate the content for the notification sent by the password reset form.
		We have to do this as I couldn't figure out how to edit the default notification Wordpress sends.
		So instead we are doing a custom form + notification for the initial "Request a reset" form.
	*/
	function my_gform_notification_signature( $notification, $form, $entry ) {
		// Allow our custom HTML
		$notification['message_format'] = 'html';

		// Password reset text
		$message = '<p>We\'ve reset your password. Please click on the link below to change the automatically-generated password to something more memorable.</p>';
		$message .= '<p><a href="' . generate_reset_url( rgar( $entry, '1' ) ) . '">Choose your new password</a></p>';
		$message .= '<p>If you have any questions, please <a href="websupport@jerseyfinance.je">contact us</a>.</p>';
		
		$notification['message'] = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>Jersey Finance</title><!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head><body style="-webkit-font-smoothing:antialiased;width:100% !important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;margin:0;"><table border="0" cellpadding="0" cellspacing="0" width="100%"> <tr><td class="background-cell" bgcolor="#F5F5F5" align="center" style="padding:0px"><table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="width:600px" class="responsive-table"><tr><td bgcolor="#ffffff" class="fw-cell" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt"><table class="fw-table" width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" bgcolor="#CB181C" style="padding:15px 15px 5px 15px;text-align:right;font-size:9px;color:#ffffff"></td></tr><tr><td align="center" bgcolor="#CB181C" style="padding:0px 30px 40px 30px;" valign="top"><div class="h-pad-xs"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td class="col grid-image-cell-stacked" width="200" valign="top" style="padding-right:10px"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td><a href="https://www.jerseyfinance.je/" target="_blank"><img class="masthead-logo" src="https://s3.eu-west-2.amazonaws.com/jfl-fonts/jfl_logo_masthead_ls.png" width="200" height="31" alt="Jersey Finance" border="0" style="display:block; margin:0 auto;"></a></td></tr></table></td><td class="col" valign="middle" width="240" valign="top" style="padding-left:60px;padding-right:20px;padding-top:10px"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td><a href="https://www.jerseyfinance.je/" target="_blank"><img class="responsive-image" src="https://s3.eu-west-2.amazonaws.com/jfl-fonts/jfl_strapline_masthead.png" width="246" height="12" alt="Delivering Insight" border="0" style="display:block; margin:0 auto;max-width:246px !important;"></a></td></tr></table></td></tr></table></div></td></tr></table></td></tr></table></td></tr><tr><td align="center" class="content-container" bgcolor="#ffffff" style="padding:15px 0px 15px 0px"><div><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td class="responsive-cell"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td class="content-cell-p-h-1" style="text-align:center"><h1>Reset your password to access your Jersey Finance website account</h1></td></tr></table></td></tr></table></div><div><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td class="responsive-cell"><table width="100%" cellpadding="20" cellspacing="0" border="0"><tr><td class="content-cell"><div class="content-cell-p-h-1">';
		$notification['message'] .= $message;
		$notification['message'] .= '</div></td></tr></table></td></tr></table></div><br/></td></tr><tr><td align="center" bgcolor="#F0EFEE" class="content-cell" style="padding:40px 30px 5px 30px"><img src="https://s3.eu-west-2.amazonaws.com/jfl-fonts/jfl_footer_logo.png" alt="Jersey Finance" width="66" height="46"></td></tr><tr><td align="center" bgcolor="#F0EFEE" class="content-cell" style="padding:15px 15px 15px 15px;"><div style="display:block;max-width:315px"><p style="color:#4A4A4A;font-size:16px;line-height:26px">Promoting Jersey as the clear leader in international finance</p></div></td></tr><tr><td align="center" bgcolor="#F0EFEE" class="content-cell" style="padding:15px 0px 40px 0px"><table cellpadding="0" cellspacing="0" border="0"><tr><td align="center" valign="middle" class="socialIcon"><a href="http://twitter.com/jerseyfinance/" target="_blank"><img src="https://s3.eu-west-2.amazonaws.com/jfl-fonts/social_icon_twitter.png" width="20" height="20" alt="twitter" border="0"></a></td><td align="center" valign="middle" class="socialIcon"><a href="http://www.linkedin.com/company/jersey-finance/" target="_blank"><img src="https://s3.eu-west-2.amazonaws.com/jfl-fonts/social_icon_linkedin.png" width="20" height="20" alt="linkedin" border="0"></a></td><td align="center" valign="middle" class="socialIcon"><a href="http://www.youtube.com/user/JerseyFinance/" target="_blank"><img src="https://s3.eu-west-2.amazonaws.com/jfl-fonts/social_icon_youtube.png" width="20" height="20" alt="youtube" border="0"></a></td><td align="center" valign="middle" class="socialIcon"><a href="http://www.facebook.com/jerseyfinanceeducation/" target="_blank"><img src="https://s3.eu-west-2.amazonaws.com/jfl-fonts/social_icon_facebook.png" width="20" height="20" alt="facebook" border="0"></a></td><td align="center" valign="middle" class="socialIcon"><a href="https://www.instagram.com/jerseyfinance/" target="_blank"><img src="https://s3.eu-west-2.amazonaws.com/jfl-fonts/social_icon_instagram.png" width="20" height="20" alt="instagram" border="0"></a></td></tr></table></td></tr><tr><td align="center" bgcolor="#F0EFEE" class="content-cell" style="padding:0px 0px 15px 0px"><a class="button" href="https://www.jerseyfinance.je/">Contact Us</a></td></tr><tr><td align="center" bgcolor="#F0EFEE" class="content-cell" style="padding:15px 30px 60px 30px"><p style="font-size:12px;color:#CB181C;"><a href="https://www.jerseyfinance.je/terms-conditions/" target="blank">Terms &amp; Conditions</a> |<a style="font-size:12px;color:#CB181C;text-decoration:none" href="https://www.jerseyfinance.je/privacy-policy/" target="_blank">Privacy Policy</a> | <a href="https://www.jerseyfinance.je/cookie-policy/" target="_blank" style="font-size:12px;color:#CB181C;text-decoration:none">Cookie Policy</a><br></p></td></tr><tr><td align="center" bgcolor="#ffffff" class="content-cell" style="padding:25px 30px 25px 30px"><p style="font-size:12px">Copyright &copy; Jersey Finance Limited<br></p></td></tr></table></td></tr></table></body></html>';

		return $notification;
	}
	add_filter( 'gform_notification_16', 'my_gform_notification_signature', 10, 3 );

	/*
		Only allow the form to submit if the account exists
	*/
	function custom_validation( $validation_result ) {
		$form = $validation_result['form'];
	
		// supposing we don't want input 1 to be a value of 86
		if ( rgpost( 'input_1' ) ) {

			$user = get_user_by( 'email', rgpost( 'input_1' ) );

			if( empty($user) ):

				// set the form validation to false
				$validation_result['is_valid'] = false;

				// Finding Field with ID of 1 and marking it as failed validation
				foreach( $form['fields'] as &$field ) {
		
					//NOTE: replace 1 with the field you would like to validate
					if ( $field->id == '1' ) {
						$field->failed_validation = true;
						$field->validation_message = 'An account with this email doesn\'t exist!';
						break;
					}
				}

			endif;

		}
	
		//Assign modified $form object back to the validation result
		$validation_result['form'] = $form;
		return $validation_result;
	}
	add_filter( 'gform_validation_16', 'custom_validation' );
?>