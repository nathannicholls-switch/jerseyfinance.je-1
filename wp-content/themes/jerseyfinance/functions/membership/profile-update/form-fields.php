<?
	/*
		An array of all our form field identifiers, entry array keys and acf field keys - used for our hooks/functions.
		
			input_id:
				The form input ID, made up of `form ID`_`field ID`_`column index`.

			entry_key: 
				The array key of the posted data.

			acf_key:
				The acf field key.
	*/

	$user_update_fields = array(
		'first_name' => array(
			'input_id' 		=> '6_3',
			'entry_key'		=> '6.3'
		),
		'last_name' => array(
			'input_id' 		=> '6_6',
			'entry_key'		=> '6.6'
		),
		'job_title' => array(
			'input_id' 		=> '7',
			'entry_key'		=> '7',
		),
		'email' => array(
			'input_id' 		=> '8',
			'entry_key'		=> '8'
		),
		'phone' => array(
			'input_id' 		=> '9',
			'entry_key'		=> '9',
		),		
		'city' => array(
			'input_id' 		=> '10',
			'entry_key'		=> '10',
		),
		'country' => array(
			'input_id' 		=> '11',
			'entry_key'		=> '11',
		),
		'market_interests' => array(
			'input_id' 		=> '12',
			'entry_key'		=> '12'
		),
		'pillars' => array(
			'input_id' 		=> '13',
			'entry_key'		=> '13'
		),
		'newsletters' => array(
			'input_id' 		=> '14',
			'entry_key'		=> '14'
		),
		'hear_about' => array(
			'input_id' 		=> '15',
			'entry_key'		=> '15'
		),
	);