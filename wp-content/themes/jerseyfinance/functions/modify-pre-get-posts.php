<?
	/*
		This file contains some functions that will modify the default WP_Query on certain archive/single pages.
		We do it this way so that we aren't just overwriting the default query with our own.

		I have split it into two main parts:
			- modify_default_queries(): 	This part specifies the queries that we want to modify (which post types)
			- generate_query():				This part will run a series of functions that modify the query/args depending on the parameters passed into it
	*/

	/* ==== Choose queries that we want to modify and pass in parameters to `generate_query()` ==== */

		function modify_default_queries( $query ) {

			if( !is_admin() ) {

				/*
					Usage: generate_query( $query, &$args, $publish_date, $event_date, $taxonomies, $related_post_types, $post_types )
				*/

				// We don't have custom args, so set to false. This has to be a variable as we declare the parameter as a reference (&$args)
				$args = false;
				
				// News
				if ( !is_admin() && $query->is_post_type_archive('all-news') && $query->is_main_query() ) {
					$query->set('posts_per_page', 18);
					generate_query( $query, $args, true, false, array('all-news-types', 'all-topics', 'all-authors'), false, false );
				}

				// Events
				if ( !is_admin() && $query->is_post_type_archive('all-events') && $query->is_main_query() ) {
					$query->set('posts_per_page', 18);
					generate_query( $query, $args, false, true, array('all-topics', 'all-authors', 'all-event-locations'), array('all-sectors', 'all-markets'), false );

					only_fetch_future_events($query);
				}

				// Work
				if ( !is_admin() && $query->is_post_type_archive('all-work') && $query->is_main_query() ) {
					$query->set('posts_per_page', 18);
					generate_query( $query, $args, true, false, array('all-work-types', 'all-topics', 'all-authors'), false, false );
				}

				// Hubs (Spotlight On)
				if( !is_admin() && is_tax('all-topics') && $query->is_main_query() ) { 
					$query->set('posts_per_page', 18);
					generate_query( $query, $args, false, false, false, false, true );

					// Don't show any posts tagged with "Jersey Industry News" tag_ID = 70 (member news)
					if( is_archive('all-news') ):
						/* Grab default query values from default query */
						$tax_query = $query->query_vars['tax_query'];

						$tax_query[] = array(
							'taxonomy' 	=> 'all-news-types',
							'field'		=> 'term_id',
							'terms'		=> array(70),
							'operator' => 'NOT IN',
						);
						
						$query->query_vars['tax_query'] = $tax_query;
					endif;
				}

				// Adust the posts_per_page and offset for our target post types if they are trying to output 10 posts or more by default 
				// (it assumes these are archives/feeds, not related posts widgets)
					if ( !is_admin() && array_key_exists('post_type', $query->query) && !is_page('dashboard') ):

						// Used to check for any of our aggregated feeds of posts (on markets, sectors)
						$target_post_types = array('all-events', 'all-news', 'all-work');
						// Fetch the post types that are being queried, make sure they are in array format
						$query_post_types = is_array($query->query['post_type']) ? $query->query['post_type'] : array( $query->query['post_type'] );
						
						// If any of the post types in the query match our target post types (and attempt to output more than 10 posts), modify posts_per_page, offset and sorting
						if ( count(array_intersect($query_post_types, $target_post_types)) > 0 && (isset($query->query_vars['posts_per_page']) && $query->query_vars['posts_per_page'] >= 10) ):

							// Add order and orderby values to query
							generate_sort_query($query);
							
							// Need to do this to access those queries from here.
							global $pinned_posts;
							global $jf_news_posts;
							global $pinned_ids;

							// Set posts per page to 18 (taking into account that there might be pinned posts on the first page)
							$pinned_post_count 	= !empty($pinned_ids) ? sizeof($pinned_ids) : 0;
							$posts_per_page 		= 18 - $pinned_post_count;
							$query->set('posts_per_page', $posts_per_page);

							// If we are on anything other than page 1 output extra posts as we don't show two large ones
							if ( $query->is_paged && isset($query->query_vars['posts_per_page']) && request_is_AJAX() ):

								// Set a new PPP that is         
								$ppp = $query->query['posts_per_page'] ? $query->query['posts_per_page'] + 2 : 20;
								$query->set('posts_per_page', $ppp);
								$offset = -2;

								//Manually determine page query offset (offset + current page (minus one) x posts per page)
								$page_offset = $offset + ( ($query->query_vars['paged']-1) * $ppp );

								//Apply adjust page offset
								$query->set('offset', $page_offset );
							endif;

						endif;
					endif;
				// END

				// Everything after here isn't affected by offsets

				// Vacancies
				if ( !is_admin() && $query->is_post_type_archive('all-vacancies') && $query->is_main_query() ) {
					$query->set('posts_per_page', 18);
					generate_query( $query, $args, false, false, array('all-vacancy-types', 'all-vacancy-terms'), false, false );
				}

				// JFL Vacancies
				if ( !is_admin() && $query->is_post_type_archive('all-jfl-vacancies') && $query->is_main_query() ) {
					$query->set('posts_per_page', 18);
				}

				// Businesses
				if ( !is_admin() && $query->is_post_type_archive('all-businesses') && $query->is_main_query() ) {
					$query->set('posts_per_page', -1);
					generate_query( $query, $args, false, false, array('all-business-categories'), array('all-sectors', 'all-markets') );
					
					// Hide Jersey Finance
					$query->query_vars['post__not_in']	= array(6718);
				}

				// Business categories
				if( !is_admin() && is_tax('all-business-categories') && $query->is_main_query() ) { 
					$query->set('posts_per_page', -1);
					generate_query( $query, $args, false, false, false, array('all-sectors', 'all-markets') );

					// Hide Jersey Finance
					$query->query_vars['post__not_in']	= array(6718);
				}

				// Team
				if( !is_admin() && is_tax('all-team-groups') && $query->is_main_query() ) { 
					$query->set('posts_per_page', -1);
					generate_query( $query, $args, false, false, array('all-team-teams'), false );
				}

				// Timelines
				if( !is_admin() && is_tax('all-timelines') && $query->is_main_query() ) { 
					// Get the order direction from the term
					$order = get_field('order', get_queried_object()) ? get_field('order', get_queried_object()) : 'DESC';
					
					$query->query_vars['order'] 				= $order;
					$query->query_vars['orderby'] 			= 'meta_value';
					$query->query_vars['meta_key'] 			= 'date';
					$query->query_vars['posts_per_page'] 	= -1;
				}

				if( !is_admin() && $query->is_main_query() && is_post_type_archive() ) {

					/* ==== Only fetch posts that havent been marked as private (also passed in as SQL via _meta_or_title) ==== */

						$meta_query = $query ? $query->query_vars['meta_query'] : $args['meta_query'];

						$meta_query[] = array(
							'relation' => 'OR',
							array(
								'key' 		=> 'make_private',
								'value'		=> 1,
								'compare' 	=> '!='
							),
							array(
								'key' 		=> 'make_private',
								'compare' 	=> 'NOT EXISTS'
							)
						);

						$query->set('meta_query', $meta_query);
						
						// echo '<pre>';
						// print_r( $query->query_vars );
						// echo '</pre>';

					/* === END === */
				}

			}

		}
		// Run at pre_get_posts
		add_action( 'pre_get_posts', 'modify_default_queries' );

	/* === END === */

	/*
		This function takes the below parameters and fires functions that generate the new queries/push them into the main query

		$query 					- The query we want to modify
		$args 					- The args of a custom query, will be modfied instead of $query if provided (fullcalendar has it's own args)
		$publish_date 			- (bool) Are we allowing search by publish date?
		$event_date				- (bool) Are we allowing search by event date?
		$taxonomies 			- (array) A list of all taxonimies that can be queried against
		$related_post_types	- (array) A list of the custom relationship field names that we will be querying against. Used for events (markets/sectors)
	*/

		function generate_query( $query, &$args, $publish_date = false, $event_date = false, $taxonomies = false, $related_post_types = false, $post_types = false ) {

			/* ==== Sort by ==== */
				if( $publish_date ):
					generate_publish_date_query($query, $args);
				endif;
			
			/* === END === */

			/* ==== If we want to filter by publish date run the function that grab the date from the URL and pass that into the main query as a `date_query`  ==== */

				if( $publish_date ):
					generate_publish_date_query($query, $args);
				endif;

			/* === END === */

			/* ==== If we want to filter by a set of taxonomies, take the list of taxonomies and generate a tax_query containing any searched terms from the URL ==== */

				if( $taxonomies ):
					generate_tax_query($query, $taxonomies, $args);
				endif;

			/* === END TAX_QUERY === */

			/* ==== META QUERY ==== */

				if( $related_post_types ):
					generate_relationship_query($query, $related_post_types, $args);
				endif;

			/* === END META_QUERY === */

			/* ==== If we want to filter by event date run the function that grab the date from the URL and pass that into the main query as a `date_query`  ==== */

				if( $event_date ):
					generate_event_date_query($query, $args);
				endif;

			/* === END === */

			if( $post_types ):
				generate_post_types_query($query, $args);
			endif;

			/* ==== Generate the sort parameters ==== */

				generate_sort_query( $query, $args );

			/* === END === */

				generate_keyword_query( $query, $args );			

			/* ==== Generate a WP_Query that fetches any pinned posts (don't run on JSON feeds) ==== */

				if( is_archive() || is_page() || is_single() ):
					generate_pinned_posts_query($query);
				endif;

			/* === END === */

			// echo '<pre>';
			// print_r($query);
			// echo '</pre>';
		}

	/* === END === */

	/* ==== Push our keyword into the query (we aren't using ?s=keyword so it isn't automatically detected) ==== */

		function generate_keyword_query( $query, &$args ) {
			$keyword = filter_input( INPUT_GET, 'keyword', FILTER_SANITIZE_SPECIAL_CHARS );

			if( !empty($keyword) ):

				// We can't search the content of events as it conflicts with `_meta_or_title` (the OR relation doesnt work)
				if( get_queried_object()->name == 'all-events' ):
					$query->set('s', $keyword);
				else:

					/*
						Only add a meta query if we are on a post type that has flexible content.
						We have to do this check because if a post doesn't have the key in the DB it doesn't show in results even if the title has a match.
					*/
					$flex_post_types = array('all-news', 'all-work', 'all-events', 'all-team', 'all-vacancies', 'all-jfl-vacancies' );
					
					if( in_array( get_queried_object()->name, $flex_post_types) || !empty($args) ):
						// Get the current meta query so we can append a new query to it
						$updated_meta = $query ? $query->query_vars['meta_query'] : $args['meta_query'];

						// The flexible content field we want to search is different for events
						$key = get_queried_object()->name == 'all-events' || $args ? 'event_fields_0_text' : 'fields_0_text' ;

						// Search the first flexible content text field (limited for performance)
						$updated_meta[] = array(
							'relation'	=> 'OR',
							array(
								'key'			=> $key,
								'value'		=> $keyword,
								'compare'	=> 'LIKE'
							),
							// array(
							// 	'key'			=> 'fields_1_text',
							// 	'value'		=> $keyword,
							// 	'compare'	=> 'LIKE'
							// ),
							array(
								'key'			=> 'search_keywords',
								'value'		=> $keyword,
								'compare'	=> 'LIKE'
							)
						);
						
						// Replace the meta_query with our new one				
						// Add keyword search to our custom version of the 's' parameter
						if( !empty($query) ):
							$query->query_vars['meta_query'] = $updated_meta;
							$query->set('_meta_or_title', $keyword);
						elseif( !empty($args) ):
							$args['meta_query'] = $updated_meta;
							$args['_meta_or_title'] = $keyword;
						endif;

					else:
					
						// Doesnt work on terms for some reason...
						if( !is_tax() ):
							// Get existing meta query
							$custom_keyword = $query->query_vars['meta_query'];

							$custom_keyword[] = array(
								'key'			=> 'search_keywords',
								'value'		=> $keyword,
								'compare'	=> 'LIKE'
							);

							// Replace the meta_query with our new one
							$query->query_vars['meta_query'] = $custom_keyword;

							// Add keyword search to our custom version of the 's' parameter
							$query->set('_meta_or_title', $keyword);
						else:
							$query->set('s', $keyword);
						endif;

					endif;
				endif;

			endif;
		}
	
	/* === END === */

	/* ==== This function will generate a new query that will fetch the first 5 pinned posts from the given post type ==== */

		function generate_pinned_posts_query( $query ) {

			// Make global so we can access it on our archive template
			global $pinned_posts;
			global $jf_news_posts;
			global $pinned_ids;

			$post_type = $query->query['post_type'];

			if( $post_type ):

				$pinned_ids = [];

				// If we are on news, we want to fetch a single Jersey Finance news article first, so reduce number of pinned items
				$posts_per_page = $post_type == 'all-news' ? 5 : 6;

				// Grab default query values from default query
				$original_meta = isset($query->query_vars['meta_query']) ? $query->query_vars['meta_query'] : [];
				$original_tax 	= isset($query->query_vars['tax_query']) ? $query->query_vars['tax_query'] : [];

				$keyword = filter_input( INPUT_GET, 'keyword', FILTER_SANITIZE_SPECIAL_CHARS );

				/* ==== Get pinned items  ==== */				
					
					// Copy for use with this query
					$pinned_meta 	= $original_meta;
					$pinned_tax 	= $original_tax;

					$pinned_meta[] = array(
						'key'			=> 'pinned',
						'value'		=> true,
						'compare'	=> '='	
					);					

					$args = array(
						'post_type'			=> $post_type,
						'posts_per_page'	=> $posts_per_page,
						'meta_query'		=> $pinned_meta,
						'tax_query'			=> $pinned_tax,
						's'					=> $keyword
					);

					$pinned_posts = new WP_Query($args);

					// ID's to exclude from the main query
					$query_ids 		= array_column($pinned_posts->posts, 'ID');
					$pinned_ids 	= array_merge($pinned_ids, $query_ids);

				/* === END === */

				/* ==== Get single Jersey Finance news item ==== */

					if( $post_type == 'all-news' ):

						// Copy for use with this query
						$jlf_news_meta = $original_meta;
						$jlf_news_tax 	= $original_tax;

						// "Our News"
						$jlf_news_tax[] = array(
							'taxonomy' 	=> 'all-news-types',
							'field'		=> 'term_id',
							'terms'		=> 117, 
							'operator' 	=> 'IN',
						);

						$args = array(
							'post_type'			=> $post_type,
							'posts_per_page'	=> 1,
							'meta_query'		=> $jlf_news_meta,
							'tax_query'			=> $jlf_news_tax,
							's'					=> $keyword
						);

						$jf_news_posts = new WP_Query($args);

						// ID's to exclude from the main query
						$query_ids 		= array_column($jf_news_posts->posts, 'ID');
						$pinned_ids 	= array_merge($pinned_ids, $query_ids);

					endif;

				/* === END === */

			endif;

			$query->set('post__not_in', $pinned_ids);

			// echo '<pre>';
			// print_r($pinned_posts);
			// echo '</pre>';		

		}

	/* ==== END ==== */

	/* ==== This function will take start and end dates from the URL and pass them into `date_query` ==== */

		function generate_publish_date_query( $query, &$args = false ) {

			$date_query = [];

			// Get the queried date range from the url
			$start_date 	= filter_input( INPUT_GET, 'publish-start', FILTER_SANITIZE_NUMBER_INT ); 
			$end_date 		= filter_input( INPUT_GET, 'publish-end', FILTER_SANITIZE_NUMBER_INT ); 

			// Everything after the start date
			if( $start_date ):
				$date_query[]['after'] = date('c', $start_date);
			endif;

			// Everything before the end date
			if( $end_date ):
				$date_query[]['before'] = date('c', $end_date);
			endif;

			// Include the selected date
			if( $start_date || $end_date )  {
				$date_query[]['inclusive'] = true;
			}

			// Push our date_query into the main query
			if( !empty($args) ) {
				$args['date_query'] = $date_query;
			} else {
				$query->query_vars['date_query'] = $date_query;
			}
		}

	/* === END === */

	/* ==== This function generates the tax_query needed to search against a series of specified taxonomies ==== */

		function generate_tax_query( $query, $taxonomies, &$args = false ) {
			$tax_query = [];		
				
			// Loop through the taxonomies and add to the tax_query
			foreach( $taxonomies as $taxonomy ):

				// Get the queried topics from the URL (minus `all-` as that would cause permalink conflicts)
				$query_string_param = str_replace('all-', '', $taxonomy);
				$terms = filter_input( INPUT_GET, $query_string_param, FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY ); 

				if( $terms ):

					$tax_query[] = array(
						'taxonomy' 	=> $taxonomy,
						'field'		=> 'term_id',
						'terms'		=> $terms,
						'operator' => 'IN',
					);

				endif;

			endforeach;

			// Push our tax_query into the main query
			if( !empty($args) ) {
				$args['tax_query'] = $tax_query;
			} else {
				$query->query_vars['tax_query'] = $tax_query;
			}
		}

	/* === END === */

	/* ==== This function generates the meta_query needed to search against a post object or relationship field ==== */
		
		/* 
			Expects an array of post types: 
				e.g. array('all-markets', 'all-sectors')

			Also expects the relationship fields to follow the format: `related_` + post type name minus "all-"
				e.g. `related_markets` and `related_sectors`
		*/
		function generate_relationship_query( $query, $related_post_types, &$args = false ) {
			$meta_query = [];
				
			// Loop through each of the post types we want to filter against
			foreach( $related_post_types as $post_type ):

				// Get the queried topics from the URL (minus `all-` as that would cause permalink conflicts)
				$query_string_param = str_replace('all-', '', $post_type);
				$posts = filter_input( INPUT_GET, $query_string_param, FILTER_VALIDATE_INT, FILTER_REQUIRE_ARRAY ); 

				if( $posts ):
					// For each of the posts we are searching for, add a new meta query (can't use and array and `IN` as we need to use "post id" and LIKE)
					foreach( $posts as $post ):

						$meta_query[] = array(
							'key'			=> 'related_' . $query_string_param,
							'value'		=> '"' . $post . '"',
							'compare' 	=> 'LIKE'
						);

					endforeach;
				endif;

				if( $post_type == 'all-markets' ):

					// If we are querying the markets post type, also check for our fake "Rest of World" checkbox
					// If that is ticked show posts that have no market ticked
					$rest_of_world	= filter_input( INPUT_GET, 'rest-of-world', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY );

					if( !empty( $rest_of_world ) ):

						$market_args = array(
							'post_type' 		=> 'all-markets',
							'posts_per_page'	=> -1
						);

						$markets = new WP_Query( $market_args );

						if( $markets->have_posts() ):

							// Get all market ID's
							$market_ids = array_column($markets->posts, 'ID');

							// Fetch posts that have none of the markets assigned to it
							$meta_query[] = array(
								'key'			=> 'related_markets',
								'value'		=> $market_ids,
								'compare' 	=> 'NOT EXISTS'
							);

						endif;
					endif;

				endif;


			endforeach;

			// Push our meta_query onto the main query
			if( $args ) {
				$args['meta_query'][] = $meta_query;
			} else {
				$query->query_vars['meta_query'][] = $meta_query;
			}
		}
	
	/* === END === */

	/* ==== This function will take start and end dates from the URL and pass them into `date_query` ==== */

		function generate_event_date_query( $query, &$args = false ) {

			$meta_query = [];

			// Get the queried date range from the url
			$start_date 	= filter_input( INPUT_GET, 'event-start', FILTER_SANITIZE_NUMBER_INT ); 
			$end_date 		= filter_input( INPUT_GET, 'event-end', FILTER_SANITIZE_NUMBER_INT ); 


			// Any events whose end date is after the selected date AND whose start date is before the selected end date
			if( $start_date && $end_date ):
				$meta_query[] = array(
					'relation' => 'AND',
					array(
						'relation' => 'OR',
						array(
							'key'			=> 'dates_0_end_date',				// First row in our date repeater
							'value'		=> date("Y-m-d", $start_date),
							'compare' 	=> '>=',
							'type'		=> 'DATE'
						),
						array(
							'key'			=> 'dates_1_end_date',				// Second row in our date repeater
							'value'		=> date("Y-m-d", $start_date),
							'compare' 	=> '>=',
							'type'		=> 'DATE'
						),
						array(
							'key'			=> 'dates_2_end_date',				// Third row in our date repeater
							'value'		=> date("Y-m-d", $start_date),
							'compare' 	=> '>=',
							'type'		=> 'DATE'
						),
					),
					array(
						'relation' => 'OR',
						array(
							'key'			=> 'dates_0_start_date',				// First row in our date repeater
							'value'		=> date("Y-m-d", $end_date),
							'compare' 	=> '<=',
							'type'		=> 'DATE'
						),
						array(
							'key'			=> 'dates_1_start_date',				// First row in our date repeater
							'value'		=> date("Y-m-d", $end_date),
							'compare' 	=> '<=',
							'type'		=> 'DATE'
						),
						array(
							'key'			=> 'dates_2_start_date',				// First row in our date repeater
							'value'		=> date("Y-m-d", $end_date),
							'compare' 	=> '<=',
							'type'		=> 'DATE'
						)
					)
				);
			// Any events whose end date is after the selected date
			elseif( $start_date ):
				$meta_query[] = array(
					'relation' => 'OR',
					array(
						'key'			=> 'dates_0_end_date',				// First row in our date repeater
						'value'		=> date("Y-m-d", $start_date),
						'compare' 	=> '>=',
						'type'		=> 'DATE'
					),
					array(
						'key'			=> 'dates_1_end_date',				// Second row in our date repeater
						'value'		=> date("Y-m-d", $start_date),
						'compare' 	=> '>=',
						'type'		=> 'DATE'
					),
					array(
						'key'			=> 'dates_2_end_date',				// Third row in our date repeater
						'value'		=> date("Y-m-d", $start_date),
						'compare' 	=> '>=',
						'type'		=> 'DATE'
					)
				);
			// Any events whose start date is after the selected end date
			elseif( $end_date ):
				$meta_query[] = array(
					'relation' => 'OR',
					array(
						'key'			=> 'dates_0_start_date',				// First row in our date repeater
						'value'		=> date("Y-m-d", $end_date),
						'compare' 	=> '>=',
						'type'		=> 'DATE'
					),
					array(
						'key'			=> 'dates_1_start_date',				// Second row in our date repeater
						'value'		=> date("Y-m-d", $end_date),
						'compare' 	=> '>=',
						'type'		=> 'DATE'
					),
					array(
						'key'			=> 'dates_2_start_date',				// Third row in our date repeater
						'value'		=> date("Y-m-d", $end_date),
						'compare' 	=> '>=',
						'type'		=> 'DATE'
					)
				);
			endif;

			// Push our meta_query into the main query
			if( !empty($args) ) {
				$args['meta_query'][] = $meta_query;
			} else {
				$query->query_vars['meta_query'][] = $meta_query;
			}
		}

	/* === END === */

	/* ==== This function will filter by post type (mainly on the term templates) ==== */

		function generate_post_types_query( $query, &$args = false ) {
			$post_types = filter_input( INPUT_GET, 'post-type', FILTER_SANITIZE_SPECIAL_CHARS, FILTER_REQUIRE_ARRAY ); 
				
			$query->query_vars['post_type'] = $post_types;			
		}

	/* === END === */

	/* ==== This function will take the sort value from the URL and pass them into our query (we don't need to modify $args as the JSON feed doesn't need sorting) ==== */

		function generate_sort_query( $query ) {

			// Gets the value from the URL
			$sort = filter_input( INPUT_GET, 'sort-by', FILTER_SANITIZE_SPECIAL_CHARS ); 

			// If sort is not defined on events, default to sorting by event date
			if( empty($sort) && is_post_type_archive('all-events') ) {
				$query->query_vars['order'] 		= 'ASC';
				$query->query_vars['orderby'] 	= 'meta_value';
				$query->query_vars['meta_key'] 	= 'dates_0_start_date';
			}

			if( empty($sort) && ( is_post_type_archive('all-businesses') || is_tax('all-business-categories') ) ) {
				$query->query_vars['order'] 		= 'ASC';
				$query->query_vars['orderby'] 	= 'title';
			}

			if( $sort == 'event_date' ) {
				$query->query_vars['order'] 		= 'ASC';
				$query->query_vars['orderby'] 	= 'meta_value';
				$query->query_vars['meta_key'] 	= 'dates_0_start_date';
			} elseif( $sort == 'popularity' ) {
				$query->query_vars['order'] 		= 'DESC';
				$query->query_vars['orderby'] 	= 'meta_value_num';
				$query->query_vars['meta_key'] 	= 'post_views_count';
			}

		}

	/* === END === */	

	/* ==== Function that will adjust the meta query so only future dates are returned ==== */

		function only_fetch_future_events( $query ) {
			/* Grab existing meta_query values, add to it, then replace it */
			$meta_query = $query->query_vars['meta_query'];

			$today = date('Ymd');

			$meta_query[] = array(
				'key'			=> 'hide_date',
				'value'		=> $today,
				'compare'	=> '>'	
			);

			// Replace meta query
			$query->query_vars['meta_query'][] = $meta_query;
		}

	/* === END === */	

?>