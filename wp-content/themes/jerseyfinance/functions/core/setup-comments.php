<?
	function enqueue_comment_reply() {

		// On single blog post pages with comments open and threaded comments
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) { 
			// enqueue the javascript that performs in-link comment reply fanciness
			wp_enqueue_script( 'comment-reply' ); 
		}
	}
	// Hook into wp_enqueue_scripts
	add_action( 'wp_enqueue_scripts', 'enqueue_comment_reply' );


	function themeslug_commentform_title( $args ) {
			
		$args['title_reply_before'] = '';
		$args['title_reply_after']  = '';

		return  $args;
	}
	add_filter( 'comment_form_defaults', 'themeslug_commentform_title' );



	// Only show first name on comments
	function wpse_use_user_real_name( $author, $comment_id, $comment ) {

		$firstname = '' ;

		$user_id = $comment->user_id ;

		if ( $user_id ) {
			$user_object = get_userdata( $user_id ) ;
			$firstname = $user_object->user_firstname ;
		}

		if ( $firstname || $lastname ) {
			$author = $firstname; 
		}

		return $author ;
	}
	add_filter( 'get_comment_author', 'wpse_use_user_real_name', 10, 3 ) ;

?>