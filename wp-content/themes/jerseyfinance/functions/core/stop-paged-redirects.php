<?
	/* ==== Stop paged redirects, allows us to use ?paged=2 on singles so we can do pagination ==== */

		add_action( 'template_redirect', function() {
			if ( is_singular() ) {
				global $wp_query;
				$page = ( int ) $wp_query->get( 'page' );
				if ( $page > 1 ) {
						// convert 'page' to 'paged'
						$query->set( 'page', 1 );
						$query->set( 'paged', $page );
				}
				// prevent redirect
				remove_action( 'template_redirect', 'redirect_canonical' );
			}
		}, 0 ); // on priority 0 to remove 'redirect_canonical' added with priority 10	

	/* === END === */
?>