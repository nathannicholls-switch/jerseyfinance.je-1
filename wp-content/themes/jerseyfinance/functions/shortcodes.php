<?
   function update_business_form() {
      ob_start();
		global $edit_post_id;

		if( isset($edit_post_id) ):
			if( user_can_edit( $edit_post_id ) ):
				echo do_shortcode( '[gravityform id="12" title="false" description="false" ajax="true"]' );
			else:
				echo '<p class="u-align-centre"><strong>You do not have permission to edit this business.</strong></p>';
			endif;
		else:
			echo '<div class="u-align-centre">';
				echo '<p><strong>Sorry there was a problem loading your business details.</strong></p>';
				echo '<p>Please try again later or contact <a href="mailto:Kayleigh.wells@jerseyfinance.je">Kayleigh.wells@jerseyfinance.je</a> for assistance.</p>';
			echo '</div>';
		endif;

		return ob_get_clean();
	};

	function list_user_favourites() {
		ob_start(); ?>

		<? if( is_user_logged_in() ): ?>
			<?
				// Fetch array of favourites ID's
				$favourites = fetch_favourites(); 
			?> 

			<? if( $favourites ): ?>
				<ul class="button-list u-margin-top-0">

					<? foreach( $favourites as $favourite ): ?>

						<li class="u-table saved-item">
							<div class="u-table-cell">
								<a href="<? echo get_the_permalink($favourite); ?>">
									<? echo get_the_title($favourite); ?>
								</a>
							</div>
							<div class="u-table-cell">
								<span class="c-button red remove-from-favourites" data-post-id="<? echo $favourite; ?>">
									<span class="label">
										Remove saved item
									</span>
								</span>
							</div>
						</li>

					<? endforeach; ?>					

				</ul>

				<? function remove_saved_items() { ?>
					<script>
						$(function() {
							$('.remove-from-favourites').click(function() {
								var target = $(this);

								var post_id = $(this).data('post-id');

								// The data we want to push to our function
								var data = {
									action: 'add_to_favourites',	// Will run the action `wp_ajax_add_to_favourites`
									post_id: post_id
								};

								// Make an ajax request to our add_to_favourites function which will remove the favourite
								jQuery.post('/wp-admin/admin-ajax.php', data, function(response) {
									// Remove the saved item from the list
									target.closest('.saved-item').remove();
								});

								return false;
							});

						});
					</script>
				<? } ?>
				<? add_action('wp_footer', 'remove_saved_items', 20); ?>

			<? else: ?>
				<p>You do not have any saved items</p>
			<? endif; ?>

		<? else: ?>
			<p>Sorry, you must be logged in to view your favourites.</p>
			<p><a href="/login">Log in now</a>.</p>
		<? endif;

		return ob_get_clean();
	}

	function aifmd_map() {
		include( locate_template('/functions/shortcodes/aifmd-map.php') );
	}

	function fund_distribution_map() {
		include( locate_template('/functions/shortcodes/fund-distribution-map.php') );
	}

   function register_shortcodes() {
      add_shortcode('update_business_form', 'update_business_form');
		add_shortcode('list_user_favourites', 'list_user_favourites');
		add_shortcode('aifmd_map', 'aifmd_map');
		add_shortcode('fund_distribution_map', 'fund_distribution_map');
   };

   add_action( 'init', 'register_shortcodes');
?>