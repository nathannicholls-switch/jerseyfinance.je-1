<?
	/* ==== Detect if the current request is being made over AJAX ==== */

		function request_is_AJAX() {
			if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				return true;
			};
			return false;
		};

	/* ==== END ==== */
?>