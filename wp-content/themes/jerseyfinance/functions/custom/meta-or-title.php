<?
	/* ===== Allow for combination of `s` and `meta_query` on search results (use `_meta_or_title` instead of `s`) ==== */

		/*
			This is customised version.
			By default the keyword match would return hidden posts due to the OR comparison.
			This version adds an additional AND parameter that won't return private posts.
		*/

		add_action( 'pre_get_posts', function( $q ) {
			if( $title = $q->get( '_meta_or_title' ) ) {
				add_filter( 'get_meta_sql', function( $sql ) use ( $title ) {
					global $wpdb;

					// Only run once:
					static $nr = 0; 
					if( 0 != $nr++ ) return $sql;

					// Hide any posts that are marked as private - this gets tagged onto the end of the query as an additional AND parameter
					$private_sql = "(( mt1.meta_key = 'make_private' AND mt1.meta_value != '1' ) OR mt2.post_id IS NULL )";
					
					// Modified WHERE
					$sql['where'] = sprintf(
						" AND ( %s OR %s ) AND %s ",
						$wpdb->prepare( "{$wpdb->posts}.post_title like '%%%s%%'", $title),
						mb_substr( $sql['where'], 5, mb_strlen( $sql['where'] ) ),
						$private_sql
					);

					return $sql;
				});
			}
		});

	/* === END === */
?>