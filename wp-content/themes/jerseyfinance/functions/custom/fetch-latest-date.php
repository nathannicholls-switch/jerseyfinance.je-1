<?	
	// This function runs when an event is created/saved, it will set the date from which the event shouldn't appear in results

	function set_hide_date( $post_ID ) {
		
		// Only run for events
		if( get_post_type( $post_ID ) == 'all-events' ):
			
			// Old date to get us started
			$previous_date 	= '0';

			// Loop through all dates and get the last end date value (furthest in the future)
			if( have_rows('dates', $post_ID) ):
				while( have_rows('dates', $post_ID) ): the_row();

					// If this end date is greater than the previous, set it as the overall end date
					if( get_sub_field('end_date', false, false) > $previous_date ):
						$overall_end_date 	= get_sub_field('end_date', false, false); // must save as raw value (YYYYMMDD)
						$previous_date 		= get_sub_field('end_date', false, false); // must save as raw value (YYYYMMDD)
					endif;

				endwhile;

				// print_r( get_field('dates') );

				// echo '<h1>' . $overall_end_date . '</h1>';

				// die();

				// Update the overall "hide_date" date field with the value from the above loop
				update_field('field_5bf2ab35f60f7', $overall_end_date, $post_ID);
			endif;
		endif;
	}

	add_action( 'save_post', 'set_hide_date', 10, 1 ); 
?>