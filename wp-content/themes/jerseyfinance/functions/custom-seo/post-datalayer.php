<script>
	<?	
		if( is_singular('all-news') ):
			
			$type = get_the_terms(get_the_ID(), 'all-news-types');

			// Get the name of the first term
			$type = $type ? $type[0]->name : '';

		elseif( is_singular('all-work') ):

			$type = get_the_terms(get_the_ID(), 'all-work-types');
			
			// Get the name of the first term
			$type = $type ? $type[0]->name : '';

		elseif( is_tax('all-topics') ):
			
			$type = 'Spotlight On'; // get_queried_object()->name;

		endif;

		if( !empty($type) ): ?>

			dataLayer.push({ 'contentType': '<? echo $type; ?>' });

		<? endif;
	?>
</script>