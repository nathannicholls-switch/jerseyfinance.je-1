<script>
	<?
		// If the user has just logged in, look for query string and push logged in event
		$loggedin = filter_input( INPUT_GET, 'loggedin', FILTER_SANITIZE_NUMBER_INT ); 

		if( $loggedin ): ?>
			dataLayer.push({ 'memberLogin': true });
		<? endif;

		$member_logged_in 			= is_user_logged_in() ? true : false;

		if( $member_logged_in ):
			$member_id 						= get_current_user_id();
			$member_email 					= wp_get_current_user()->user_email;
			$member_domain 				= explode('@', $member_email)[1];
			$member_job_title				= get_user_meta( $member_id, 'job_title', true );
			$member_market_interests	= get_user_meta( $member_id, 'market_interests', false );
			$member_pillars				= get_user_meta( $member_id, 'pillars', false );

			$companies 						= get_joined_businesses( $member_id );

			if( $companies ):
				$company 		= reset($companies); // get first company ID (use reset as we set company name as the key, meaning [0] doesnt work)
				$company_name 	= $company ? get_the_title($company) : '';
				
				if( get_field('ceo_connect_status', $company) == 'yes' ):
					$company_subscription = 'CEO Connect';
				else:
					$company_subscription = 'Full';
				endif;

				if( get_field('related_sectors', $company) ):
					// print_r(get_field('related_sectors', $company));
				endif;
			endif;
		endif;

		// If user is logged in but not assigned to a company mark them as a guest
		if( $member_logged_in && !isset($company_subscription) ):
			$company_subscription = 'Guest';
		endif;
	?>
		
	dataLayer.push({ 'memberLoggedIn': 		<? echo $member_logged_in ? 'true' : 'false'; ?> });
	dataLayer.push({ 'memberId': 				'<? echo !empty($member_id) ? $member_id : "(not set)"; ?>' });
	dataLayer.push({ 'memberEmail': 			'<? echo !empty($member_email) ? $member_email : "(not set)";; ?>' });
	dataLayer.push({ 'memberEmailDomain': 	'<? echo !empty($member_domain) ? $member_domain : "(not set)";; ?>' });
	dataLayer.push({ 'memberJobTitle': 		'<? echo !empty($member_job_title) ? $member_job_title : "(not set)";; ?>' });

	<? if( !empty($member_market_interests) ): ?>
		dataLayer.push({ 'memberMarketInterests': '<? echo implode(",", $member_market_interests); ?>'.split(",") });
	<? else: ?>
			dataLayer.push({ 'memberMarketInterests': '(not set)' });
	<? endif; ?>

	<? if( !empty($member_pillars) ): ?>
		dataLayer.push({ 'memberMarketPillars': '<? echo implode(",", $member_pillars); ?>'.split(",") });
	<? else: ?>
		dataLayer.push({ 'memberMarketPillars': '(not set)' });
	<? endif; ?>

	dataLayer.push({ 'companySubscriptionType': '<? echo !empty($company_subscription) ? $company_subscription : "(not set)"; ?>' });
	dataLayer.push({ 'companyName': '<? echo !empty($company_name) ? $company_name : "(not set)"; ?>'} );

	<? if( !empty($company_sectors) ): ?>
		dataLayer.push({ 'companySectors': '<? echo implode(",", $company_sectors); ?>'.split(",") });
	<? else: ?>
		dataLayer.push({ 'companySectors': '(not set)' });
	<? endif; ?>

	/*
		No longer used:
		dataLayer.push({ 'memberReference': '$member.Reference' });
		dataLayer.push({ 'memberMarketFundInterests': '$member.FundInterest'.split(",") });
	*/
	
</script>

<? wp_reset_postdata(); ?>