<?
   /*
      Summary:
         - Deregister default jQuery and wp-embed js files
         - Remove unwanted scripts
			- Remove margin-top style in admin - not needed as we have a fixed header
			- Add custom repeater/flexible content field styling
			- Set height of WYSIWYG fields based on their content
	*/

	/* ==== Deregister default jQuery, wp-embed and jQuery Migrate js files ==== */

		function remove_scripts() {
			// Remove default jQuery
			wp_deregister_script('jquery');
			wp_deregister_script('wp-embed');
		}
		add_action('wp_enqueue_scripts', 'remove_scripts', 10);

		// Remove social login stylesheet
		function remove_assets() {
			wp_dequeue_style('wsl-widget');
			wp_deregister_style('wsl-widget');
		}
		add_action( 'login_enqueue_scripts', 'remove_assets', 99 );

		/* ==== Remove Gutenburg stylesheet from front & backend ==== */
		
		function wps_deregister_styles() {
			wp_dequeue_style( 'wp-block-library' );
			
			// Social Login
			wp_dequeue_style( 'wsl-widget' );
			wp_deregister_style( 'wsl-widget' );
		}
		add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );

		/* === END === */

		/* 
			We re-enqueue our own scripts as `jquery` in enqueue-files.php, 
			jQuery Migrate is added automatically any time a script is regeistered as `jquery`,
			so below we remove `jquery-migrate` as a dependancy of `jquery`
		*/
		
		function remove_jquery_migrate( $scripts ) {
			if ( ! is_admin() && isset( $scripts->registered['jquery'] ) ) {
				$script = $scripts->registered['jquery'];

				if ( $script->deps ) { // Check whether the script has any dependencies
					$script->deps = array_diff( $script->deps, array( 'jquery-migrate' ) );
				}
			}
		}

		add_action( 'wp_default_scripts', 'remove_jquery_migrate' );
	
	/* === END === */

	/* ==== Remove unwanted scripts ==== */

		remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
		remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
		remove_action( 'admin_print_styles', 'print_emoji_styles' );

	/* === END === */

	/* ==== Remove margin-top style in admin - not needed as we have a fixed header ==== */

		function remove_admin_bar_margin() {
			if ( is_user_logged_in() ) {
				$style = '<style>';
					$style .= 'html { margin-top: 0 !important; }';
				$style .= '</style>';

				echo $style;
			}
		}

		add_action('wp_footer', 'remove_admin_bar_margin');

	/* === END === */

	/* ==== Add custom repeater/flexible content field styling to back-end (specific field only, we are using field key) - space each row out ==== */

		function custom_field_styles() {
			$style = '<style>';
				$style .= '.acf-field-repeater .acf-table { border-collapse: separate; background: #FFF; border-spacing: 0 25px; border-right: 0; border-left: 0; }';
				$style .= '.acf-fields:after { border-spacing: 0; }';
				$style .= '.acf-field-repeater .acf-table .acf-row .acf-row-handle { color: #666; font-weight: 600; }';
				$style .= '.acf-field-repeater .acf-table .acf-row:nth-child(even) .acf-row-handle.order { background: #E7E7E7; }';
				$style .= '.acf-field-repeater .acf-table .acf-table { border-spacing: 0; }';
				$style .= '.acf-field-repeater .acf-table .acf-row { box-shadow: -3px 3px 10px rgba(35,40,45,0.1) }';
				$style .= '.acf-field-repeater .acf-table .acf-row > td { border-top: 1px solid #E1E1E1; border-bottom: 1px solid #E1E1E1; }';
				$style .= '.acf-field-repeater .acf-table .acf-row-handle.remove { border-right: 1px solid #E1E1E1; }';
				$style .= '.acf-field-repeater .acf-table .acf-row-handle.order { border-left: 1px solid #E1E1E1; }';

				$style .= '.acf-table > thead > tr > th:first-child { border-left: 1px solid #E1E1E1; }';
				$style .= '.acf-table > thead > tr > th:last-child { border-right: 1px solid #E1E1E1; }';
				$style .= '.acf-table > thead > tr > th { border-width: 1px 0px 1px 1px; }';

				$style .= '.acf-field-repeater .wp-editor-tools:after { display: none; }'; // fix wysiwyg repeater issue

				// Textarea height			
				//$style .= '.wp-editor-container textarea.wp-editor-area { height: 200px; }';

				// Style to match JFL
					// Each meta box grey tab
					$style .= '#poststuff .stuffbox>h3, #poststuff h2, #poststuff h3.hndle { background: #666; border-bottom: 0; color: #FFF; font-weight: 600; }';
					$style .= '.acf-flexible-content .layout, .acf-postbox > .inside { border: 2px solid #666; }';
					$style .= '.js .postbox .handlediv .toggle-indicator:before { background: #FFF; }';

					// Main flexible content black tabs
					$style .= '#acf-group_5bcf41300d7e1 .hndle, .acf-flexible-content .layout .acf-fc-layout-handle { background: #000; border-bottom: 0; color: #FFF; font-weight: 600; }';
					$style .= '.acf-flexible-content .layout .acf-fc-layout-order { background: #CB181C; color: #FFF; }';
					$style .= '#acf-group_5bcf41300d7e1 .inside, .acf-flexible-content .layout { border: 2px solid #000; }';
			$style .= '</style>';

			echo $style;
		}

		add_action('admin_head', 'custom_field_styles');

	/* === END === */

	/* === Set height of WYSIWYG fields based on their content === */

		function my_acf_input_admin_footer() { ?>
			<script type="text/javascript">
				(function($) {
					if ( typeof acf !== 'undefined' ) {
						acf.add_action( 'wysiwyg_tinymce_init', function( ed, id, mceInit, $field ) {

							// set height of wysiwyg on frontend
							var minHeight = 100;
							var mceHeight = $( ed.iframeElement ).contents().find( 'html' ).height() || minHeight;

							if ( mceHeight < minHeight ) {
								mceHeight = minHeight;
							}

							$( ed.iframeElement ).css( 'height', mceHeight );
						} );
					}
				})(jQuery);	
			</script>
			<? 
		} 
		add_action('acf/input/admin_footer', 'my_acf_input_admin_footer');

		function admin_styles() { ?>
			<style>
				iframe[id^='wysiwyg-acf-field-favicon_'] {
					min-height:40px;
				}
			</style>
			<?
		} 
		add_action('admin_head', 'admin_styles');

	/* === END === */

	/* ==== Remove WP XML Scripts, stop Pingbacks ==== */

		remove_action ('wp_head', 'rsd_link');
		remove_action( 'wp_head', 'wp_shortlink_wp_head');
		remove_action( 'wp_head', 'wlwmanifest_link');
		remove_action('wp_head', 'wp_generator');

	/* === END == */
?>