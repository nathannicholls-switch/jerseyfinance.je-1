<?
	/**
	* Parses PDF files and checks authentication settings
	* /check-file?file=test.pdf
	*
	*/

	function check_file( $query ) {
		if( array_key_exists('check-file', $query->query_vars) ) {

			// Grab the filename from the query string
			function ispublicallyViewable() {

				$allowed_file_types = array(
					// PDF
					'application/pdf',
					// Word
					'application/msword',
					'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
					// Powerpoint
					'application/vnd.ms-powerpoint',
					'application/vnd.openxmlformats-officedocument.presentationml.presentation',
					// Excel
					'application/vnd.ms-excel',
					'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
				);

				$getFilePath = html_entity_decode($_GET['file']) ;

				// grab the full URL of the file
				$getFullFileUrl = get_site_url() . '/' . $getFilePath;

				// get the type of the file (custom function in functions.php)
				$getFileType = mime_content_type($getFilePath);

				// convert the filepath from a path to a readable name
				$getFileName = $getFilePath ? basename($getFilePath) : null;

				// get the size of the file to check against
				$getFileSize = filesize($getFilePath);

				// get the ID of the attachment
				$getAttachmentId = attachment_url_to_postid($getFullFileUrl);

				// set the header if there's a value from the field
				$authType = get_field('security_level', $getAttachmentId);

				// If there is going to be a restriction, don't allow access
				if( $authType ):
					$can_view = false;
				endif;

				// Depending on authentication type determine whether or not the user can view the file
				if( $authType == 'working-group' ):

					// The working group ID
					$working_group = get_field('working_group', $getAttachmentId)->ID;
					
					// Users from the working group
					$allowed_users = get_field('users', $working_group);

					if( $allowed_users ):

						// Just the IDs
						$allowed_users = array_column($allowed_users, 'ID');	

						// Check if the current user is in the list
						if( in_array( get_current_user_id(), $allowed_users ) ):
							$can_view = true;
						endif;
					endif;
				else:
					$can_view = user_can_view( $authType );
				endif;

				// Allow admins to see everything
				if( current_user_can('administrator') || current_user_can('jfl_administrator') ) {
					$can_view = true;
				}

				// check to see if the file requires permissions
				if( $authType ) {
					// check to see if the user has the correct permissions
						// - user logged in must be true
						// - the user ID can't be 0
						// - the user needs the value 1 from salary_survey_member
					if( (is_user_logged_in() == true) && get_current_user_id() !== 0 && $can_view ) {

						// check to see if the file exists, is the correct mimetype and size
						if( file_exists($getFilePath) === true && $getFileSize > 0 && in_array( $getFileType, $allowed_file_types ) ) {

							renderFile($getFilePath, $getFileName, $getFileSize, $getFullFileUrl, $getFileType);

						} else {

							fileNotValid('The file you have requested is not valid.');

						};
					} else {
						redirectNonAuthorisedUser( $getFilePath );
					}
				} else {
					// if no permissions are required render the file as normal
					renderFile($getFilePath, $getFileName, $getFileSize, $getFullFileUrl, $getFileType);
				};
			};

			function renderFile($filepath, $filename, $filesize, $fileurl, $getFileType) {
				header("Content-Type: $getFileType");

				if($fileurl) {
					header("Link: \"$fileurl\"");
				};

				// Based on the type, decide how to return it
				$disposition = $getFileType == 'application/pdf' ? 'inline' : 'attachment';

				if($filename) {
					header("Content-Disposition: $disposition; filename=\"$filename\"");
				};

				readfile($filepath);
			};

			function fileNotValid($msg) {
				echo 'MSG';
				echo $msg;
			};

			function redirectNonAuthorisedUser( $file_path ) {
				$location = '/permission-denied/';

				// If we have the filepath, add that as a query string on permission denied - we will attempt to redirect the login form to this on success
				if( $file_path ):
					$location .= '?redirect=/' . $file_path;
				endif;

				// redirect to the homepage
				header("Location: $location");
				die();
			};

			ispublicallyViewable();

		}
	}
	add_action( 'pre_get_posts', 'check_file', 20 );

?>