<?
   /* ===== Include files needed for Full Calendar ==== */

      function enqueue_full_calendar() {
         if( is_post_type_archive( 'all-events' ) ) {
            wp_enqueue_style( 'full-calendar-style', get_stylesheet_directory_uri() . '/dist/css/plugins/full-calendar.min.css', false, '1.00.01' );
            wp_enqueue_style('full-calendar-style');

            wp_register_script('full-calendar-script-1', get_stylesheet_directory_uri() . '/dist/js/plugins/moment.min.js', array('jquery'),'', true);
            wp_enqueue_script('full-calendar-script-1');

            wp_register_script('full-calendar-script-2', get_stylesheet_directory_uri() . '/dist/js/plugins/full-calendar.min.js', array('jquery'),'', true);
            wp_enqueue_script('full-calendar-script-2');
         }
      }
      add_action( 'wp_enqueue_scripts', 'enqueue_full_calendar', 10, 2 );

   /* === END === */

   /* ===== Setup JSON feeds for Full Calendar - to view the JSON go to /event-json ==== */

		// (Had to declare all these in the same function for some reason)
      function add_calendar_endpoints() {
         // Event calendar JSON
         add_rewrite_endpoint('json/events', EP_ROOT);

			// Returns a JSON feed of business that the users email matches with. e.g. /json/businesses?email=me@jerseyfinance.je
			add_rewrite_endpoint('json/businesses', EP_ROOT); 

			// Used for file permissions - all PDF, DOC and DOCX files are sent here for permissions checks
			add_rewrite_endpoint('check-file', EP_ROOT);
			
			// Used to update peoples email - they get sent a link to confirm email changes, that link points here with a hashed key to verify the change
			add_rewrite_endpoint('verify-email', EP_ROOT);
			
			// Juye's thing for salesforce - will output users who have the `send_to_salesforce` value set to 1
			add_rewrite_endpoint('json/updated-users', EP_ROOT);
      }
      add_action( 'init', 'add_calendar_endpoints' );

   /* === END === */

   /* ===== Depending on the post type, fetch different post types ===== */

	function generate_calendar_query( $query ) {
		if( array_key_exists('json/events', $query->query_vars) ) {

			// Create the initial args, so that we can pass them into generate_query to be modified
			$args = array(
				'posts_per_page'  => -1,
				'post_type'       => 'all-events',
				'post_status'		=> 'publish',
			);

			// Modify our arguments
			generate_query( false, $args, false, true, array('all-topics', 'all-authors'), array('all-sectors', 'all-markets') );

			// Create our query
			$calendar_query = new WP_Query($args);

			$json_posts = [];

			// Loop through all of our posts, push them into the $json_posts array which will be converted to JSON at the end
			while ( $calendar_query->have_posts() ) : $calendar_query->the_post();
				// Reset variables
				$permalink = '';
				$title = '';
				$description = '';
				$start = '';                  // Start date/time (for full calendar)
				$end = '';                    // End date/time (for full calendar)
				$all_day = '';                // Is it an all day event (for full calendar)

				$title       	= get_the_title();
				$permalink  	= get_the_permalink();

				$dates = get_field( 'dates', get_the_ID() );

				if( have_rows('dates') ):
					while( have_rows('dates') ): the_row();
						// $date = array_values($date);

						// Get the start and end date from the field
						$start_date = get_sub_field('start_date', false, false);
						$end_date	= get_sub_field('end_date', false, false);
						$start_time = get_sub_field('start_time', false, false);
						$end_time	= get_sub_field('end_time', false, false);

						// Make sure there is a start and end date, otherwise the calendar will break
						if( !empty($start_date) && !empty($end_date) ):
							$start   = strtotime($start_date);
							$end     = strtotime($end_date);
							
							// Is it an all day event
							if( $start_time == $end_time ) {
								$all_day = true;

								// Add a day as full calendar end dates aren't inclusive
								$end = strtotime( "+1 day", strtotime($end_date) ); 
							} else {
								$all_day = false;
							}

							$calendar_start   = date('Y-m-d', $start);
							$calendar_start	.= $start_time ? ' ' . $start_time : '';

							$calendar_end     = date('Y-m-d', $end);
							$calendar_end	 	.= $end_time ? ' ' . $end_time : '';

							// Push this post to the JSON array
							$json_posts[] = array(
								'id'           => get_the_ID(),
								'url'          => $permalink,
								'title'        => html_entity_decode($title),
								'start'        => $calendar_start,
								'end'          => $calendar_end,
								'allDay'       => $all_day,
							);

						endif;

					endwhile;
				endif;
			endwhile;

         // Send to JSON object
         wp_send_json(
            $json_posts
         );
		}
	}
	add_action( 'pre_get_posts', 'generate_calendar_query', 20 );
?>