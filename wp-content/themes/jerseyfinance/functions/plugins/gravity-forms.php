<?
	// Global changes across all forms, adjusts postition of scripts, field formats etc.
	include( locate_template('functions/plugins/gravity-forms/adjust-default-behaviour.php') );

	// Functions that allow you to change a field type (useful for changing fields insde a list field)
	include( locate_template('functions/plugins/gravity-forms/field-conversion.php') );
	
	// Functions that set the choices of checkboxes based on post type or taxonomy
	include( locate_template('functions/plugins/gravity-forms/set-choices.php') );

	// Function that will sort dropdown/checkbox Gravity Forms choices by text value.
	include( locate_template('functions/plugins/gravity-forms/sort-choices.php') );

	// Functions used to prefill, set and update ACF/Gravity forms fields
	include( locate_template('functions/plugins/gravity-forms/content-submission-functions.php') );
?>