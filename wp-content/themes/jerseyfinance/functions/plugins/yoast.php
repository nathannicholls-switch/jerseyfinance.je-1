<?
   /*
      Summary:
         - Move Yoast meta editor to the bottom when editing
			- Stop stupid Yoast from breaking the site with it's stupid automatic redirects
	*/

	/* ==== Move Yoast meta editor to the bottom when editing ==== */

		add_filter( 'wpseo_metabox_prio', function() { return 'low';}); 

	/* === END === */

   /* ===== Stop stupid Yoast from breaking the site with it's stupid automatic redirects ==== */

      add_filter('wpseo_premium_post_redirect_slug_change', '__return_true' );

   /* ==== END ==== */
?>