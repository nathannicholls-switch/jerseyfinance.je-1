<?
	/* 
		This file contains functions that are used to import/manipulate/sanitise data imported via WP All Import	

      Summary:
         - Function that strips unwanted tags from the import
			- Function that will add https://www.jerseyfinance.je before any urls
			- Function that will assign a random image to a post if one is not provided
	*/

   /* ==== Used to strip unwanted tags from the import ==== */

      function import_shorten_and_strip($text, $length) {
         $text = (string)$text;         
         $text = strip_tags($text, "<h3><h4><h5><h6><strong><a>");

         // remove style attributes
         $text = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $text);

         // on some of the ?newer MS Word exports, where you get conditionals of the form 'if gte mso 9', etc
         $num_matches = preg_match_all("/\<!--/u", $text, $matches);
         if($num_matches){
            $text = preg_replace('/\<!--(.)*--\>/isu', '', $text);
         }

			if( $length !== "false" && strlen($text) > $length ):
				$text = substr($text, 0, $length) . '...';
			endif;

         return $text;
      }
   
   /* === END === */

	/* ==== This function will add https://www.jerseyfinance.je before any urls such as /media/PDF-Brochures/Charities-Jersey-Law-Factsheet.pdf ==== */
	
		function prepend_site_url( $url ) {

			$url = 'https://www.jerseyfinance.je' . $url;

		}

	/* === END === */

	/* ==== This function will assign a random image from the below array to the imported post if an image is not specified ==== */

		function assign_random_image( $image ) {
			if( empty($image) ):
				$images = array(
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_639712069.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_644242231.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_440798026.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_342875246.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_316790060-2.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_718695385.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_399932962.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_188830688.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_696219664.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_644243890.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_154446107.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_446825206.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_791448886.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_1015134556.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_273209258.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_1101304928.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_750404785.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_351548024.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_570581215.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_1020928975.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2018/12/shutterstock_158437148.jpg'
				);

				$total 			= count( $images );
				$random_index 	= rand(0, $total - 1 );

				$image = $images[$random_index];
			endif;

			return $image;
		}

	/* === END === */

	/* ==== This function will assign a random image from the below array to the imported post if an image is not specified ==== */

		function assign_random_event_image( $image ) {
			if( empty($image) ):
				$images = array(
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2019/01/shutterstock_1240685776.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2019/01/shutterstock_1148010527.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2019/01/shutterstock_1016897443.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2019/01/shutterstock_705442543.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2019/01/shutterstock_672088813.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2019/01/shutterstock_502315747.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2019/01/shutterstock_482953693.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2019/01/shutterstock_388483528.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2019/01/shutterstock_256492474.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2019/01/shutterstock_663759304.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2019/01/shutterstock_548125588.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2019/01/shutterstock_533253712.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2019/01/image-8-1024x682.png',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2019/01/shutterstock_291725090.jpg',
					'http://jerseyfinance.wpengine.com/wp-content/uploads/2019/01/shutterstock_272255090.jpg',
				);

				$total 			= count( $images );
				$random_index 	= rand(0, $total - 1 );

				$image = $images[$random_index];
			endif;

			return $image;
		}

	/* === END === */
?>