<?
	// Set checkbox choices based on post type
	function set_choices_from_post_type( $field, $post_type ) {

		$choices = [];

		$args = array(
			'post_type' 		=> $post_type,
			'posts_per_page' 	=> -1,
			'orderby'			=> 'title',
			'order'				=> 'ASC'
		);

		$post_type_query = new WP_Query($args);

		if( $post_type_query->have_posts() ):
			while( $post_type_query->have_posts() ): $post_type_query->the_post();
				$choices[] = array(
					'text' => get_the_title(),
					'value' => get_the_ID()
				);
			endwhile;
			wp_reset_postdata();
		endif;

		// Set the field choices
		$field->choices = $choices;
	}
?>