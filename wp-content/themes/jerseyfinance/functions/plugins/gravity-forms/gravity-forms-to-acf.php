<?
	/*
		FUNCTION: 	gf_to_acf_checkboxes
		PURPOSE: 	This function is fired when a post is CREATED. It is used to save a CHECKBOX Gravity Forms field as an ACF object in the DB. 
						It is fired once a post is created and then grabs the gravity forms data from the submitted entry.
						It then formats it to be ACF friendly and then uses update_field to replace the data in ACF.

		USAGE: 		saveGravityFormsCheckboxes( $entry['post_id'], $form_fields['business']['related_sectors'] );
	*/

	function gf_to_acf_checkboxes( $post_id, $form_field ) {

		// Prefix with input_ to match gravity forms - we ue this to grab the posted data
		$form_field_key = 'input_' . $form_field['input_id'];

		// The ACF key we want to push the data to
		$acf_key = $form_field['acf_key'];

		// Create an empty array that we can push all our checbox values into
      $acf_checkboxes = array();

      // Look for all of the posted values using the forms' field key
      foreach($_POST as $key => $value) {
         
         // If the posted key has matches 'input_X_' in it, push the value into the ACF array
         if (strpos($key, $form_field_key . '_') === 0) {
            array_push($acf_checkboxes, $value);
         }
      }

      // add new value
      update_field( $acf_key, $acf_checkboxes, $post_id  );
	}

	/*
		FUNCTION: 	gf_to_acf_list
		PURPOSE: 	This function is fired when a post is CREATED. It is used to save a LIST Gravity Forms field as an ACF repeater field  in the DB. 
						When the posts are created Gravity Forms adds the data to the database in it's own format.
						We then grab the gravity forms data from the DB and format/replace it with ACF friendly data.

		USAGE: 		gf_to_acf_list( $entry['post_id'], $form_fields['business']['domain_names'] );
	*/

	function gf_to_acf_list( $post_id, $form_field, $field_name ) {

		// Fetch each of the column field names from our field array
		$column_names = $form_field['columns'];

		// The ACF key we want to push the data to
		$acf_key = $form_field['acf_key'];

		// Get all of the gravty forms rows and have them as part of one array
      $gravity_forms_rows = get_post_meta( $post_id, $field_name );
      
		// Remove the gravity forms data
		delete_post_meta( $post_id, $field_name );

      // Format the Gravity Forms data so that we can push it into the DB as an ACF repeater
		if( $gravity_forms_rows ) {
         $value = array();

			// Group every 2 items in the array and add them into post_meta
			foreach($gravity_forms_rows as $list_row) {
            
				// Explode as data is stored as a single string and we need to loop through it
				$list_row = explode('|', $list_row); 
            
            $col_count = 0;

            $row = array();

            // Loop through each column and set the value against the key in thr $row array
            foreach( $column_names as $key => $attributes ) {

               // If the list has an upload field in it, grab the attachment ID and return that
					if( array_key_exists( 'type', $attributes ) && $attributes['type'] == 'image' ) {
						$col_value = attachment_url_to_postid( $list_row[$col_count] );
					} else {
						$col_value = $list_row[$col_count];
					}

					// Set value against row key				
               $row[$key] = $col_value;
               $col_count++;
            }

            // Push this row into the array
            $value[] = $row;
         }

         // Push our rows into the post meta
         update_field($acf_key, $value, $post_id);
      }
	}
?>