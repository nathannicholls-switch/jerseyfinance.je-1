<?
	/*
		This file contains a collection of functions that can change input types of Gravity Forms.

		Useful for modifying list fields (We have a plugin that allows fileuploads inside lists).
	*/

	// Converts field into a textarea
	function convert_to_textarea( $input, $input_info, $field, $text, $value, $form_id ) {
		// Build field name, must match list field syntax to be processed correctly
		$input_field_name = 'input_' . $field->id . '[]';
		$tabindex = GFCommon::get_tabindex();

		// Convert to a textarea
		$input = '<textarea name="' . $input_field_name . '" ' . $tabindex . ' class="textarea medium" cols="50" rows="5">' . $value . '</textarea>';
		
		// Output new field
		return $input;
	}

	// Converts field into a email type
	function convert_to_email( $input, $input_info, $field, $text, $value, $form_id ) {
		// Build field name, must match list field syntax to be processed correctly
		$input_field_name = 'input_' . $field->id . '[]';
		$tabindex = GFCommon::get_tabindex();

		// Convert to a textarea
		$input = '<input type="email" name="' . $input_field_name . '" ' . $tabindex . ' placeholder="info@yourdomain.co.uk" value="' . $value . '" >';
		
		// Output new field
		return $input;
	}

	// Turn regular fields into datepicker
	function make_datepicker( $input = false, $field = false, $value = false, $lead_id = false, $form_id = false ) {
		// Build field name, must match list field syntax to be processed correctly
		$input_field_name = 'input_' . $field->id;
		$tabindex = GFCommon::get_tabindex();

		// Convert to a timepicker
		$input = '<input class="flatpickr datepicker" type="text" name="' . $input_field_name . '" ' . $tabindex . ' value="' . $value . '" >';
		
		// Output new field
		return $input;
	}

	// (list) Adds a class to the field, so that the datepicker is triggered on it
	function make_list_datepicker( $input = false, $input_info = false, $field = false, $text = false, $value = false, $form_id = false ) {
		// Build field name, must match list field syntax to be processed correctly
		$input_field_name = 'input_' . $field->id . '[]';
		$tabindex = GFCommon::get_tabindex();

		// Convert to a timepicker
		$input = '<input class="flatpickr datepicker" type="text" name="' . $input_field_name . '" ' . $tabindex . ' value="' . $value . '" >';
		
		// Output new field
		return $input;
	}

	// Turn regular fields into timepicker
	function make_timepicker( $input = false, $field = false, $value = false, $lead_id = false, $form_id = false ) {
		// Build field name, must match list field syntax to be processed correctly
		$input_field_name = 'input_' . $field->id;
		$tabindex = GFCommon::get_tabindex();

		// Convert to a timepicker
		$input = '<input class="flatpickr timepicker" type="text" name="' . $input_field_name . '" ' . $tabindex . ' value="' . $value . '" >';
		
		// Output new field
		return $input;
	}

	// (list) Adds a class to the field, so that the timepicker is triggered on it
	function make_list_timepicker( $input = false, $input_info = false, $field = false, $text = false, $value = false, $form_id = false ) {
		// Build field name, must match list field syntax to be processed correctly
		$input_field_name = 'input_' . $field->id . '[]';
		$tabindex = GFCommon::get_tabindex();

		// Convert to a timepicker
		$input = '<input class="flatpickr timepicker" type="text" name="' . $input_field_name . '" ' . $tabindex . ' value="' . $value . '" >';
		
		// Output new field
		return $input;
	}
?>