<?
	/* ==== Add Google Maps API key to the admin so that the Google Maps fields works ==== */

		function acf_google_maps_init() {
			acf_update_setting('google_api_key', 'AIzaSyD_Qv26-GFSqOKim0Mw6Lu7km7D-3vSd5A');
		}

		add_action('acf/init', 'acf_google_maps_init');
	
	/* === END === */

	/* ==== Sort relationship fields by name ==== */

		function order_relationship_fields( $args, $field, $post_id ) {
			
			// Sort by name
			$args['orderby'] = 'name';
			
			// return
			return $args;
		}

		// Apply to every relationship field
		add_filter('acf/fields/relationship/query', 'order_relationship_fields', 10, 3);

	/* === END === */
?>