<?
	/*
		If the post is marked as private, add noindex
	*/
	function make_private() {
		// Get the post ID using the current url		
		if( is_page() || is_singular() ):
			// If the post has been marked private add <meta name="robots" content="noindex,follow"/> (YOAST SPECIFIC)
			if( get_field('make_private', get_the_ID()) ):
				add_filter("wpseo_robots", function() { return "noindex, follow"; });
			endif;
		endif;
	}

	add_action( 'wp_head', 'make_private' );

?>