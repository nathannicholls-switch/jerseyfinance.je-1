<?
	$args = array(
		'post_type'			=> 'all-fund-countries',
		'posts_per_page'	=> -1,
		'orderby'			=> 'title',
		'order'				=> 'ASC'
	);

	$fund_query = new WP_Query( $args ); 
?>

<? if( $fund_query->have_posts() ): ?>
	<div class="container">
		<div class="interactive-map-container">

			<!-- <img class="u-margin-bottom-20" style="margin-left: auto;margin-right: auto;" src="<? echo get_template_directory_uri(); ?>/dist/images/am-maps/AIFMD-NPPRs-banner.png" alt="AIFMD How to navigate the NPPRs" title="AIFMD How to navigate the NPPRs" /> -->

			<div id="aifmd-map-container">
				<div id="listdiv"></div>
				<div id="mapdiv" class="aifmd-map"></div>
		

				<a class="btn" href="#" id="aifmb-map-toggle" onclick="dataLayer.push({'event': 'switchToList'});"><i class="jfl-icon jfl-icon-white-menu"></i> Switch to list view</a>
			</div>


			<div id="aifmd-list-container">
				<p class="clearfix"><a class="btn hidden-phone" href="#" id="aifmb-list-toggle" onclick="dataLayer.push({'event': 'switchToMap'});"><i class="aifmd-icon-globe-white"></i> Switch to map view</a></p>

				<div class="c-accordion">

					<? while( $fund_query->have_posts() ): $fund_query->the_post(); ?>

						<div class="c-accordion__title" onclick="dataLayer.push({'event': 'fundAccordionCountry', 'contentListTitle': '<? the_title(); ?>'});">
							<div class="u-table">
								<?  /*
									<div class="u-table-cell u-valign-middle u-padding-right-20" style="width: 66px;">								
										if( get_field('flag') ): ?>
											<img class="u-display-inline-block" src="<? echo get_field('flag')['url']; ?>" alt="<? the_title(); ?>" title="<? the_title(); ?>" />
										<? else: ?>
											&nbsp;
										<? endif; 								
									</div>
								*/ ?>
								<div class="u-table-cell u-valign-middle">
									<? the_title(); ?>
								</div>
							</div>
						</div>

						<div class="c-accordion__content">
							<? /*
								<div class="c-accordion__content-wrapper u-font-size-small">
									<p>
										<? if( get_field('year_of_entry') ): ?>
											<strong>Year of EU entry:</strong> <? echo get_field('year_of_entry'); ?> <br>
										<? endif; ?>
									
										<? if( get_field('capital') ): ?>
											<strong>Capital city:</strong> <? echo get_field('capital'); ?> <br>
										<? endif; ?>
										
										<? if( get_field('area') ): ?>
											<strong>Total area:</strong> <? echo get_field('area'); ?> <br>
										<? endif; ?>
										
										<? if( get_field('population') ): ?>
											<strong>Population:</strong> <? echo get_field('population'); ?> <br>
										<? endif; ?>
									
										<? if( get_field('currency') ): ?>
											<strong>Currency:</strong> <? echo get_field('currency'); ?> <br>
										<? endif; ?>

										<? if( get_field('schengen_area') ): ?>
											<strong>Schengen area:</strong> <? echo get_field('schengen_area'); ?> <br>
										<? endif; ?>
									</p>

									<? 
										if( get_field('text') ):
											the_field('text');
										endif; 
									?>
								</div>
							*/ ?>						
						</div>

					<? endwhile; wp_reset_postdata(); ?>

				</div>
			</div>

			<? /*

			<div class="u-padding-top-20 u-padding-bottom-20">
				<div class="pull-left padding-bottom10 padding-top10 ">
					<!--<a class="icon-arrow " href="/media/AIFMD/AIFMD%20Implementation%20in%20Europe.pdf" onclick="dataLayer.push({'event': 'aifmdPDF'});" title="Download AIFMD PDF" target="_blank">Download PDF</a>-->
					<a class="c-button red fancybox-disclaimer" href="#modal-disclaimer" id="trigger-disclaimer" onclick="dataLayer.push({'event': 'disclaimerReview'});">View disclaimer</a>
					<a class="c-button red icon-arrow" href="mailto:peggy.gielen@jerseyfinance.je?subject=Feedback on AIFMD Tool" target="_blank" title="Email us your feedback" onclick="dataLayer.push({'event': 'aifmdFeedback'});">Send us your feedback</a>
				</div>
				<!-- <div class="pull-right padding-bottom10 padding-top10 ">
					<span>Share:</span>
					<span class="addthis_inline_share_toolbox" addthis:title="Jersey Finance AIFMD Comparison Tool" addthis:title="Is Jersey ready for AIFMD? Find out our position compared to EU countries with our new comparison tool!"></span>
				</div> -->
			</div>

			*/ ?>


			<div class="hidden">
				<? /*		
				<div id="modal-disclaimer" class="u-font-size-small">
					<p class="text-center aifmd-title"><img class="retina" src="<? echo get_template_directory_uri(); ?>/dist/images/am-maps/AIFMD-NPPRs-banner.png" alt="AIFMD How to navigate the NPPRs" title="AIFMD How to navigate the NPPRs" /></p>
					
					<!-- <p><img style="margin: 0 auto; max-width: 70%;" src="<? echo get_template_directory_uri(); ?>/dist/images/am-maps/L4340_AIFMD_MAP_INFOGRAPHIC.gif" alt="Jersey: at the forefront of the alternative funds industry" title="Jersey: at the forefront of the alternative funds industry" /></p> -->
					
					<h4>Disclaimer</h4>
					<p>The information set out in this AIFMD implementation tool (the <strong>Website Tool</strong>) is only intended to give a summary and general overview of the subject matter. It is not intended to be comprehensive and does not constitute, and should not be taken as constituting, legal or financial advice.</p>
					<p>Jersey Finance Limited makes no representations or warranties of any kind, whether express or implied, in relation to the content of the Website Tool.  Although that content has been compiled with considerable care, it may, from time to time, contain technical inaccuracies or be incomplete.  All users assume the risk of this being the case and should seek professional advice before placing any reliance on it.</p>
					
					<p class="text-center"><a class="c-button red icon-arrow u-margin-right-20" href="/" title="I don't agree" onclick="dataLayer.push({'event': 'disclaimerDisagree'});">I do not agree</a> <a class="c-button red icon-arrow aifmd-disclaimer-agree" href="#" title="I Agree to the Disclaimer" onclick="dataLayer.push({'event': 'disclaimerAgree'});">I agree to the Disclaimer</a></p>
				</div>
				*/ ?>
				
				<?/*
				while( $fund_query->have_posts() ): $fund_query->the_post(); ?>
					<a id="trigger-<? echo get_the_ID(); ?>" href="#modal-<? echo get_the_ID(); ?>" title="<? the_title(); ?>" rel="AIFMD" class="fancybox"><? the_title(); ?></a>
					<div id="modal-<? echo get_the_ID(); ?>" class="aifmd-modal">

						<div class="sidebar">
							<h2><? the_title(); ?></h2>

							<? if( get_field('flag') ): ?>
								<img src="<? echo get_field('flag')['url']; ?>" alt="<? the_title(); ?>" title="<? the_title(); ?>" />
							<? else: ?>
								&nbsp;
							<? endif; ?>
							
							<p class="u-font-size-small">
								<? if( get_field('year_of_entry') ): ?>
									<strong>Year of EU entry:</strong> <? echo get_field('year_of_entry'); ?> <br>
								<? endif; ?>
							
								<? if( get_field('capital') ): ?>
									<strong>Capital city:</strong> <? echo get_field('capital'); ?> <br>
								<? endif; ?>
							
								<? if( get_field('area') ): ?>
									<strong>Total area:</strong> <? echo get_field('area'); ?> <br>
								<? endif; ?>
							
								<? if( get_field('population') ): ?>
									<strong>Population:</strong> <? echo get_field('population'); ?> <br>
								<? endif; ?>
								
								<? if( get_field('currency') ): ?>
									<strong>Currency:</strong> <? echo get_field('currency'); ?> <br>
								<? endif; ?>
								
								<? if( get_field('schengen_area') ): ?>
									<strong>Schengen area:</strong> <? echo get_field('schengen_area'); ?>
								<? endif; ?>
							</p>
						</div>

						<div class="content">
							<div class="tabbable">
								<!--
									<ul class="nav nav-tabs" id="tab-<? echo get_the_ID(); ?>">
										<li class="active"><a class="first" href="#tab1-<? echo get_the_ID(); ?>" data-toggle="tab">AIFMD <br>Passport</a></li>
										<li><a class="active last" href="#tab2-<? echo get_the_ID(); ?>" data-toggle="tab">Private Placement <br>Regime</a></li>
									</ul>
								-->
								<div class="tab-content">
									<!--
									<div class="tab-pane" id="tab1-<? echo get_the_ID(); ?>">
										<? if( get_field('transitional_provisions') ): ?>
											<? echo get_field('transitional_provisions'); ?>
										<? endif; ?>

										<p>On 30 July 2015 ESMA announced that it could see no obstacles to the extension of the AIFMD passport to Jersey and recommended that Jersey should be among the first wave of non-EU countries to get access to a Europe-wide passport under the AIFMD.</p>
										<p>On 19 July 2016 ESMA published its second positive recommendation.  ESMA confirmed that Jersey was one of only five non-EU jurisdictions to have no significant obstacles in being able to apply the passport.</p>
										<p><strong>What next?</strong><br/>
										Technically, the European Commission should adopt a delegated act in order to activate the relevant AIFMD provisions extending the passport within three months of a positive advice from ESMA. However, the Commission may wait until an adequate number of countries has been positively assessed.</p>
									</div>
									-->
									<div class="tab-pane active" id="tab2-<? echo get_the_ID(); ?>">
										<? echo get_field('text'); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				<? endwhile; wp_reset_postdata(); */ ?>
			</div>

			<?
				function aifmd_map_js() { ?>
					<?
						// Re-declare for wp_footer
						$args = array(
							'post_type'			=> 'all-fund-countries',
							'posts_per_page'	=> -1,
							'orderby'			=> 'title',
							'order'				=> 'ASC'
						);

						$fund_query = new WP_Query( $args ); 
					?>

					<link rel="stylesheet" href="<? echo get_template_directory_uri(); ?>/dist/css/plugins/am-maps/am-maps.css" type="text/css">
					<link rel="stylesheet" href="<? echo get_template_directory_uri(); ?>/dist/css/plugins/fancybox.css" type="text/css">
					<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
					<link rel="stylesheet" href="<? echo get_template_directory_uri(); ?>/dist/css/plugins/am-maps/legacy.css" type="text/css">
					<script src="<? echo get_template_directory_uri(); ?>/dist/js/plugins/jquery-ui.min.js"></script>
					<script src="<? echo get_template_directory_uri(); ?>/dist/js/plugins/fancybox.min.js" type="text/javascript"></script>
					<script src="<? echo get_template_directory_uri(); ?>/dist/js/plugins/am-maps/ammap.js" type="text/javascript"></script>
					<script src="<? echo get_template_directory_uri(); ?>/dist/js/plugins/am-maps/worldHigh.js" type="text/javascript"></script>

					<script>
						$(function(){
							
							$('.jersey-title').on('click', function () {
								$(this).toggleClass('active');
								$('.jersey-text').slideToggle('fast');
								return false;
							});
							
							$('#jersey-text-close').on('click', function () {
								$('.jersey-title').toggleClass('active');
								$('.jersey-text').slideToggle('50');
								return false;
							});
							
							$('#aifmb-list-toggle,#aifmb-map-toggle').on('click', function () {
								$('#aifmd-map-container').slideToggle('fast');
								$('#aifmd-list-container').slideToggle('fast');
								return false;
							});
							
							$('#aifmb-list-open').on('click', function () {
								$('.accordion-toggle').addClass('collapsed');
								$('.accordion-body').addClass('in').css("height","auto");
								return false;
							});
							
							$('#aifmb-list-close').on('click', function () {
								$('.accordion-toggle').removeClass('collapsed');
								$('.accordion-body').removeClass('in').css("height","0");
								return false;
							});
							
						});
					</script>

					<script type="text/javascript">
						var map;

						AmCharts.ready(function() {
							
							map = new AmCharts.AmMap();
							map.pathToImages = "<? echo get_template_directory_uri(); ?>/dist/images/am-maps/";
							map.panEventsEnabled = true;
							
							map.balloon.adjustBorderColor = true;
							map.balloon.color = "#ffffff";
							map.balloon.borderThickness = 1;
							map.balloon.cornerRadius = 0;
							map.balloon.fillColor = "#000000";
							
							map.zoomControl.buttonFillColor = "#bab7b8";
							map.zoomControl.buttonRollOverColor = "#000000";
							map.zoomControl.homeIconColor = "#000000";
							map.zoomControl.top = 120;
							
							var dataProvider = {
								mapVar: AmCharts.maps.worldHigh,
								zoomLevel:1,
								zoomLongitude:-169.305199,
								zoomLatitude:40.19709
							};
						
							map.areasSettings = {
								unlistedAreasColor: "#ead2d4",
								rollOverOutlineColor: "#FFFFFF",
								balloonText: "[[title]]: [[customData]]",
								autoZoom: true,
								outlineThickness: 0.2
							};
							
							dataProvider.areas = [
								<? while( $fund_query->have_posts() ): $fund_query->the_post(); ?>
									{
										title: "<? the_title(); ?>",
										id: "<? echo get_field('country_code'); ?>",
										groupId: "#trigger-<? echo get_the_ID(); ?>",
										color: "#ca161c",
										<? if( get_field('part_of_the_eu') == true ): ?>
											customData: "part of the EU",
										<? else: ?>
											customData: "not part of the EU",
										<? endif; ?>
										rollOverColor: "#000000",
									},
								<? endwhile; wp_reset_postdata(); ?>
							];
							
							map.dataProvider = dataProvider;
							
							map.objectList = new AmCharts.ObjectList("listdiv");
							map.showImagesInList = true;
							
							map.addListener("clickMapObject", function (event) {
								dataLayer.push({'event': 'fundCountry', 'mapCountry': event.mapObject.title});
								setTimeout(function() {
									jQuery(event.mapObject.groupId).click();
									return false;
								}, 1000);
							});
							
							map.write("mapdiv");
							
							$(window).resize(function () {
								map.write("mapdiv");
							});
							
							$('.fancybox').fancybox({
								'afterClose': function() {map.selectObject();},
								parent : '.main-content',
								helpers:  {
									title:  null
								}
							});

							<? /*
							
							$('.fancybox-disclaimer').fancybox({
								parent : '.main-content',
								width: 740,
								height: 600,
								autoSize: false,
								modal: true,
								closeBtn: false,
								helpers:  {
									overlay : {
										showEarly  : true
									},
									title:  null
								}
							});
							
							$('.aifmd-disclaimer-agree').on('click', function () {
								jQuery.fancybox.close();
								return false;
							});
							
							$('#trigger-disclaimer').click();

							*/ ?>
						});
					</script>

				
					</script>
				<? }
				add_action('wp_footer', 'aifmd_map_js', 20);
			?>
		
		</div>
	</div>

<? endif; ?>